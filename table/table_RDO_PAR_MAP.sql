
CREATE TABLE [dbo].[RDO_PAR_MAP](
	[AUTOID] [int] IDENTITY(1,1) NOT NULL,
	[AUDIT_USR] [varchar](150) NULL,
	[AUDIT_PRG] [varchar](100) NULL,
	[AUDIT_DT] [datetime] NULL,
	[PAR_ID] [varchar](100) NULL,
	[PAR_VAL_RDO] [varchar](20) NULL,
	[PAR_DESC_RDO] [varchar](150) NULL,
	[PAR_VAL_MYBO] [varchar](20) NULL,
	[PAR_DESC_MYBO] [varchar](150) NULL,
	[LAST_UPDT] [datetime] NULL
) 