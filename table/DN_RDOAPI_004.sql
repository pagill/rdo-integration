CREATE TABLE [dbo].[DN_RDOAPI](
	[AUTOID] [int] IDENTITY(1,1) NOT NULL,
	[AUDIT_USR] [varchar](50) NULL,
	[AUDIT_PRG] [varchar](50) NULL,
	[AUDIT_DT] [datetime] NULL,
	[PRFL_ID] [varchar](50) NULL,
	[PRFL_SEQ_NO] [varchar](100) NULL,
	[TRN_DT] [varchar](10) NULL,
	[TRN_TYPE] [varchar](5) NULL,
	[SA_CODE] [varchar](50) NULL,
	[IFUA_NO] [varchar](5000) NULL,
	[FUND_CODE] [varchar](50) NULL,
	[AMOUNT_NOMINAL] [float] NULL,
	[AMOUNT_UNIT] [float] NULL,
	[AMOUNT_ALLUNIT] [varchar](1000) NULL,
	[FEE_NOMINAL] [float] NULL,
	[FEE_UNIT] [float] NULL,
	[FEE_PCT] [float] NULL,
	[RED_PYMNT_ACC_SEQ_CODE] [varchar](100) NULL,
	[RED_PYMNT_BANK_BIC_CODE] [varchar](100) NULL,
	[RED_PYMNT_BANK_BI_MBR_CODE] [varchar](100) NULL,
	[RED_PYMNT_BANK_ACC_NO] [varchar](100) NULL,
	[PYMNT_DT] [datetime] NULL,
	[TRNSFR_TYPE] [int] NULL,
	[SA_REFF_NO] [varchar](100) NULL,
	[ERROR_CODE] [varchar](10) NULL,
	[ERROR_MESSAGE] [varchar](500) NULL,
	[SWC_FUND_CODE] [varchar](50) NULL,
	[CHARGE_FUND] [varchar](50) NULL,
	[PROC_STS] [varchar](1) NULL,
	[AUDIT_PROC_DT] [datetime] NULL,
	[SourceOfFund] [varchar](5) NULL,
	[BankRecipient] [varchar](5) NULL,
	[Type] [varchar](5) NULL,
	[ApprovedTime] [datetime] NULL,
	[TrxUnitPaymentProvider] [varchar](20) NULL,
	[TrxUnitPaymentType] [varchar](20) NULL,
 CONSTRAINT [PK_DN_RDOAPI] PRIMARY KEY CLUSTERED 
(
	[AUTOID] ASC
)
)
GO
ALTER TABLE [dbo].[DN_RDOAPI] ADD  CONSTRAINT [DF_DN_RDOAPI_PROC_STS]  DEFAULT ('N') FOR [PROC_STS]
GO
