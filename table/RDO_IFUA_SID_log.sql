
CREATE TABLE [dbo].[RDO_IFUA_SID_log](
	[AUTOID] [int] IDENTITY(1,1) NOT NULL,
	[AUDIT_USR] [varchar](100) NULL,
	[AUDIT_PRG] [varchar](100) NULL,
	[AUDIT_DT] [datetime] NULL,
	[SA_CODE] [varchar](5000) NULL,
	[IFUA_NO] [varchar](5000) NULL,
	[IFUA_NAME] [varchar](5000) NULL,
	[SID] [varchar](5000) NULL,
	[Client_Code] [varchar](5000) NULL,
	[Investor_Type] [varchar](5000) NULL,
	[Status] [varchar](5000) NULL,
	[ERR_STS] [int] NULL,
	[ERR_MSG] [varchar](500) NULL,
	[RSLT_MSG] [varchar](500) NULL
) 