
CREATE TABLE [dbo].[RDOAPI_PROC_LOG](
	[AUTOID] [int] IDENTITY(1,1) NOT NULL,
	[AUDIT_DT] [datetime] NULL,
	[AUDIT_PRG] [varchar](150) NULL,
	[AUDIT_USR] [varchar](150) NULL,
	[PRFL_SEQ_NO] [int] NULL,
	[PRFL_ID] [varchar](50) NULL,
	[PROC_ID] [varchar](50) NULL,
	[PROC_STS] [varchar](5) NULL,
	[PROC_MSG] [varchar](500) NULL
) ON [PRIMARY]
GO
