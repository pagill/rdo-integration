CREATE procedure [dbo].[UPD_DATA_PROC_RDOAPI](
	--@pn_PRFL_SEQ_NO int,
	@ps_PRFL_ID varchar(50),
	@ps_ID varchar(200),
	@ps_ID_TYPE varchar(200),
	@ps_PROC_STS varchar(5),
	@ps_RSLT_MSG varchar(500)
)

as

if @ps_PRFL_ID = 'DN_S-INVEST_001' or @ps_PRFL_ID = 'DN_S-INVEST_002'
BEGIn
	update [DN_RDOAPI]
	set PROC_STS = @ps_PROC_STS,
		AUDIT_PROC_DT = getDate()
	where autoid = cast(@ps_ID as int)

	INSERT INTO RDOAPI_PROC_LOG(
		AUDIT_DT, AUDIT_PRG, AUDIT_USR, PRFL_ID, PROC_ID, PROC_STS, PROC_MSG
	)VALUES
	(
		GETDATE(), @ps_PRFL_ID, 'SYSTEM-JAVA',  @ps_PRFL_ID, @ps_ID, @ps_PROC_STS, @ps_RSLT_MSG
	)

	/*
	if @ps_PROC_STS = 'Y'
	BEGIN
		--transaksi cabang
		update no_surat_detail
		set sinvest_flag = 1
		where tc_detail_id = cast(replace(@ps_ID, 'TC_', '') as int)
  
	  -- MANUAL
		update no_surat_detail
		set sinvest_flag = 1
		where urutan = cast(replace(@ps_ID, 'MNLI_', '') as int)

	  -- AUTODEBET
	  update no_surat_detail
		 set sinvest_flag = 1
	   where urutan = cast(replace(@ps_ID, 'AUTO_DBT_', '') as int)
  
	  -- ROL
	  update data_rol
		 set sinvest_flag = 1 
	   where trans_id = replace(@ps_ID, 'SF_', '')
  
	  -- VOUCHER ULTAH
	  update tbl_voc_ultah_detail
		 set SINVEST_FLAG = 1
	   where id_dtl = cast(replace(@ps_ID, 'VCHR_ULTAH_', '') as int)

		--PEMBAYARAN KOMISI KE REKSADANA RUPIAH PLUS
	  update MUTUAL_FUND_AGENT_CMSN_IFUA
	  set SINVEST_FLAG = 1
	  WHERE autoid = cast(replace(@ps_ID, 'TRN_AGENT_CMSN_', '') as int)

	   -- CashBackSIP
	  update Report_CashBack_SIP
		 set sinvestflag = 1
	   where autoid = cast(replace(@ps_ID, 'VCHR_CASHBACK_SIP_', '') as int)

	  update SINVEST_DN001
		 set FLAG = 1
	   where flag = 0
		
	END
	*/


END
ELSE IF @ps_PRFL_ID = 'DN_S-INVEST_003'
BEGIN
	if @ps_ID_TYPE = 'INDIVIDU'
	BEGIN
		update [DN_RDOAPI_003]
		set PROC_STS = @ps_PROC_STS,
			AUDIT_PROC_DT = getDate()
		where autoid = cast(@ps_ID as int)
        --    and dbo.trimdate(audit_dt) = dbo.trimdate(getDate())
	END
	ELSE IF @ps_ID_TYPE = 'INSTITUSI'
	BEGIN
		update [DN_RDOAPI_004]
		set PROC_STS = @ps_PROC_STS,
			AUDIT_PROC_DT = getDate()
		where autoid = cast(@ps_ID as int)
           -- and dbo.trimdate(audit_dt) = dbo.trimdate(getDate())
	END

	INSERT INTO RDOAPI_PROC_LOG(
		AUDIT_DT, AUDIT_PRG, AUDIT_USR, PRFL_ID, PROC_ID, PROC_STS, PROC_MSG
	)VALUES
	(
		GETDATE(), @ps_PRFL_ID, 'SYSTEM-JAVA', @ps_PRFL_ID, @ps_ID, @ps_PROC_STS, @ps_RSLT_MSG
	)

	/*if @ps_PROC_STS = 'Y'
	BEGIN
		update FPR_BANK
		set SINVEST_FLAG_KYC = 1
		where FPR_ID = cast(@ps_ID as int)

		update FPR
		set sinvest_flag_kyc = 1
		where urutan = cast(@ps_ID as int)
	END*/
	
END



GO
