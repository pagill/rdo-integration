
CREATE PROCEDURE dbo.GNRT_KYC_RDOAPI_SET_FLAG(
	@PRFL_ID varchar(20)
)

as

IF @PRFL_ID = 'DN_RDOAPI_003'
BEGIN
	update f
    set SINVEST_FLAG_KYC = 1
	from FPR_BANK f
    where exists (  
		select 1 
		from DN_RDOAPI_003 rdo
		where rdo.clientcode = f.FPR_ID
			and isnull(f.sinvest_flag_kyc, 0) = 0
			and rdo.PROC_STS = 'N'
	)
  
	update f
    set sinvest_flag_kyc = 1
	from FPR f
    where f.JENIS_REKENING = '0'
		and isnull(f.sinvest_flag_kyc, 0) = 0
		and exists (
			select 1 
			from DN_RDOAPI_003 rdo
			where rdo.clientcode = f.urutan
				and rdo.PROC_STS = 'N'
		)
END