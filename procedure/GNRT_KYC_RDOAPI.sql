CREATE PROCEDURE [dbo].[GNRT_KYC_RDOAPI]

as


INSERT INTO [dbo].[DN_RDOAPI_003](
              [AUDIT_USR]
            ,[AUDIT_PRG]
            ,[AUDIT_DT]
            ,[PRFL_ID]
            --,[PRFL_SEQ_NO]
            ,[type]
            ,[SA_Code]
            ,[SID]
            ,[FirstName]
            ,[MiddleName]
            ,[LastName]
            ,[CountryofNationality]
            ,[IDNo]
            ,[IDExpirationDate]
            ,[NPWPNo]
            ,[NPWPRegistrationDate]
            ,[CountryofBirth]
            ,[PlaceofBirth]
            ,[DateofBirth]
            ,[Gender]
            ,[EducationalBackground]
            ,[MothersMaidenName]
            ,[Religion]
            ,[Occupation]
            ,[IncomeLevel]
            ,[MaritalStatus]
            ,[SpousesName]
            ,[InvestorsRiskProfile]
            ,[InvestmentObjective]
            ,[SourceofFund]
            ,[AssetOwner]
            ,[KTPAddress]
            ,[KTPCityCode]
            ,[KTPPostalCode]
            ,[CorrespondenceAddress]
            ,[CorrespondenceCityCode]
            ,[CorrespondenceCityName]
            ,[CorrespondencePostalCode]
            ,[CountryofCorrespondence]
            ,[DomicileAddress]
            ,[DomicileCityCode]
            ,[DomicileCityName]
            ,[DomicilePostalCode]
            ,[CountryofDomicile]
            ,[HomePhone]
            ,[MobilePhone]
            ,[Facsimile]
            ,[Email]
            ,[StatementType]
            ,[FATCA]
            ,[TIN]
            ,[TINIssuanceCountry]
            ,[REDMPaymentBankBICCode1]
            ,[REDMPaymentBankBIMemberCode1]
            ,[REDMPaymentBankName1]
            ,[REDMPaymentBankCountry1]
            ,[REDMPaymentBankBranch1]
            ,[REDMPaymentA/CCCY1]
            ,[REDMPaymentA/CNo1]
            ,[REDMPaymentA/CName1]
            ,[REDMPaymentBankBICCode2]
            ,[REDMPaymentBankBIMemberCode2]
            ,[REDMPaymentBankName2]
            ,[REDMPaymentBankCountry2]
            ,[REDMPaymentBankBranch2]
            ,[REDMPaymentA/CCCY2]
            ,[REDMPaymentA/CNo2]
            ,[REDMPaymentA/CName2]
            ,[REDMPaymentBankBICCode3]
            ,[REDMPaymentBankBIMemberCode3]
            ,[REDMPaymentBankName3]
            ,[REDMPaymentBankCountry3]
            ,[REDMPaymentBankBranch3]
            ,[REDMPaymentA/CCCY3]
            ,[REDMPaymentA/CNo3]
            ,[REDMPaymentA/CName3]
            ,[ClientCode]
            ,[NamaKantor]
            ,[AlamatKantorInd]
            ,[TeleponKantor]
            ,[JabatanKantor]
            ,[KodeKotaKantorInd]
            ,[KodePosKantorInd]
            ,[BeneficialName]
            ,[Politis]
            ,[PolitisRelation]
            ,[PolitisName]
            ,[IdentitasInd1]
            ,[IFUA_NO]
			,[bitissuspend]
        )

		
 select 'SYSTEM' as AUDIT_USR,'SCRIPT' as AUDIT_PRG,GETDATE() as AUDIT_DT,'DN_S-INVEST_003' as PRFL_ID, /*@PRFL_SEQ_NO as PRFL_SEQ_NO,*/
     case isnull(isnull(ifua.IFUA_NO, ifua_s.IFUA_NO),'') 
        when '' then '1' 
        else '2'
      end as type,
	  --1 as type,
      'DH002' as SA_Code,
      case isnull(isnull(ifua.IFUA_NO, ifua_s.IFUA_NO),'') 
        when '' then '' 
        else a.clientsid
      end as SID, 
      a.NAMA_NASABAH as [FirstName], 
      '' as [MiddleName], 
      '' as [LastName],
      case a.dd_warganegara
        when 'A' then mcc.ISO_CODE 
        else 'ID' 
      end as [CountryofNationality], 
      left(
        case when (len(a.dd_no_id) =0 or a.dd_no_id is null) then '' 
          else isnull(a.dd_no_id, '')  
        end 
      ,20)
      as [IDNo], 
      case when len(ISNULL(a.dd_no_id, '')) > 0 then 
          replace(convert(varchar,ISNULL(a.dd_tgl_exp_id, '25000101'),112) , '19000101', '25000101')
          else ''
      end as [IDExpirationDate],
  case when len(replace(dbo.stripID(isnull(a.dd_no_npwp,'')), '-', '')) <> 15 then '' 
        else dbo.stripID(isnull(a.dd_no_npwp,''))  
      end [NPWPNo], 
      case when len(replace(dbo.stripID(isnull(a.dd_no_npwp,'')), '-', '')) <> 15 then ''
        else replace(ISNULL(convert(varchar,a.dd_tanggal_registrasi,112), '19000101'), '19000101', '') 
      end [NPWPRegistrationDate],
      case a.dd_warganegara
        when 'A' then ISNULL(mcc.ISO_CODE , '')
        else 'ID' 
      end [CountryofBirth], 
      case when len(a.dd_tmp_lahir)=0 then '0' 
        else a.dd_tmp_lahir 
      end as [PlaceofBirth], 
      case when len(cast(a.dd_tgl_lahir as varchar(30)))=0 then '0' 
        else convert(varchar,a.dd_tgl_lahir,120) 
      end as [DateofBirth],
      case when len(replace(replace(replace(replace(replace(a.dd_jenis_kelamin,'P','2'),'F','2'),'W','2'),'L',1),'M',2))=0 then '0' 
        else case when replace(replace(replace(replace(replace(a.dd_jenis_kelamin,'P','2'),'F','2'),'W','2'),'L',1),'M',2) is null then '0' 
              else replace(replace(replace(replace(replace(a.dd_jenis_kelamin,'P','2'),'F','2'),'W','2'),'L',1),'M',2) 
            end 
      end [Gender],
      case a.dd_pendidikan
        when '0' then '3'
        when '1' then '4'
        when '2' then '5'
        when '3' then '6'
        when '4' then '7'
        else '8'
      end as [EducationalBackground],
      a.dd_nama_ibu_sblm_nikah as [MothersMaidenName],
      case a.dd_Agama
        when '0' then '5'
        when '1' then '4'
        when '2' then '1'
        when '3' then '2'
        when '4' then '3'
        else '7'
      end as [Religion],
      case a.dp_status_kerja
        when '0' then '3'
        when '1' then '2'
        when '2' then '1'
        when '3' then '9'
        when '4' then '8'
        when '5' then '4'
        when '6' then '4'
        when '7' then '5'
        when '8' then '6'
        else '9'
      end as [Occupation],
      case isnull(a.dp_hasil_perthn,0) 
        when '0' then '2'
        when '1' then '2'
        when '2' then '2'
        when '3' then '4'
        when '4' then '4'
        when '5' then '5'
        when '6' then '6'
        when '7' then '4'
        when '8' then '1'
        when '9' then '2'
        when '10' then '3'
        when '11' then '4'
      else '3' end as [IncomeLevel], 
      case a.dd_status_nikah
        when '0' then '1'
        when '1' then '3'
        when '2' then '2'
      else '1' end [MaritalStatus], 
      ISNULL(a.dd_nama_suami_istri, '') [SpousesName], 
      '' [InvestorsRiskProfile],
      case 
        when a.tujuan_investasi >=32 then '5' 
        when a.tujuan_investasi >=16 then '3' 
        when a.tujuan_investasi >=8 then '4' 
        when a.tujuan_investasi >=4 then '1' 
        when a.tujuan_investasi >=2 then '2' 
        when a.tujuan_investasi >=1 then '1' 
        else '5'
      end as [InvestmentObjective], 
      case 
        when a.dp_Sumber_dana >=2048 then '10' 
        when a.dp_Sumber_dana >=1024 then '3'
        when a.dp_Sumber_dana >=512 then '6'
        when a.dp_Sumber_dana >=256 then '10'
        when a.dp_Sumber_dana >=128 then '9'
        when a.dp_Sumber_dana >=64 then '5'
        when a.dp_Sumber_dana >=32 then '7'
        when a.dp_Sumber_dana >=16 then '8'
        when a.dp_Sumber_dana >=8 then '1'
        when a.dp_Sumber_dana >=4 then '5'
        when a.dp_Sumber_dana >=2 then '9'
        when a.dp_Sumber_dana >=1 then '1'
      else '10' end as [SourceofFund],
      case a.dd_status_rumah 
        when '0' then '1'
        when '1' then '1'
        when '5' then '1'
        else '2' 
      end [AssetOwner], 
      left(
      REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
      case when (len(a.dd_alamat_ktp)=0) or (a.dd_alamat_ktp='<br>')  then '' 
        else case when charindex('EMAIL Alternatif',a.dd_alamat_ktp)>0  then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Alternatif email',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Alternatif',a.dd_alamat_ktp)-2) 
        else case when charindex('Alternative email',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Alternative',a.dd_alamat_ktp)-2) 
        else case when charindex('Email Alternatif',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Email Alternative',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Email Lain',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Email Tambahan',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Email Alt',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Email',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else isnull(a.dd_alamat_ktp,'') end end end end end end end end end 
      end 
      , CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' ') 
      ,100)
      as [KTPAddress],  
      REPLACE(
      case when (len(a.dd_kota_ktp)=0) then '9999'
        when isnumeric(a.dd_kota_ktp)=1 then isnull(cast(convert(int, k.kode) as varchar(7)),cast(convert(int, m.kode) as varchar(7)))
        when (k.kode is null) then isnull(k.kode, '9999')
        when (a.dd_kota_ktp is null) then '9999'	
        else isnull(cast(convert(int, k.kode) as varchar(7)),cast(convert(int, m.kode) as varchar(7)))  
      end 
      ,'.0','')
      as [KTPCityCode], 
      case when (len(ISNULL(a.dd_kodepos_ktp, ''))=0) then '' 
        else isnull(a.dd_kodepos_ktp, '') 
      end as [KTPPostalCode],
      left(
      REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
      case when (len(a.dd_alamat_srt)=0) then '' 
        else isnull(a.dd_alamat_srt,di_alamat) 
      end 
      , CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' ') 
      ,100)
      as [CorrespondenceAddress], 
      REPLACE(
      case when (len(a.dd_kota_srt)=0) then '9999'
        when isnumeric(a.dd_kota_srt)=1 then isnull(cast(convert(int, k.kode) as varchar(7)), '9999')
        when l.kode is null then isnull(l.kode, '9999')
        when a.dd_kota_srt is null then '9999'
        else isnull(cast(convert(int, l.kode) as varchar(7)), '9999') 
      end 
      ,'.0','')
      as [CorrespondenceCityCode], 
      case when (len(a.dd_kota_srt)=0) then ''
        when isnumeric(a.dd_kota_srt)=1 then isnull(cast(a.dd_kota_srt as varchar(30)), '')
        when l.kode is null then isnull(cast(a.dd_kota_srt as varchar(30)), '')
        when a.dd_kota_srt is null then ''
        else isnull(cast(a.dd_kota_srt as varchar(30)), '') 
      end as [CorrespondenceCityName], 
      case when (len(ISNULL(a.dd_kodepos_srt, ''))=0) then '' 
        else isnull(a.dd_kodepos_srt, a.di_kodepos) 
      end as [CorrespondencePostalCode], 
      '' as [CountryofCorrespondence],
      REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
      case when (len(a.dd_alamat_tinggal)=0) then '' 
        else isnull(a.dd_alamat_tinggal, a.dd_alamat_ktp) 
      end 
      , CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' ') as [DomicileAddress], 
      REPLACE(
      case when (len(a.dd_kota_tinggal)=0) then '9999'
        when isnumeric(a.dd_kota_tinggal)=1 then isnull(cast(convert(int, l.kode) as varchar(7)),cast(convert(int, m.kode) as varchar(7)))
        when l.kode is null then isnull(l.kode, '9999')
        when a.dd_kota_tinggal is null then '9999'
        else isnull(cast(convert(int, l.kode) as varchar(7)),cast(convert(int, m.kode) as varchar(7)))  
      end 
      ,'.0','')
      as [DomicileCityCode], 
      case when (len(a.dd_kota_tinggal)=0) then ''
        when isnumeric(a.dd_kota_tinggal)=1 then isnull(cast(a.dd_kota_tinggal as varchar(30)), '')
        when l.kode is null then isnull(a.dd_kota_tinggal, '')
        when a.dd_kota_tinggal is null then isnull(cast(a.dd_kota_tinggal as varchar(30)), '')
        else isnull(cast(a.dd_kota_tinggal as varchar(30)), '')  
      end as [DomicileCityName], 
      ISNULL(case when (len(a.dd_kodepos_tinggal)=0) or (len(a.di_kodepos)=0)  then '' 
        else isnull(a.dd_kodepos_tinggal,di_kodepos) 
      end, '') as [DomicilePostalCode], 
      '' as [CountryofDomicile], 
      ISNULL(cast(a.dd_telp_ktp as varchar), '0') as [HomePhone], 
      cast(a.dd_handphone_ktp as varchar) as [MobilePhone], 
      ISNULL(cast(a.dd_fax_srt as varchar), '') [Facsimile], 
    case isnull(a.dd_email_srt,'') 
        when '' then '' 
        else a.dd_email_srt 
      end as [Email], 
      case isnull(a.dd_email_srt,'') 
        when '' then '1'
        else '2' 
      end as [StatementType], 
      '' as [FATCA], 
      '' as [TIN], 
      '' as [TINIssuanceCountry],
      '' as [REDMPaymentBankBICCode1], 
      ISNULL(c.map_id, '') as [REDMPaymentBankBIMemberCode1], 
      ISNULL(b1map.PAR_VAL_RDO, '') as [REDMPaymentBankName1], 
      case isnull(c.map_id,'')
        when '' then ''
        else 'ID'
      end as [REDMPaymentBankCountry1], 
      LEFT(dbo.stripid(ISNULL(b.cabang, '')), 20) as [REDMPaymentBankBranch1], 
      case when isnull(b1map.PAR_VAL_RDO, '') = '' then '' 
          else case when isnull(b.currency, 'IDR') = 'IDR' then '1'
                    when isnull(b.currency, 'IDR') = 'USD' then '2'
                    when isnull(b.currency, 'IDR') = 'EUR' then '3'
              else '4' --lainnya
              end
      end as [REDMPaymentA/CCCY1], 
      dbo.StripID(ISNULL(b.no_rek, '')) as [REDMPaymentA/CNo1], 
      ISNULL(b.pemilik, '') as [REDMPaymentA/CName1],
      '' as [REDMPaymentBankBICCode2], 
      ISNULL(e.map_id, '') as [REDMPaymentBankBIMemberCode2], 
      ISNULL(b2map.PAR_VAL_RDO, '') as [REDMPaymentBankName2], 
      case isnull(e.map_id,'')
        when '' then ''
        else 'ID'
      end as [REDMPaymentBankCountry2], 
      LEFT(dbo.stripid(ISNULL(d.cabang, '')), 20) as [REDMPaymentBankBranch2], 

      case when isnull(b2map.PAR_VAL_RDO, '') = '' then '' 
          else case when isnull(d.currency, 'IDR') = 'IDR' then '1'
                    when isnull(d.currency, 'IDR') = 'USD' then '2'
                    when isnull(d.currency, 'IDR') = 'EUR' then '3'
              else '4' --lainnya
              end 
      end as [REDMPaymentA/CCCY2],

      dbo.StripID(ISNULL(d.no_rek, '')) as [REDMPaymentA/CNo2],
      isnull(d.pemilik, '') as [REDMPaymentA/CName2],
      '' as [REDMPaymentBankBICCode3], 
      ISNULL(g.map_id, '') as [REDMPaymentBankBIMemberCode3],
      ISNULL(b3map.PAR_VAL_RDO, '') as [REDMPaymentBankName3], 
      case isnull(g.map_id,'')
        when '' then ''
        else 'ID'
      end as [REDMPaymentBankCountry3], 
      LEFT(dbo.stripid(ISNULL(f.cabang, '')), 20) as [REDMPaymentBankBranch3],
      case when ISNULL(b3map.PAR_VAL_RDO, '') = '' then '' 
          else case when isnull(f.currency, 'IDR') = 'IDR' then '1'
                    when isnull(f.currency, 'IDR') = 'USD' then '2'
                    when isnull(f.currency, 'IDR') = 'EUR' then '3'
              else '4' --lainnya
              end
      end as [REDMPaymentA/CCCY3], 
      dbo.StripID(ISNULL(f.no_rek, '')) as [REDMPaymentA/CNo3],
      ISNULL(f.pemilik, '') as [REDMPaymentA/CName3],
      RIGHT(replicate('0',6) + convert(varchar(6),a.urutan), 6) as [ClientCode],
    ISNULL(DP_NAMA_PERUSAHAAN, '') [NamaKantor],
    LEFT(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(DP_ALAMAT, ''), CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' '), 100)  [AlamatKantorInd],
    ISNULL(REPLACE(DP_TELP, 'NULL', '') , '') [TeleponKantor],
    ISNULL(a.DP_JABATAN, '') [JabatanKantor],
    REPLACE(
      case when (len(a.DP_KOTA)=0) then '9999'
        when isnumeric(a.DP_KOTA)=1 then cast(convert(int, dp.kode) as varchar(7))
        when a.dd_kota_tinggal is null then '9999'
        else cast(convert(int, dp.kode) as varchar(7))
      end 
      ,'.0','') [KodeKotaKantorInd],
    case when (len(a.dp_kodepos)=0)  then '' 
        else isnull(a.dp_kodepos, '') 
      end [KodePosKantorInd],
    '' [BeneficialName],
    '' [Politis],
    '' [PolitisRelation],
    '' [PolitisName],
      ISNULL(id_map.PAR_VAL_RDO, 7) IdentitasId1,
      isnull(isnull(ifua.IFUA_NO, ifua_s.IFUA_NO),'')IFUA_NO,
	  case when isnull(ifua.IFUA_NO,'') = '' and isnull(ifua_s.IFUA_NO, '') <> '' then '1' else '0' end bitissuspend
      from FPR a (nolock)
      left join aria_provinsi k (nolock) on a.dd_kota_ktp=k.[kotamadya/kabupaten]
      left join aria_provinsi l (nolock) on a.dd_kota_srt=l.[kotamadya/kabupaten]
      left join aria_provinsi m (nolock) on a.dd_kota_tinggal=m.[kotamadya/kabupaten]
	  left join aria_provinsi dp (nolock) on a.DP_KOTA=dp.[kotamadya/kabupaten]
      left join fpr_bank_sinvest_temp b (nolock)
        on b.fpr_id = a.urutan    
      and b.SEQ_NO = 1
      left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) c 
        on b.bank = c.BANK_ID
      left join SINVEST_BI_MBR_CD n on c.MAP_ID = n.BI_CD
      left join RDO_PAR_MAP b1map on b1map.PAR_VAL_MYBO = n.BI_CD and b1map.PAR_ID = 'MSTR_BANK'
      left join fpr_bank_sinvest_temp d
        on d.fpr_id = a.urutan    
      and d.SEQ_NO = 2    
      left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) e 
        on d.bank = e.BANK_ID
      left join SINVEST_BI_MBR_CD o on e.MAP_ID = o.BI_CD
      left join RDO_PAR_MAP b2map on b2map.PAR_VAL_MYBO = o.BI_CD and b2map.PAR_ID = 'MSTR_BANK'
      left join fpr_bank_sinvest_temp f
        on f.fpr_id = a.urutan    
      and f.SEQ_NO = 3   
      left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) g 
        on f.bank = g.BANK_ID
      left join SINVEST_BI_MBR_CD p on g.MAP_ID = p.BI_CD 
      left join RDO_PAR_MAP b3map on b3map.PAR_VAL_MYBO = p.BI_CD and b3map.PAR_ID = 'MSTR_BANK'
      left join (
            select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
            from cust_cif a
            inner join cust_product b on a.CIF_ID = b.CIF_ID
            inner join SINVEST_IFUA_MAP c on b.Niaga_acc = c.niaga_acc 
              and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
              and c.IFUA_NO not like '%T%'
            group by a.CIF_ID
      )ifua on a.cif_sms = ifua.cif_id
	  left join (
            select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
            from cust_cif a
            inner join cust_product b on a.CIF_ID = b.CIF_ID
            inner join SINVEST_IFUA_MAP_delete c on b.Niaga_acc = c.niaga_acc 
              and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
              and c.IFUA_NO not like '%T%'
              and c.NOTES like '%suspend%'
            group by a.CIF_ID
      ) ifua_s on a.CIF_SMS = ifua_s.CIF_ID
      inner join (select fprid, max(Approval_Date) Apv_Date
                    from trx_fpr_temp 
                  where approval_by is not null 
                    and (valid=9 and approval_status=1
                          or valid=0 and approval_status=0
                          or valid=77 and approval_status=1) 
                  group by fprid) q on q.fprid = a.urutan
      left join map_cifsinvest_country mcc on mcc.CIF_CODE = a.HR_DD_WARGANEGARA_LAINNYA
      left join RDO_PAR_MAP id_map on id_map.PAR_VAL_MYBO = a.DD_JENIS_ID and id_map.PAR_ID = 'identity' and id_map.PAR_VAL_MYBO <> ''
    where a.JENIS_REKENING = '0'
        and isnull(a.SINVEST_FLAG_KYC, 0) = 0
        and exists (select 1 from vw_cust_cif_sam_rfrrl cs where cs.cif_id = a.cif_sms)
       and q.Apv_Date between dbo.getprevTradingday(getdate()) and getDate()
	   and a.urutan not in ('47875', '54818')
    order by a.urutan
  

/*

update FPR_BANK
       set SINVEST_FLAG_KYC = 1
     where exists (  
      select 1 
	  from FPR a
       where a.Urutan = FPR_BANK.FPR_ID
         and a.JENIS_REKENING = '0'
         and isnull(a.sinvest_flag_kyc, 0) = 0
         and exists (select 1 from vw_cust_cif_sam_rfrrl cs where cs.cif_id = a.cif_sms)
		 and exists (select 1 from DN_RDOAPI_003 rdo where a.urutan = rdo.frontid and rdo.proc_sts = 'N')
         and exists (
          select fprid from (
            select fprid, max(Approval_Date) Apv_Date
              from trx_fpr_temp 
             where approval_by is not null 
               and (valid=9 and approval_status=1
                        or valid=0 and approval_status=0
                        or valid=77 and approval_status=1) 
             group by fprid) b
             where b.fprid = a.urutan
              and DATEADD(dd, DATEDIFF(dd, 0, b.Apv_Date), 0) between '2021-12-10 16:00:00' and getDate()
          )    
     )
  
    update FPR
       set sinvest_flag_kyc = 1
     where JENIS_REKENING = '0'
       and isnull(sinvest_flag_kyc, 0) = 0
       and exists (select 1 from vw_cust_cif_sam_rfrrl cs where cs.cif_id = fpr.cif_sms)
	   and exists (select 1 from DN_RDOAPI_003 rdo where a.urutan = rdo.frontid and rdo.proc_sts = 'N')
       and exists (
        select fprid from (
          select fprid, max(Approval_Date) Apv_Date
            from trx_fpr_temp 
           where approval_by is not null 
             and (valid=9 and approval_status=1
                        or valid=0 and approval_status=0
                        or valid=77 and approval_status=1) 
           group by fprid) a
        where a.fprid = fpr.urutan
        and DATEADD(dd, DATEDIFF(dd, 0, a.Apv_Date), 0) between '2021-12-10 16:00:00' and getDate()
    )
	*/


	insert into [DN_RDOAPI_004](
		[AUDIT_USR],
		[AUDIT_PRG],
		[AUDIT_DT],
		[PRFL_ID],
		--[PRFL_SEQ_NO],
		type,
		ClientCode,
		NamaPerusahaan,
		NegaraDomisili,
		NoSIUP,
		SIUPExpirationDate,
		NoSKD,
		ExpiredDateSKD,
		NPWPNo,
		RegistrationNPWP,
		PlaceofEstablishment,
		DateofEstablishment,
		CompanyType,
		CompanyCharacteristic,
		IncomeLevel,
		InvestmentObjective,
		SourceofFund,
		CompanyAddress,
		CompanyCityName,
		CityRHB,
		CompanyPostalCode,
		TeleponBisnis,
		CompanyFax,
		CompanyMail,
		StatementType,
		NamaBank1,
		BankCountry1,
		BankBranchName1,
		MataUang1,
		NomorRekening1,
		NamaNasabah1,
		NamaBank2,
		BankCountry2,
		BankBranchName2,
		MataUang2,
		NomorRekening2,
		NamaNasabah2,
		NamaBank3,
		BankCountry3,
		BankBranchName3,
		MataUang3,
		NomorRekening3,
		NamaNasabah3,
		IFUA_NO,
		bitissuspend,
		CountryofEstablishment,
		nomoranggaran,
		InvestorRiskProfile,
		AssetOwner
    )
    select 
      'SYSTEM' as AUDIT_USR,
      'SCRIPT' as AUDIT_PRG,
      GETDATE() as AUDIT_DT,
      'DN_S-INVEST_003' as PRFL_ID,
    -- @PRFL_SEQ_NO as PRFL_SEQ_NO,
      case isnull(isnull(ifua.IFUA_NO, ifua_s.IFUA_NO),'') 
        when '' then '1' 
        else '2'
      end as type,  
      RIGHT(replicate('0',6) + convert(varchar(6),a.urutan), 6) as [ClientCode],
      NAMA_NASABAH as NamaPerusahaan,
      ISNULL(mcc.ISO_CODE, '') NegaraDomisili,
      case when LEN(REPLACE(REPLACE(REPLACE(ISNULL(cast(ad_no_siup as varchar), ''), '0', ''), '-', ''), 'NULL', '')) = 0 then '' 
        else ISNULL(cast(ad_no_siup as varchar), '') 
      end as NoSIUP,
      case when LEN(REPLACE(REPLACE(REPLACE(ISNULL(cast(ad_no_siup as varchar), ''), '0', ''), '-', ''), 'NULL', '')) = 0 then '' 
        else replace(convert(varchar(10),isnull(ad_no_siup_exp, '2500-01-01'),120), '1900-01-01', '2500-01-01') 
      end SIUPExpirationDate,
      case when LEN(REPLACE(REPLACE(ISNULL(cast(ad_no_surat_domisili as varchar), ''), '0', ''), '-', '')) = 0 then '' 
        else ISNULL(cast(ad_no_surat_domisili as varchar), '') 
      end as NoSKD,
      case when LEN(REPLACE(REPLACE(ISNULL(cast(ad_no_surat_domisili as varchar), ''), '0', ''), '-', '')) = 0 then '' 
        else replace(convert(varchar(10),ISNULL(ad_surat_domisili_exp, '2500-01-01'),120), '1900-01-01', '2500-01-01') 
      end ExpiredDateSKD,
      case when len(replace(dbo.StripID(ISNULL(a.di_no_npwp, '')), '-', '')) <> 15 then '' 
        else dbo.stripID(isnull(a.di_no_npwp,'')) 
      end [NPWPNo],
      case when len(replace(dbo.StripID(ISNULL(a.di_no_npwp, '')), '-', '')) <> 15 then '' 
          else REPLACE(convert(varchar,ISNULL(ad_tanggal_registrasi, '19000101') ,112), '19000101', '') 
      end NPWPRegistrationDate,
      ISNULL(di_lokasi_pendirian, '') as PlaceofEstablishment,
      case when convert(varchar(10), ISNULL(di_tgl_pendirian, '1900-01-01') ,120) =  '1900-01-01'  then '' 
        else convert(varchar(10),di_tgl_pendirian ,120) 
      end  DateofEstablishment,
      case di_jenis_institusi
      when '0' then '2'
      WHEN '1' then '3'
      WHEN '2' then '4'
      WHEN '3' then '6'
      WHEN '4' then '8'
      WHEN '5' then '8'
      WHEN '6' then '8'
      WHEN '7' then '8'
      WHEN '8' then '2'
      WHEN '9' then '1'
      WHEN '10' then '1'
      else '' end CompanyType,
      case di_karakter_pt
      when '0' then '1'
      WHEN '1' then '6'
      WHEN '2' then '8'
      WHEN '3' then '5'
      WHEN '4' then '7'
      WHEN '5' then '8' ELSE  '' END AS CompanyCharacteristic,
      case laba_bersih
      when '0' then '1'
      WHEN '1' then '1'
      WHEN '2' then '1'
      WHEN '3' then '1'
      WHEN '4' then '1'
      WHEN '5' then '2'
      WHEN '6' then '3'
      WHEN '7' then '4'
      WHEN '8' then '5'
      else '' end IncomeLevel,
      case 
        when tujuan_investasi >=32 then '5' 
        when tujuan_investasi >=16 then '3' 
        when tujuan_investasi >=8 then '4' 
        when tujuan_investasi >=4 then '1' 
        when tujuan_investasi >=2 then '2' 
        when tujuan_investasi >=1 then '1' 
        else '5'
        end as [InvestmentObjective], 
      case 
        when di_jenis_institusi in (0,6) then 3 --Yayasan/lainnya -> savingInterest
        else 1 -- > Business Profit
      end SourceofFund,
      left(
      REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
      case when (len(di_alamat)=0) or (di_alamat='<br>')  then '' 
      else case when charindex('EMAIL Alternatif',di_alamat)>0  then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Alternatif email',di_alamat)>0 then left(di_alamat,charindex('Alternatif',di_alamat)-2) 
      else case when charindex('Alternative email',di_alamat)>0 then left(di_alamat,charindex('Alternative',di_alamat)-2) 
      else case when charindex('Email Alternatif',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Email Alternative',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Email Lain',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Email Tambahan',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Email Alt',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Email',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else isnull(di_alamat,'') end end end end end end end end end end
      , CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' ') 
      ,100)
      as CompanyAddress,
      REPLACE(
      case when (len(di_kota)=0) then '0'
          when isnumeric(di_kota)=1 then isnull(cast(convert(int, k.kode) as varchar(7)),'0')--isnull(cast(di_kota as varchar(7)),cast(di_kota as varchar(30)))
          when (k.kode is null) then isnull(k.kode, '0')
          when (di_kota is null) then '9999'	
      else isnull(cast(convert(int, k.kode) as varchar(7)),'0')  
      end
      ,'.0','') as CompanyCityName, --ini name apa code??
      case when (len(di_kota)=0) then ''
          when isnumeric(di_kota)=1 then isnull(cast(di_kota as varchar(30)),cast(di_kota as varchar(30)))
          when (k.kode is null) then isnull(di_kota, '')
          when (di_kota is null) then ''	
      else isnull(cast(di_kota as varchar(30)),'') end CityRHB,
      case when (len(di_kodepos)=0) then '0' else isnull(di_kodepos, '0')  end CompanyPostalCode,
      ISNULL(cast(di_telp as varchar), '') [TeleponBisnis], 
      ISNULL(cast(di_fax as varchar), '') [CompanyFax], 
      ISNULL(di_email, '') [CompanyMail], 
      case when LEN(ISNULL(di_email, '')) = 0 then '1' else '2' end StatementType,

        ISNULL(b1map.PAR_VAL_RDO, '') as [NamaBank1],  
        case isnull(b1map.PAR_VAL_RDO, '')
            when '' then ''
                else 'ID'
            end as [BankCountry1],  
        ISNULL(b.cabang, '') as [BankBranchName1], 
        case when isnull(b1map.PAR_VAL_RDO, '') = '' then '' 
            else case when isnull(b.currency, 'IDR') = 'IDR' then '1'
                      when isnull(b.currency, 'IDR') = 'USD' then '2'
                      when isnull(b.currency, 'IDR') = 'EUR' then '3'
                else '4' --lainnya
                end
        end as [MataUang1], 
        dbo.stripID(ISNULL(b.no_rek, '')) as [NomorRekening1], 
        ISNULL(b.pemilik, '') as [NamaNasabah1],
      
        ISNULL(b2map.PAR_VAL_RDO, '') as [NamaBank2],  
        case isnull(b2map.PAR_VAL_RDO, '')
        when '' then ''
            else 'ID'
        end as [BankCountry2],  
        ISNULL(d.cabang, '') as [BankBranchName2],
        case when isnull(b2map.PAR_VAL_RDO, '') = '' then '' 
            else case when isnull(d.currency, 'IDR') = 'IDR' then '1'
                      when isnull(d.currency, 'IDR') = 'USD' then '2'
                      when isnull(d.currency, 'IDR') = 'EUR' then '3'
                else '4' --lainnya
                end 
        end as [MataUang2], 
        dbo.StripID(ISNULL(d.no_rek, '')) as [NomorRekening2], 
        ISNULL(d.pemilik, '') as [NamaNasabah2],

        ISNULL(b3map.PAR_VAL_RDO, '') as [NamaBank3],  
        case isnull(b3map.PAR_VAL_RDO, '')
        when '' then ''
            else 'ID'
        end as [BankCountry3], 
        ISNULL(f.cabang, '') as [BankBranchName3], 
        case when ISNULL(b3map.PAR_VAL_RDO, '') = '' then '' 
            else case when isnull(f.currency, 'IDR') = 'IDR' then '1'
                      when isnull(f.currency, 'IDR') = 'USD' then '2'
                      when isnull(f.currency, 'IDR') = 'EUR' then '3'
                else '4' --lainnya
                end
        end as [MataUang3], 
        dbo.StripID(ISNULL(f.no_rek, '')) as [NomorRekening3], 
        ISNULL(f.pemilik, '') as [NamaNasabah3],
        isnull(isnull(ifua.IFUA_NO, ifua_s.IFUA_NO),'')IFUA_NO,
		case when isnull(ifua.IFUA_NO, '') = '' and isnull(ifua_s.IFUA_NO, '') <> '' then '1' else '0' end bitissuspend,
		ISNULL(mcc.ISO_CODE, '') CountryofEstablishment,
		cast(a.AD_NO_AKTA_BERDIRI as varchar(150)) ArticlesofAssociationNo,
		'3' InvestorsRiskProfile, --sementara di default sampe ada tambahan di BO
		case di_domisili_kantor 
			when '0' then '1'
			when '1' then '2'
		else '2' end [AssetOwner] 
		from FPR a
		left join fpr_keuangan_pt fkeu on fkeu.FPR_ID=a.urutan
		left join aria_provinsi k on a.di_kota=k.[kotamadya/kabupaten]
		--left join aria_provinsi m on a.dd_kota_tinggal=m.[kotamadya/kabupaten]
		left join fpr_bank_sinvest_temp b on b.fpr_id = a.urutan and b.SEQ_NO = 1
		left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) c on b.bank = c.BANK_ID
		left join SINVEST_BI_MBR_CD n on c.MAP_ID = n.BI_CD
		left join RDO_PAR_MAP b1map on b1map.PAR_VAL_MYBO = n.BI_CD and b1map.PAR_ID = 'MSTR_BANK'
		left join fpr_bank_sinvest_temp d on d.fpr_id = a.urutan and d.SEQ_NO = 2       
		left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) e on d.bank = e.BANK_ID
		left join SINVEST_BI_MBR_CD o on e.MAP_ID = o.BI_CD
		left join RDO_PAR_MAP b2map on b2map.PAR_VAL_MYBO = o.BI_CD and b2map.PAR_ID = 'MSTR_BANK'
		left join fpr_bank_sinvest_temp f on f.fpr_id = a.urutan and f.SEQ_NO = 3   
		left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) g on f.bank = g.BANK_ID
		left join SINVEST_BI_MBR_CD p on g.MAP_ID = p.BI_CD
		left join RDO_PAR_MAP b3map on b3map.PAR_VAL_MYBO = p.BI_CD and b3map.PAR_ID = 'MSTR_BANK'
		left join (
			select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
            from cust_cif a
            inner join cust_product b on a.CIF_ID = b.CIF_ID
            inner join SINVEST_IFUA_MAP c on b.Niaga_acc = c.niaga_acc 
				and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
				and c.IFUA_NO not like '%T%'
            group by a.CIF_ID
		)ifua on a.cif_sms = ifua.cif_id
		left join (
            select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
            from cust_cif a
            inner join cust_product b on a.CIF_ID = b.CIF_ID
            inner join SINVEST_IFUA_MAP_delete c on b.Niaga_acc = c.niaga_acc 
              and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
              and c.IFUA_NO not like '%T%'
              and c.NOTES like '%suspend%'
            group by a.CIF_ID
		) ifua_s on a.CIF_SMS = ifua_s.CIF_ID
		inner join (
			select fprid, max(Approval_Date) Apv_Date
            from trx_fpr_temp 
            where approval_by is not null 
				and (valid=9 and approval_status=1
					or valid=0 and approval_status=0
                    or valid=77 and approval_status=1) 
            group by fprid
		) q on q.fprid = a.urutan
		left join map_cifsinvest_country mcc on mcc.CIF_CODE = a.HR_DI_NEGARA
		where a.JENIS_REKENING = '1'
			and isnull(a.SINVEST_FLAG_KYC, 0) = 0  
			and exists (select 1 from vw_cust_cif_sam_rfrrl cs where cs.cif_id = a.cif_sms)
			and DATEADD(dd, DATEDIFF(dd, 0, q.Apv_Date), 0) between dbo.getprevTradingday(getdate()) and getDate()
		order by a.urutan desc

