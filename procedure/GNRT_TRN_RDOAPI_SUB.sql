CREATE PROCEDURE [dbo].[GNRT_TRN_RDOAPI_SUB](
	@date_from datetime,
	@date_to datetime
)

as

declare
	@profile_id VARCHAR(100),
	@AUDIT_PRG VARCHAR(100)

set @profile_id = 'DN_S-INVEST_001'
set @AUDIT_PRG = 'SCRIPT'
--set @date_from = '20211222'
--set @date_to = '20211222'


insert into SINVEST_DN001(URUTAN_MSTR, FLAG_TYPE, FLAG)
  select urutan, 'AUTODEBET_KARYAWAN', 0
    from upload_autodebet_karyawan a
   where tgl_upload between @date_from and @date_to
     and not exists (select 1 
                       from SINVEST_DN001 x
                      where x.FLAG_TYPE = 'AUTODEBET_KARYAWAN'
                        --and x.FLAG = 1
                        and x.URUTAN_MSTR = a.urutan)

  insert into SINVEST_DN001(URUTAN_MSTR, FLAG_TYPE, FLAG)
  select b.ID_Detail, 'AUTODEBET_DNMS_RP', 0
    from aude_letter a
   inner join aude_detail b on a.id_letter=b.id_letter 
   where a.Tipe in ('S', 'R')
     and a.TglSurat between @date_from and @date_to
     and not exists (select 1 
                       from SINVEST_DN001 x
                      where x.FLAG_TYPE = 'AUTODEBET_DNMS_RP'
                        --and x.FLAG = 1
                        and x.URUTAN_MSTR = b.ID_Detail)
	and a.posted = '1'


	  insert into SINVEST_DN001(URUTAN_MSTR, FLAG_TYPE, FLAG)
  select b.ID_Detail, 'AUTODEBET_DIVIDEN', 0
    from audi_letter a
   inner join audi_detail b on a.id_letter=b.id_letter 
   where a.Tipe in ('S')
     and a.TglSurat between @date_from and @date_to
     and not exists (select 1 
                       from SINVEST_DN001 x
                      where x.FLAG_TYPE = 'AUTODEBET_DIVIDEN'
                        --and x.FLAG = 1
                        and x.URUTAN_MSTR = b.ID_Detail)
	and a.posted = '1'

	
    INSERT INTO [dbo].[DN_RDOAPI]
        ([AUDIT_USR],
		[AUDIT_PRG],
        [AUDIT_DT],
        [PRFL_ID],
        [TRN_DT],
        [TRN_TYPE],
        [SA_CODE],
        [IFUA_NO],
        [FUND_CODE],
        [AMOUNT_NOMINAL],
        [AMOUNT_UNIT],
        [AMOUNT_ALLUNIT],
        [FEE_NOMINAL],
        [FEE_UNIT],
        [FEE_PCT],
        [SWC_FUND_CODE],
        [RED_PYMNT_ACC_SEQ_CODE],
        [RED_PYMNT_BANK_BIC_CODE],
        [RED_PYMNT_BANK_BI_MBR_CODE],
        [RED_PYMNT_BANK_ACC_NO],
        [PYMNT_DT],
        [TRNSFR_TYPE],
        [SA_REFF_NO],
        [SourceOfFund],
        [BankRecipient],
        [TYPE],
        [ApprovedTime],
        [TrxUnitPaymentProvider],
        [TrxUnitPaymentType],
        BANKDETAIL)
    (
	select 'SYSTEM' as AUDIT_USR,
		@AUDIT_PRG as AUDIT_PRG,
		GETDATE() as AUDIT_DT,
		@profile_id as PRFL_ID,
		CONVERT(VARCHAR(10), a.tgl_trans, 120) as TRN_DT, 
		case a.jns_trans when 'B' then 'SUB' when 'S' then 'RED' end as TRN_TYPE,
		'DH002' as SA_CODE,
		isnull(g.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
		c.MAP_ID as FUND_CODE,
		case b.rp 
			when 0 then '0'
			when 1 then '0'
			when 2 then replace(convert(varchar(30), cast(case when a.jns_trans = 'B' then b.jml_bersih else b.jml_kotor end as money), 0),'.00','')
		end 
		as AMOUNT_NOMINAL,
		case b.rp 
			when 0 then s.good_fund_value
			when 1 then ltrim(STR(b.jml_kotor,26,4))--replace(convert(varchar(30), b.jml_kotor, 0),'.00','')
			when 2 then ''
		end
		as AMOUNT_UNIT,
		case b.rp 
			when 0 then 'FULL'
			when 1 then ''
			when 2 then ''
		end
		as AMOUNT_ALLUNIT,
		dbo.IsZeroString(
		case b.rp 
			when 0 then ''
			when 1 then ''
			when 2 then replace(convert(varchar(30), b.fee, 0),'.00','')
		end 
		,'')
		as FEE_NOMINAL,
		'' as FEE_UNIT,
		dbo.IsZeroString(
		case b.rp 
			when 0 then replace(convert(varchar(30), b.fee, 0),'.00','')
			when 1 then replace(convert(varchar(30), b.fee, 0),'.00','')
			when 2 then ''
		end
		,'')
		as FEE_PCT,
		'' [SWC_FUND_CODE],
		'' as RED_PYMNT_ACC_SEQ_CODE,
		'' as RED_PYMNT_BANK_BIC_CODE,
		case a.jns_trans 
			when 'B' then '' 
			when 'S' then e.map_id
		end 
		as RED_PYMNT_BANK_BI_MBR_CODE,
		case a.jns_trans 
			when 'B' then '' 
			when 'S' then dbo.StripID(b.no_rek) 
		end 
		as RED_PYMNT_BANK_ACC_NO,
		case a.jns_trans 
			when 'B' then '' 
			when 'S' then CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tgl_trans), 112)
		end 
		as PYMNT_DT,
		case a.jns_trans 
			when 'B' then '' 
			when 'S' then '2' 
		end 
		as TRNSFR_TYPE,
		'TC_'+cast(b.tc_detail_id as varchar) as SA_REFF_NO,
		'1' SourceOfFund,
		'0' BankRecipient, 
		'' [Type],
		b.approve_ho_date ApprovedTime,
		'' TrxUnitPaymentProvider,
		'' TrxUnitPaymentType,
        case when a.jns_trans = 'B' then b.cara else '' end BANKDETAIL
		from transaksi_cabang a (nolock)
		inner join transaksi_cabang_detail b (nolock) on a.tc_header_id = b.tc_header_id
		left join saldo_daily_reksadana_ALL s (nolock) on a.niaga_acc = s.niaga_acc and b.product_code = s.product_code and s.date_trans = dbo.GetPrevTradingDay(a.tgl_trans)
		inner join NFS_PRD_MAP c (nolock) on b.product_code = c.PRD_ID
		inner join no_surat_detail d (nolock) on b.tc_detail_id = d.tc_detail_id
		left join (select bank_id, map_id 
					from nfs_bank_map (nolock)
					group by bank_id, map_id) e on b.bank_rek = e.BANK_ID
		left join SINVEST_IFUA_MAP g (nolock) on d.niaga_acc = g.niaga_acc 
									and isnull(d.aperd_kode,'') = isnull(g.aperd_kode,'')
		left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
					max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
					from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
					where a.Niaga_acc = b.NIAGA_ACC
					and b.IFUA_NO is not null
					and a.cif_id = c.CIF_ID
					and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
					group by a.cif_id) h on h.CIF_ID = a.cif_id
		where a.jns_trans in ('B'/*, 'S'*/)
		and c.PRD_TYP = 'FND'
		and isnull(d.aperd_kode, '') = ''
		and b.status_drowdown = 'S'
		and a.tgl_trans between @date_from and @date_to
		--and isnull(d.sinvest_flag, 0) = 0 
		and not exists (
			select 1
			from [DN_RDOAPI] dn (nolock)
			where LEFT(dn.SA_REFF_NO, 3) = 'TC_'
				and replace(dn.SA_REFF_NO, 'TC_', '')  = b.tc_detail_id 

		)
    
		UNION ALL

		-- MANUAL
		select 'SYSTEM' as AUDIT_USR,
			@AUDIT_PRG as AUDIT_PRG,
			GETDATE() as AUDIT_DT,
			@profile_id as PRFL_ID,
			CONVERT(VARCHAR(10), a.tgl, 120) as TRN_DT, 
			case when a.jenis like '%Pembelian%' then 'SUB' 
				when a.jenis like '%Penjualan%' then 'RED' 
			end as TRN_TYPE,     
			'DH002' as SA_CODE,
			isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
			c.MAP_ID as FUND_CODE,
			case when a.jenis like '%Pembelian%' then replace(convert(varchar(30), a.jumlah, 0),'.00','')
			else
				case a.rp 
				when 0 then replace(convert(varchar(30), a.jumlah, 0),'.00','')
				else
					case when a.jenis like '%Rp%' then replace(convert(varchar(30), a.jumlah, 0),'.00','')
						when a.jenis like '%Unit(S)%' then ''
						else ''
					end 
				end  
			end  
			as AMOUNT_NOMINAL,
			case a.rp 
				when 0 then ''
				else
				case when a.jenis like '%Rp%' then ''
					when a.jenis like '%Unit(S)%' then s.good_fund_value
					else ltrim(STR(a.jumlah,26,4))--replace(convert(varchar(30), a.jumlah, 0),'.00','')
				end 
			end 
			as AMOUNT_UNIT,
			case when a.jenis like '%Rp%' then ''
				when a.jenis like '%Unit(S)%' then 'FULL'
				else ''
			end
			as AMOUNT_ALLUNIT,
			dbo.IsZeroString(
			case when a.jenis like '%Pembelian%' then replace(convert(varchar(30), a.jmlfee, 0),'.00','')
			else
				case when a.jenis like '%Rp%' then replace(convert(varchar(30), a.jmlfee, 0),'.00','')
					when a.jenis like '%Unit(S)%' then ''
					else ''
				end
			end  
			,'')
			as FEE_NOMINAL,
			'' as FEE_UNIT,
			dbo.IsZeroString(
			case when a.jenis like '%Rp%' then ''
				when a.jenis like '%Unit(S)%' then replace(convert(varchar(30), a.fee, 0),'.00','')
				else replace(convert(varchar(30), a.fee, 0),'.00','')
			end 
			,'')
			as FEE_PCT,
			'' [SWC_FUND_CODE],
			'' as RED_PYMNT_ACC_SEQ_CODE,
			'' as RED_PYMNT_BANK_BIC_CODE,
			case when a.jenis like '%Pembelian%' then '' 
				when a.jenis like '%Penjualan%' then e.map_id   
			end 
			as RED_PYMNT_BANK_BI_MBR_CODE,
			case when a.jenis like '%Pembelian%' then '' 
				when a.jenis like '%Penjualan%' then dbo.StripID(a.bank_account)   
			end 
			as RED_PYMNT_BANK_ACC_NO,
			case when a.jenis like '%Pembelian%' then '' 
				when a.jenis like '%Penjualan%' then CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tgl), 112)   
			end
			as PYMNT_DT,
			case when a.jenis like '%Pembelian%' then '' 
				when a.jenis like '%Penjualan%' then '2'
			end 
			as TRNSFR_TYPE,
			'MNLI_' + cast(a.urutan as varchar) as SA_REFF_NO,
			'1' SourceOfFund,
			'0' BankRecipient, 
			'' [Type],
			a.CRT_DT ApprovedTime,
			'' TrxUnitPaymentProvider,
			'' TrxUnitPaymentType,
            case when a.jenis like '%Pembelian%' then a.Transfer else '' end BANKDETAIL
		from no_surat_detail a
		inner join NFS_PRD_MAP c on a.kode_tr = c.PRD_ID
		left join saldo_daily_reksadana_ALL s on a.niaga_acc = s.niaga_acc and a.Kode_tr = s.product_code and s.date_trans = dbo.GetPrevTradingDay(a.tgl)
		left join (select bank_id, map_id 
						from nfs_bank_map
						group by bank_id, map_id) e on a.bank_name = e.BANK_ID   
		left join SINVEST_IFUA_MAP b on a.niaga_acc = b.niaga_acc 
									and isnull(a.aperd_kode,'') = isnull(b.aperd_kode,'')
		left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
					max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
					from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
					where a.Niaga_acc = b.NIAGA_ACC
					and b.IFUA_NO is not null
					and a.cif_id = c.CIF_ID
					and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
					group by a.cif_id) h on h.CIF_ID = a.cif
		where a.tc_detail_id is null
		and a.auto_detail_id is null
		--and a.category_trx = '10001'
		and (isnumeric(right(a.niaga_acc,5)) = 1 
			or isnull(a.niaga_acc,'') = '' 
			or a.niaga_acc = 'Nasabah Baru'
			or a.niaga_acc in (select ifua_no from SINVEST_IFUA_MAP))
		and (a.jenis like '%Pembelian%' /*or a.jenis like '%Penjualan%'*/)
		and c.PRD_TYP = 'FND'
		and isnull(a.aperd_kode, '') = ''
		and a.tgl between @date_from and @date_to
		--and isnull(a.sinvest_flag, 0) = 0
		and not exists (
			select 1
			from [DN_RDOAPI] dn
			where LEFT(dn.SA_REFF_NO, 5) = 'MNLI_'
				and replace(dn.SA_REFF_NO, 'MNLI_', '') = a.urutan 
		)
    
		UNION ALL

		-- AUTODEBET
		select 'SYSTEM' as AUDIT_USR,
			@AUDIT_PRG as AUDIT_PRG,
			GETDATE() as AUDIT_DT,
			@profile_id as PRFL_ID,
		   -- @PRFL_SEQ_NO as PRFL_SEQ_NO, 
			CONVERT(VARCHAR(10), a.tgl, 120) as TRN_DT, 
			'SUB' as TRN_TYPE,     
			'DH002' as SA_CODE,
			isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
			c.MAP_ID as FUND_CODE,
			replace(convert(varchar(30), a.jumlah, 0),'.00','') as AMOUNT_NOMINAL,
			'' as AMOUNT_UNIT,
			'' as AMOUNT_ALLUNIT,
			dbo.IsZeroString(replace(convert(varchar(30), a.JmlFee, 0),'.00',''),'') as FEE_NOMINAL,
			'' as FEE_UNIT,
			'' as FEE_PCT,
			'' [SWC_FUND_CODE],
			'' as RED_PYMNT_ACC_SEQ_CODE,
			'' as RED_PYMNT_BANK_BIC_CODE,
			'' as RED_PYMNT_BANK_BI_MBR_CODE,
			'' as RED_PYMNT_BANK_ACC_NO,
			'' as PYMNT_DT,
			'' as TRNSFR_TYPE,
			'AUTO_DBT_' + cast(a.urutan as varchar) as SA_REFF_NO,
			'1' SourceOfFund,
			'0' BankRecipient, 
			'' [Type],
			a.CRT_DT ApprovedTime,
			'' TrxUnitPaymentProvider,
			'' TrxUnitPaymentType,
            a.Transfer BANKDETAIL
		from no_surat_detail a
		inner join NFS_PRD_MAP c on a.kode_tr = c.PRD_ID
		left join (select bank_id, map_id 
						from nfs_bank_map
						group by bank_id, map_id) e on a.bank_name = e.BANK_ID
		left join SINVEST_IFUA_MAP b on a.niaga_acc = b.niaga_acc 
									and isnull(a.aperd_kode,'') = isnull(b.aperd_kode,'')
		left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
					max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
					from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
					where a.Niaga_acc = b.NIAGA_ACC
					and b.IFUA_NO is not null
					and a.cif_id = c.CIF_ID
					and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
					group by a.cif_id) h on h.CIF_ID = a.cif
		where a.auto_detail_id is not null
			and a.tc_detail_id is null
			and a.jenis = 'Autodebet Unit'
			and c.PRD_TYP = 'FND'
			and (a.aperd_kode is null or a.aperd_kode = '')
			and a.tgl between @date_from and @date_to
			--and isnull(a.sinvest_flag, 0) = 0   
			and not exists (
				select 1
				from [DN_RDOAPI] dn
				where LEFT(dn.SA_REFF_NO, 9) = 'AUTO_DBT_'
					and LEFT(dn.SA_REFF_NO, 10) not in ('AUTO_DBT_D', 'AUTO_DBT_K')
					and replace(dn.SA_REFF_NO, 'AUTO_DBT_', '') = a.urutan 

		)

		UNION ALL
    
		-- AUTODEBET KARYAWAN
		select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,
			GETDATE() as AUDIT_DT,
			@profile_id as PRFL_ID,
			CONVERT(VARCHAR(10), a.tgl_upload, 120) as TRN_DT, 
			'SUB' as TRN_TYPE,     
			'DH002' as SA_CODE,
			isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
			c.MAP_ID as FUND_CODE,
			replace(convert(varchar(30), a.nominal, 0),'.00','') as AMOUNT_NOMINAL,
			'' as AMOUNT_UNIT,
			'' as AMOUNT_ALLUNIT,
			'' as FEE_NOMINAL,
			'' as FEE_UNIT,
			'' as FEE_PCT,
			'' [SWC_FUND_CODE],
			'' as RED_PYMNT_ACC_SEQ_CODE,
			'' as RED_PYMNT_BANK_BIC_CODE,
			'' as RED_PYMNT_BANK_BI_MBR_CODE,
			'' --dbo.StripID(a.norek) 
			as RED_PYMNT_BANK_ACC_NO,
			'' --CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tgl_upload), 112) 
			as PYMNT_DT,
			'' TRNSFR_TYPE,
			'AUTO_DBT_KRYWN_' + cast(a.urutan as varchar)  as SA_REFF_NO,
			'1' SourceOfFund,
			'0' BankRecipient, 
			'' [Type],
			a.tgl_upload ApprovedTime,
			'' TrxUnitPaymentProvider,
			'' TrxUnitPaymentType,
            '' BANKDETAIL
		from upload_autodebet_karyawan a
		inner join NFS_PRD_MAP c on a.kd_reksadana = c.PRD_ID
		left join (
			select bank_id, map_id 
			from nfs_bank_map
			group by bank_id, map_id
		) e on upper(a.bank) = upper(e.BANK_ID)
		left join cust_product d on a.niaga_acc = d.Niaga_acc and d.product_code = a.kd_reksadana
		left join (
			select imap.ifua_no, cc.cif_id
			from sinvest_ifua_map imap (nolock), cust_product cp (nolock), cust_cif cc (nolock)
			where imap.niaga_acc = cp.niaga_acc
				and isnull(imap.aperd_kode, '') = isnull(cc.aperd_kode, '')
				and cp.cif_id = cc.cif_id
				and isnull(cc.aperd_kode, '') = ''
			group by imap.ifua_no, cc.cif_id
		)ifua on a.niaga_acc = ifua.ifua_no
		left join cust_cif f on f.CIF_ID = ISNULL(d.CIF_ID, ifua.cif_id)
		left join SINVEST_IFUA_MAP b on a.niaga_acc = b.niaga_acc 
									and isnull(f.aperd_kode,'') = isnull(b.aperd_kode,'')
		left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
					max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
					from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
					where a.Niaga_acc = b.NIAGA_ACC
					and b.IFUA_NO is not null
					and a.cif_id = c.CIF_ID
					and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
					group by a.cif_id) h on h.CIF_ID = f.CIF_ID
		where c.PRD_TYP = 'FND'
			and a.tgl_upload between @date_from and @date_to
			and  exists (select 1 
								from SINVEST_DN001 x
							where x.FLAG_TYPE = 'AUTODEBET_KARYAWAN'
								and x.FLAG = 0
								and x.URUTAN_MSTR = a.urutan)
			and not exists (
				select 1
				from [DN_RDOAPI] dn (nolock)
				where LEFT(dn.SA_REFF_NO, 15) = 'AUTO_DBT_KRYWN_'
					and replace(dn.SA_REFF_NO, 'AUTO_DBT_KRYWN_', '') = a.urutan 
			)


		UNION ALL

		-- AUTODEBET DANAMAS RUPIAH
		select 'SYSTEM' as AUDIT_USR,
			@AUDIT_PRG as AUDIT_PRG,
			GETDATE() as AUDIT_DT,
			@profile_id as PRFL_ID,
			CONVERT(VARCHAR(10), a.TglSurat, 120) as TRN_DT, 
			case when a.Tipe = 'S' then 'SUB'
				 when a.Tipe = 'R' then 'RED'
			end TRN_TYPE,     
			'DH002' as SA_CODE,
			isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
			c.MAP_ID as FUND_CODE,
			case f.allunit 
				when 1 then ''
				when 0 then replace(convert(varchar(30), f.Nominal, 0),'.00','')
			end 
			as AMOUNT_NOMINAL,
			case f.allunit 
				when 1 then s.good_fund_value
				when 0 then ''
			end 
			as AMOUNT_UNIT,
			case f.allunit 
				when 1 then 'FULL'
				when 0 then ''
			end
			as AMOUNT_ALLUNIT,
			'' as FEE_NOMINAL,
			'' as FEE_UNIT,
			'' as FEE_PCT,
			'' [SWC_FUND_CODE],
			'' as RED_PYMNT_ACC_SEQ_CODE,
			'' as RED_PYMNT_BANK_BIC_CODE,
			case a.Tipe 
				when 'S' then '' 
				when 'R' then e.map_id
			end
			as RED_PYMNT_BANK_BI_MBR_CODE,
			case a.Tipe 
				when 'S' then '' 
				when 'R' then dbo.StripID(bc.investoraccount)
			end
			as RED_PYMNT_BANK_ACC_NO,
			case a.Tipe 
				when 'S' then '' 
				when 'R' then CONVERT(VARCHAR(10), a.TglTransfer, 112)
			end
			as PYMNT_DT,
			case a.Tipe 
				when 'S' then ''
				when 'R' then '2' 
			end  
			as TRNSFR_TYPE,
			'AUTO_DBT_DNRP_' + cast(f.Id_Detail as varchar) as SA_REFF_NO,
			'1' SourceOfFund,
			'0' BankRecipient, 
			'' [Type],
			a.tglsurat ApprovedTime,
			'' TrxUnitPaymentProvider,
			'' TrxUnitPaymentType,
            '' BANKDETAIL
		from aude_letter a 
		inner join aude_detail f on a.id_letter=f.id_letter 
		left join saldo_daily_reksadana_ALL s on s.niaga_acc = f.NoInvestor and s.product_code = f.ProductCode and s.date_trans = dbo.GetPrevTradingDay(a.TglSurat)
		--inner join aude_refBank ba on isnull(a.BankRef,1)=ba.AutoID
		left join aude_refnasabah bb on f.noInvestor=bb.niagaCode 
		left join [172.251.1.17].S21_DH.dbo.Client bc on bb.S21_code=bc.ClientID
		inner join NFS_PRD_MAP c on a.productcode = c.PRD_ID
		left join (select bank_id, map_id 
						from nfs_bank_map
					   group by bank_id, map_id) e 
					   --on upper(ba.NamaBank) = upper(e.BANK_ID)
					   on upper(bc.savingsbankname) = upper(e.BANK_ID)
		left join cust_product g on f.noinvestor = g.niaga_acc and a.productcode = g.Product_code
		left join cust_cif d on g.cif_id = d.CIF_ID 
		left join SINVEST_IFUA_MAP b on f.noinvestor = b.niaga_acc 
									 and isnull(d.aperd_kode,'') = isnull(b.aperd_kode,'')
		left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
					 max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
					 from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
					 where a.Niaga_acc = b.NIAGA_ACC
					 and b.IFUA_NO is not null
					 and a.cif_id = c.CIF_ID
					 and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
					 group by a.cif_id) h on h.CIF_ID = d.cif_id
		where a.Tipe in ('S'/*, 'R'*/)
			and c.PRD_TYP = 'FND'
			and a.TglSurat between @date_from and @date_to
			and  exists (
				select 1 
				from SINVEST_DN001 x
				where x.FLAG_TYPE = 'AUTODEBET_DNMS_RP'
					and x.FLAG = 0
					and x.URUTAN_MSTR = f.ID_Detail) 
			and a.Posted = 1     
			and not exists (
				select 1
				from [DN_RDOAPI] dn (nolock)
				where LEFT(dn.SA_REFF_NO, 14) = 'AUTO_DBT_DNRP_'
				  and replace(dn.SA_REFF_NO, 'AUTO_DBT_DNRP_', '') = f.Id_Detail
			)      
    
		UNION ALL

		 -- AUTODEBET DIVIDEN
		 select 'SYSTEM' as AUDIT_USR,
			@AUDIT_PRG as AUDIT_PRG,
			GETDATE() as AUDIT_DT,
			@profile_id as PRFL_ID,
			CONVERT(VARCHAR(10), a.TglSurat, 120) as TRN_DT, 
			'SUB' as TRN_TYPE,     
			'DH002' as SA_CODE,
			isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
			c.MAP_ID as FUND_CODE,
			replace(convert(varchar(30), f.nominal, 0),'.00','') as AMOUNT_NOMINAL,
			'' as AMOUNT_UNIT,
			'' as AMOUNT_ALLUNIT,
			'' as FEE_NOMINAL,
			'' as FEE_UNIT,
			'' as FEE_PCT,
			'' [SWC_FUND_CODE],
			'' as RED_PYMNT_ACC_SEQ_CODE,
			'' as RED_PYMNT_BANK_BIC_CODE,
			'' as RED_PYMNT_BANK_BI_MBR_CODE,
			'' --dbo.StripID(a.norek) 
			as RED_PYMNT_BANK_ACC_NO,
			'' --CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tgl_upload), 112) 
			as PYMNT_DT,
			'' TRNSFR_TYPE,
			'AUTO_DBT_DIV_' + cast(f.Id_Detail as varchar) as SA_REFF_NO,
			'1' SourceOfFund,
			'0' BankRecipient, 
			'' [Type],
			a.tglsurat ApprovedTime,
			'' TrxUnitPaymentProvider,
			'' TrxUnitPaymentType,
            '' BANKDETAIL
		 from audi_letter a 
		 inner join audi_detail f on a.id_letter=f.id_letter 
		 left join audi_refnasabah bb on f.noInvestor=bb.niagaCode 
		 left join [172.251.1.17].S21_DH.dbo.Client bc on bb.S21_code=bc.ClientID
		 inner join NFS_PRD_MAP c on a.productcode = c.PRD_ID
		 left join (select bank_id, map_id 
						from nfs_bank_map
					   group by bank_id, map_id) e 
					   on upper(bc.savingsbankname) = upper(e.BANK_ID)
		 left join cust_product g on f.noinvestor = g.niaga_acc and a.productcode = g.Product_code
		 left join cust_cif d on g.cif_id = d.CIF_ID 
		 left join SINVEST_IFUA_MAP b on f.noinvestor = b.niaga_acc 
									 and isnull(d.aperd_kode,'') = isnull(b.aperd_kode,'')
		 left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
					 max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
					 from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
					 where a.Niaga_acc = b.NIAGA_ACC
					 and b.IFUA_NO is not null
					 and a.cif_id = c.CIF_ID
					 and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
					 group by a.cif_id) h on h.CIF_ID = d.cif_id
		 where a.Tipe in ('S')
			and c.PRD_TYP = 'FND'
			and a.TglSurat between @date_from and @date_to
			and  exists (select 1 
                    from SINVEST_DN001 x
                   where x.FLAG_TYPE = 'AUTODEBET_DIVIDEN'
                     and x.FLAG = 0
                     and x.URUTAN_MSTR = f.ID_Detail) 
			and a.Posted = 1  
			and not exists (
					select 1
					from [DN_RDOAPI] dn (nolock)
					where LEFT(dn.SA_CODE, 13) = 'AUTO_DBT_DIV_'
					  and replace(dn.SA_REFF_NO, 'AUTO_DBT_DIV_', '') = f.Id_Detail
				)    
    
		UNION ALL

		-- ROL
		select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID,/*@PRFL_SEQ_NO as PRFL_SEQ_NO,*/
		CONVERT(VARCHAR(10), a.tanggal_approve_spv, 120) as TRN_DT, 
		case a.jenis_transaksi when 'S' then 'SUB' when 'R' then 'RED' end as TRN_TYPE,     
		'DH002' as SA_CODE,
		isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
		c.MAP_ID as FUND_CODE,
		case a.jenis_nominal 
			when 1 then ''
			when 2 then ''
			when 3 then replace(cast(a.nilai_transaksi as varchar(30)) , '.00', '')
		end 
		as AMOUNT_NOMINAL,
		case a.jenis_nominal 
			when 1 then STR(a.nilai_transaksi, 25, 8)--replace(convert(varchar(30), a.nilai_transaksi, 0),'.00','')
			when 2 then STR(s.good_fund_value, 25, 8)
			when 3 then ''
		end
		as AMOUNT_UNIT,
		case a.jenis_nominal 
			when 1 then ''
			when 2 then 'FULL'
			when 3 then ''
		end
		as AMOUNT_ALLUNIT,
		dbo.IsZeroString(
		case a.jenis_nominal 
			when 1 then ''
			when 2 then ''
			when 3 then replace(convert(varchar(30), a.biaya_transaksi, 0),'.00','')
		end 
		,'')
		as FEE_NOMINAL,
		'' as FEE_UNIT,
		dbo.IsZeroString(
		case a.jenis_nominal 
			when 1 then replace(convert(varchar(30), a.persen_biaya_transaksi, 0),'.00','')
			when 2 then replace(convert(varchar(30), a.persen_biaya_transaksi, 0),'.00','')
			when 3 then ''
		end
		,'')
		as FEE_PCT,
		'' [SWC_FUND_CODE],
		'' as RED_PYMNT_ACC_SEQ_CODE,
		'' as RED_PYMNT_BANK_BIC_CODE,
		case a.jenis_transaksi 
			when 'S' then '' 
			when 'R' then e.map_id
		end
		as RED_PYMNT_BANK_BI_MBR_CODE,
		case a.jenis_transaksi 
			when 'S' then '' 
			when 'R' then dbo.StripID(a.no_rek) 
		end
		as RED_PYMNT_BANK_ACC_NO,
		case a.jenis_transaksi 
			when 'S' then '' 
			when 'R' then CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tanggal_approve_spv), 112) 
		end
		as PYMNT_DT,
		case a.jenis_transaksi 
			when 'S' then '' 
			when 'R' then '2' 
		end  
		as TRNSFR_TYPE,
		'SF_'+a.trans_id as SA_REFF_NO,
		'1' SourceOfFund,
		'0' BankRecipient, 
		'' [Type],
		a.tanggal_approve_spv ApprovedTime,
		'' TrxUnitPaymentProvider,
		'' TrxUnitPaymentType,
        case when a.jenis_transaksi = 'S' then a.bank else '' end BANKDETAIL
		from data_rol a
		inner join NFS_PRD_MAP c on a.product_code = c.PRD_ID
		left join saldo_daily_reksadana_ALL s on a.niaga_acc = s.niaga_acc and a.product_code = s.product_code and s.date_trans = dbo.GetPrevTradingDay(a.tanggal_approve_spv)
		left join (select bank_id, map_id 
						from nfs_bank_map
						group by bank_id, map_id) e on upper(a.bank) = upper(e.BANK_ID)
		left join cust_cif d on a.cif_id = d.CIF_ID 
		left join SINVEST_IFUA_MAP b on a.niaga_acc = b.niaga_acc 
									and isnull(d.aperd_kode,'') = isnull(b.aperd_kode,'')
		left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
					max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
					from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
					where a.Niaga_acc = b.NIAGA_ACC
					and b.IFUA_NO is not null
					and a.cif_id = c.CIF_ID
					and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
					group by a.cif_id) h on h.CIF_ID = a.cif_id
		where a.jenis_transaksi in ('S'/*, 'R'*/)
		and a.status='2'
		and c.PRD_TYP = 'FND'
		and CONVERT(VARCHAR(10), a.tanggal_approve_spv, 101) between @date_from and @date_to
		--and isnull(a.sinvest_flag, 0) = 0
			and not exists (
				select 1
				from [DN_RDOAPI] dn (nolock)
				where  LEFT(dn.SA_REFF_NO, 3) = 'SF_'
					and replace(dn.SA_REFF_NO, 'SF_', '') = a.trans_id
			) 

		UNION ALL

		-- VOUCHER ULTAH
		select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID,/*@PRFL_SEQ_NO as PRFL_SEQ_NO, */
			CONVERT(VARCHAR(10), a.APV_DRWDWN_DT, 120) as TRN_DT, 
			'SUB' as TRN_TYPE,     
			'DH002' as SA_CODE,
			isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
			c.MAP_ID as FUND_CODE,
			replace(convert(varchar(30), convert(money, a.nom_voucher), 0),'.00','') as AMOUNT_NOMINAL,
			'' as AMOUNT_UNIT,
			'' as AMOUNT_ALLUNIT,
			'' as FEE_NOMINAL,
			'' as FEE_UNIT,
			'' as FEE_PCT,
			'' [SWC_FUND_CODE],
			'' as RED_PYMNT_ACC_SEQ_CODE,
			'' as RED_PYMNT_BANK_BIC_CODE,
			'' as RED_PYMNT_BANK_BI_MBR_CODE,
			'' as RED_PYMNT_BANK_ACC_NO,
			'' as PYMNT_DT,
			'' as TRNSFR_TYPE,
			'VCHR_ULTAH_' +  cast(a.id_dtl as varchar) as SA_REFF_NO,
			'1' SourceOfFund,
			'0' BankRecipient, 
			'' [Type],
			a.APV_DRWDWN_DT ApprovedTime,
			'' TrxUnitPaymentProvider,
			'' TrxUnitPaymentType,
            'PB BANK SINARMAS' BANKDETAIL
		from tbl_voc_ultah_detail a
		inner join tbl_voc_ultah_header d 
			on d.id_hdr = a.id_hdr 
		inner join NFS_PRD_MAP c on a.prd_id = c.PRD_ID
		left join cust_cif e on a.cif_id = e.cif_id
		left join SINVEST_IFUA_MAP b 
			on a.no_investor = b.niaga_acc 
		and isnull(e.aperd_kode,'') = isnull(b.aperd_kode,'')
		left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
					max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
					from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
					where a.Niaga_acc = b.NIAGA_ACC
					and b.IFUA_NO is not null
					and a.cif_id = c.CIF_ID
					and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
					group by a.cif_id) h on h.CIF_ID = a.cif_id
		where d.eff_date is not null
			and a.APV_DRWDWN_DT is not null
			and c.PRD_TYP = 'FND'
			and a.APV_DRWDWN_DT between @date_from and @date_to
			--and isnull(a.sinvest_flag,0) = 0
			and not exists (
				select 1
				from [DN_RDOAPI] dn (nolock)
				where LEFT(dn.SA_REFF_NO, 11) =  'VCHR_ULTAH_'
					and cast(replace(dn.SA_REFF_NO, 'VCHR_ULTAH_', '') as int) = a.id_dtl
			) 
    
		UNION ALL

		--Pembayaran Komisi ke reksadana Danamas Rupiah Plus
		select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID,/*@PRFL_SEQ_NO as PRFL_SEQ_NO, */
		CONVERT(VARCHAR(10), a.APV_DRWDWN_DT, 120) as TRN_DT, 
			'SUB' as TRN_TYPE,     
			'DH002' as SA_CODE,
			isnull(dbo.GET_IFUA_NO_BY_CIF_ID(a.cif_id),'') AS IFUA_NO,
			d.MAP_ID as FUND_CODE,
			replace(convert(varchar(30), convert(money,a.CMSN_NET_AMNT), 0),'.00','') as AMOUNT_NOMINAL,
			'' as AMOUNT_UNIT,
			'' as AMOUNT_ALLUNIT,
			'' as FEE_NOMINAL,
			'' as FEE_UNIT,
			'' as FEE_PCT,
			'' [SWC_FUND_CODE],
			'' as RED_PYMNT_ACC_SEQ_CODE,
			'' as RED_PYMNT_BANK_BIC_CODE,
			'' as RED_PYMNT_BANK_BI_MBR_CODE,
			'' as RED_PYMNT_BANK_ACC_NO,
			'' as PYMNT_DT,
			'' as TRNSFR_TYPE,
			'TRN_AGENT_CMSN_' + cast(a.autoid as varchar) as SA_REFF_NO,
			'1' SourceOfFund,
			'0' BankRecipient, 
			'' [Type],
			a.APV_DRWDWN_DT ApprovedTime,
			'' TrxUnitPaymentProvider,
			'' TrxUnitPaymentType,
            'PB Sinarmas' BANKDETAIL
		FROM MUTUAL_FUND_AGENT_CMSN_IFUA a
		INNER JOIN NFS_PRD_MAP d on a.prd_id = d.PRD_ID
		WHERE /*isnull(a.SINVEST_FLAG , 0) = 0
			and */ISNULL(a.APV_DRWDWN_DT, '1/1/1900') <> '1/1/1900'
			and a.CMSN_IFUA_STS = 1
			and a.APV_DRWDWN_DT between @date_from and @date_to
			and not exists (
				select 1
				from [DN_RDOAPI] dn (nolock)
				where LEFT(dn.SA_REFF_NO, 15) = 'TRN_AGENT_CMSN_'
					and cast(replace(dn.SA_REFF_NO, 'TRN_AGENT_CMSN_', '') as int) = a.autoid
			) 
        

		UNION ALL
		-- CashBack SIP
		select 'SYSTEM' as AUDIT_USR,
			@AUDIT_PRG as AUDIT_PRG,
			GETDATE() as AUDIT_DT,
			@profile_id as PRFL_ID,
			CONVERT(VARCHAR(10), a.TglProcess, 120) as TRN_DT, 
			'SUB' as TRN_TYPE,     
			'DH002' as SA_CODE,
			a.IFUA_NO AS IFUA_NO,
			c.MAP_ID as FUND_CODE,
			replace(convert(varchar(30), convert(money, a.NominalCashBack), 0),'.00','') as AMOUNT_NOMINAL,
			'' as AMOUNT_UNIT,
			'' as AMOUNT_ALLUNIT,
			'' as FEE_NOMINAL,
			'' as FEE_UNIT,
			'' as FEE_PCT,
			'' [SWC_FUND_CODE],
			'' as RED_PYMNT_ACC_SEQ_CODE,
			'' as RED_PYMNT_BANK_BIC_CODE,
			'' as RED_PYMNT_BANK_BI_MBR_CODE,
			'' as RED_PYMNT_BANK_ACC_NO,
			'' as PYMNT_DT,
			'' as TRNSFR_TYPE,
			'VCHR_CASHBACK_SIP_' + cast(a.autoid as varchar) as SA_REFF_NO,
			'1' SourceOfFund,
			'0' BankRecipient, 
			'' [Type],
			a.lastupdate ApprovedTime,
			'' TrxUnitPaymentProvider,
			'' TrxUnitPaymentType,
            '' BANKDETAIL
		from Report_CashBack_SIP a
		inner join NFS_PRD_MAP c on a.jenisprodukcashback = c.PRD_ID
		left join cust_cif e on a.cif_id = e.cif_id
		where A.tglprocess is not null
			and c.PRD_TYP = 'FND'
			and a.tglprocess between @date_from and @date_to
			--and isnull(a.sinvestflag,0) = 0
			and not exists (
				select 1
				from [DN_RDOAPI] dn (nolock)
				where LEFT(dn.SA_REFF_NO, 18) = 'VCHR_CASHBACK_SIP_'
					and replace(dn.SA_REFF_NO, 'VCHR_CASHBACK_SIP_', '') = a.autoid
			) 
		)





GO
