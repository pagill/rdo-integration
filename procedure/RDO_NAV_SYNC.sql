CREATE PROCEDURE [dbo].[RDO_NAV_SYNC](
--declare 
	@ps_FUND_CODE varchar(50),
	@ps_NAB float,
	@ps_NAV_DT varchar(20),
	@ps_FUND_SIZE float
)

as

declare @vs_PRD_ID varchar(10),
	@vn_ERR_STS int,
	@vs_ERR_MSG varchar(150),
    @PROC_STS varchar(5),
    @ERR_STS varchar(5),
    @ERR_MSG varchar(500),
    @AUTOID int,
    @RSLT_MSG varchar(500);

-----------------------------------------------------------
select @vs_PRD_ID = prd_id
from NFS_PRD_MAP (nolock)
where map_id = @ps_FUND_CODE


-----------------------------------------------------------
  set @PROC_STS = '0'
  set @ERR_STS = '0'
  set @ERR_MSG = ''
 

INSERT INTO dbo.RDO_NAV_SYNC_LOG(
    AUDIT_USR, AUDIT_PRG, AUDIT_DT, PRFL_ID, NAV_DT, FUND_CODE, NAV, OUTSTANDING_UNIT
)
VALUES
(
    'SYSTEM', 'RDO_NAV_SYNC', GETDATE(), 'NAV', @ps_NAV_DT, @ps_FUND_CODE, @ps_NAB, @ps_FUND_SIZE
    
)

set @AUTOID = SCOPE_IDENTITY()

IF @@ERROR <> 0
BEGIN
    SET @ERR_STS = '1'
    SET @ERR_MSG = 'Error'
END

------------------------------------------------------------
IF @ERR_STS = 0
BEGIN

    IF exists (
        select 1
        from info_rkdn ir
        where info_id = @vs_PRD_ID
            and NAB_DT = convert(datetime,@ps_NAV_DT)
    )
    BEGIN
        UPDATE INFO_RKDN
        set NAB = @ps_NAB,
            FUND_SIZE = @ps_FUND_SIZE
        WHERE INFO_ID = @vs_PRD_ID
            and NAB_DT = convert(datetime, @ps_NAV_DT)
    END
    ELSE
    BEGIn
        insert into INFO_RKDN (info_id, nab_dt, nab, FUND_SIZE, info_type)
        values
        (@vs_PRD_ID, @ps_NAV_DT, @ps_NAB, @ps_FUND_SIZE, 'NAB_RKDN')
    END

    IF @@ERROR <> 0
    BEGIN
      SET @ERR_STS = '1'
      SET @ERR_MSG = 'Error'
    END

END

if @ERR_STS = '0' 
begin
    set @RSLT_MSG = 'NAV for <b>' + @ps_FUND_CODE + '</b> successfully uploaded'
end
else
begin
    set @RSLT_MSG = @ERR_MSG
end

update RDO_NAV_SYNC_LOG
set PROC_STS = '1',
      ERR_STS = @ERR_STS,
      ERR_MSG = @ERR_MSG,
      RSLT_MSG = @RSLT_MSG
where AUTOID = @AUTOID 
GO
