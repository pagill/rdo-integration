CREATE PROCEDURE [dbo].[SINVEST_IFUA_PATCH_RDOAPI]
	-- Add the parameters for the stored procedure here
	@ps_SA_CODE varchar(5000),
    @ps_IFUA_NO varchar(5000),
    @ps_IFUA_NAME varchar(5000),
    @ps_SID varchar(5000),
    @ps_Client_Code varchar(5000),
    @ps_Investor_Type varchar(5000),
    @ps_Status varchar(5000),
    @pn_RSLT_STS int OUTPUT,
    @ps_RSLT_MSG varchar(5000) OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/*
	IF @SA_CODE = 'DH002'
	BEGIN
	SET @SA_CODE=''
	END
	DECLARE @CIF_SMS VARCHAR(30)

 
	SELECT @CIF_SMS=CIF_SMS FROM FPR WHERE Urutan=CAST(@CLIENT_CODE as int)
	--RETURN @CIF_SMS 
	EXEC SINVEST_IFUA_PATCH2 @CIF_SMS,@IFUA_NO,@CLIENT_SID,@SA_CODE
	
	INSERT INTO SINVEST_IFUA_PATCH_RDOAPI_LOG(CIF_SMS,CLIENT_CODE,IFUA_NO,CLIENT_SID,SA_CODE,UPDT_DT) VALUES (@CIF_SMS,@CLIENT_CODE,@IFUA_NO,@CLIENT_SID,@SA_CODE,GETDATE())

	SELECT @CIF_SMS as CIF_SMS
	*/

	

		declare @AUDIT_USR varchar(5000),
				@AUDIT_PRG varchar(5000),
				@PRFL_ID varchar(30),
				@PRFL_SEQ_NO int,
				@ROW_NO int,
				@SA_Name varchar(5000),
				@First_Name varchar(5000),
				@Middle_Name varchar(5000),
				@Last_Name varchar(5000),
				@Country_of_Nationality varchar(5000),
				@Identification varchar(5000),
				@ID_No varchar(5000),
				@ID_Exp_Date varchar(5000),
				@NPWP_No varchar(5000),
				@NPWP_Reg_Date varchar(5000),
				@Country_of_Birth varchar(5000),
				@Place_of_Birth varchar(5000),
				@Date_of_Birth varchar(5000),
				@Gender varchar(5000),
				@Educational_Background varchar(5000),
				@Mothers_Maiden_Name varchar(5000),
				@Religion varchar(5000),
				@Occupation varchar(5000),
				@Income_Level varchar(5000),
				@Marital_Status varchar(5000),
				@Spouses_Name varchar(5000),
				@Investors_Risk_Profile varchar(5000),
				@Investment_Objective varchar(5000),
				@Source_of_Fund varchar(5000),
				@Asset_Owner varchar(5000),
				@KTP_Address varchar(5000),
				@KTP_City_Code varchar(5000),
				@KTP_Postal_Code varchar(5000),
				@Correspondence_Address varchar(5000),
				@Correspondence_City_Code varchar(5000),
				@Correspondence_City_Name varchar(5000),
				@Correspondence_Postal_Code varchar(5000),
				@Country_of_Correspondence varchar(5000),
				@Domicile_Address varchar(5000),
				@Domicile_City_Code varchar(5000),
				@Domicile_City_Name varchar(5000),
				@Domicile_Postal_Code varchar(5000),
				@Country_of_Domicile varchar(5000),
				@Home_Phone varchar(5000),
				@Mobile_Phone varchar(5000),
				@Facsimile varchar(5000),
				@Email varchar(5000),
				@Statement_Type varchar(5000),
				@FATCA varchar(5000),
				@TIN varchar(5000),
				@TIN_Issuance_Country varchar(5000),
				@Opening_Date varchar(5000),
				@Deactivation_Date varchar(5000),
				@RSLT_MSG varchar(5000),
				@ERR_STS int,
				@ERR_MSG varchar(5000),
				@AUTOID int

			declare --institution
				@COMPANY_NAME varchar(5000), 
				@SIUP_NO varchar(5000), 
				@SIUP_EXP_DATE varchar(5000), 
				@SKD_NO varchar(5000), 
				@SKD_EXP_DATE varchar(5000), 
				@COUNTRY_OF_ESTABLISHMENT varchar(5000), 
				@PLACE_OF_ESTABLISHMENT varchar(5000), 
				@DATE_OF_ESTABLISHMENT varchar(5000), 
				@ARTICLES_OF_ASSOCIATION_NO varchar(5000), 
				@COMPANY_TYPE varchar(5000), 
				@COMPANY_CHARACTERISTIC varchar(5000), 
				@COMPANY_ADDRESS varchar(5000), 
				@COMPANY_CITY_CODE varchar(5000), 
				@COMPANY_CITY_NAME varchar(5000), 
				@COMPANY_POSTAL_CODE varchar(5000), 
				@COUNTRY_OF_COMPANY varchar(5000), 
				@OFFICE_PHONE varchar(5000), 
				@AUTH_PERSON_1_FIRST_NAME varchar(5000), 
				@AUTH_PERSON_1_MIDDLE_NAME varchar(5000), 
				@AUTH_PERSON_1_LAST_NAME varchar(5000), 
				@AUTH_PERSON_1_POSITION varchar(5000), 
				@AUTH_PERSON_1_MOBILE_PHONE varchar(5000), 
				@AUTH_PERSON_1_EMAIL varchar(5000), 
				@AUTH_PERSON_1_NPWP_NO varchar(5000), 
				@AUTH_PERSON_1_KTP_NO varchar(5000), 
				@AUTH_PERSON_1_KTP_EXP_DT varchar(5000), 
				@AUTH_PERSON_1_PASSPORT_NO varchar(5000), 
				@AUTH_PERSON_1_PASSPORT_EXP_DT varchar(5000), 
				@AUTH_PERSON_2_FIRST_NAME varchar(5000), 
				@AUTH_PERSON_2_MIDDLE_NAME varchar(5000), 
				@AUTH_PERSON_2_LAST_NAME varchar(5000), 
				@AUTH_PERSON_2_POSITION varchar(5000), 
				@AUTH_PERSON_2_MOBILE_PHONE varchar(5000), 
				@AUTH_PERSON_2_EMAIL varchar(5000), 
				@AUTH_PERSON_2_NPWP_NO varchar(5000), 
				@AUTH_PERSON_2_KTP_NO varchar(5000), 
				@AUTH_PERSON_2_KTP_EXP_DT varchar(5000), 
				@AUTH_PERSON_2_PASSPORT_NO varchar(5000), 
				@AUTH_PERSON_2_PASSPORT_EXP_DT varchar(5000), 
				@ASSET_INFORMATION_1_YEAR varchar(5000), 
				@ASSET_INFORMATION_2_YEAR varchar(5000), 
				@ASSET_INFORMATION_3_YEAR varchar(5000), 
				@PROFIT_INFORMATION_1_YEAR varchar(5000), 
				@PROFIT_INFORMATION_2_YEAR varchar(5000), 
				@PROFIT_INFORMATION_3_YEAR varchar(5000), 
				@GIIN varchar(5000), 
				@SUBSTANTIAL_US_OWNER_NAME varchar(5000), 
				@SUBSTANTIAL_US_OWNER_ADDR varchar(5000), 
				@SUBSTANTIAL_US_OWNER_TIN varchar(5000)


			set @AUDIT_USR = 'SYSTEM'
			set @AUDIT_PRG = 'Radsoft callback'
			set @PRFL_SEQ_NO = ''
			set @ROW_NO = 1
			set @ERR_STS = 0
			set @ERR_MSG = ''
			set @RSLT_MSG = ''
			set @pn_RSLT_STS = 0
			set @ps_RSLT_MSG = 'SUCCESS'
    
			insert into RDO_IFUA_SID_log(
				AUDIT_USR,
				AUDIT_PRG,
				AUDIT_DT,
				SA_CODE,
				IFUA_NO,
				IFUA_NAME,
				SID ,
				Client_Code,
				Investor_Type,
				Status
			)values (
				@AUDIT_USR,
				@AUDIT_PRG,
				getDate(),
				@ps_SA_CODE,
				@ps_IFUA_NO,
				@ps_IFUA_NAME,
				@ps_SID,
				@ps_Client_Code,
				@ps_Investor_Type,
				@ps_Status
			)

			set @AUTOID = SCOPE_IDENTITY()

			IF EXISTS (
				SELECT 1
				from fpr (nolock)
				where urutan = @ps_Client_Code
			)
			BEGIN

				if @ps_Investor_Type = '1'
				BEGIN

					set @PRFL_ID = 'UP_S-INVEST_025'
    
					set @SA_Name = ''
					set @First_Name = ''
					set @Middle_Name = ''
					set @Last_Name = ''
					set @Country_of_Nationality = ''
					set @Identification = ''
					set @ID_No = ''
					set @ID_Exp_Date = ''
					set @NPWP_No = ''
					set @NPWP_Reg_Date = ''
					set @Country_of_Birth = ''
					set @Place_of_Birth = ''
					set @Date_of_Birth = ''
					set @Gender = ''
					set @Educational_Background = ''
					set @Mothers_Maiden_Name = ''
					set @Religion = ''
					set @Occupation = ''
					set @Income_Level = ''
					set @Marital_Status = ''
					set @Spouses_Name = ''
					set @Investors_Risk_Profile = ''
					set @Investment_Objective = ''
					set @Source_of_Fund = ''
					set @Asset_Owner = ''
					set @KTP_Address = ''
					set @KTP_City_Code = ''
					set @KTP_Postal_Code = ''
					set @Correspondence_Address = ''
					set @Correspondence_City_Code = ''
					set @Correspondence_City_Name = ''
					set @Correspondence_Postal_Code = ''
					set @Country_of_Correspondence = ''
					set @Domicile_Address = ''
					set @Domicile_City_Code = ''
					set @Domicile_City_Name = ''
					set @Domicile_Postal_Code = ''
					set @Country_of_Domicile = ''
					set @Home_Phone = ''
					set @Mobile_Phone = ''
					set @Facsimile = ''
					set @Email = ''
					set @Statement_Type = ''
					set @FATCA = ''
					set @TIN = ''
					set @TIN_Issuance_Country = ''
					set @Opening_Date = ''
					set @Deactivation_Date = ''

					exec UPDN_S_INVEST_025 @AUDIT_USR ,
						@AUDIT_PRG ,
						@PRFL_ID,
						@PRFL_SEQ_NO,
						@ROW_NO,
						@ps_SA_Code , --data dari radsoft
						@SA_Name ,
						@ps_SID , --data dari radsoft
						@ps_IFUA_No , --data dari radsoft
						@ps_IFUA_Name , --data dari radsoft
						@ps_Client_Code , --data dari radsoft
						@ps_Investor_Type , --data dari radsoft
						@First_Name ,
						@Middle_Name ,
						@Last_Name ,
						@Country_of_Nationality ,
						@Identification ,
						@ID_No ,
						@ID_Exp_Date ,
						@NPWP_No ,
						@NPWP_Reg_Date ,
						@Country_of_Birth ,
						@Place_of_Birth ,
						@Date_of_Birth ,
						@Gender ,
						@Educational_Background ,
						@Mothers_Maiden_Name ,
						@Religion ,
						@Occupation ,
						@Income_Level ,
						@Marital_Status ,
						@Spouses_Name ,
						@Investors_Risk_Profile ,
						@Investment_Objective ,
						@Source_of_Fund ,
						@Asset_Owner ,
						@KTP_Address ,
						@KTP_City_Code ,
						@KTP_Postal_Code ,
						@Correspondence_Address ,
						@Correspondence_City_Code ,
						@Correspondence_City_Name ,
						@Correspondence_Postal_Code ,
						@Country_of_Correspondence ,
						@Domicile_Address ,
						@Domicile_City_Code ,
						@Domicile_City_Name ,
						@Domicile_Postal_Code ,
						@Country_of_Domicile ,
						@Home_Phone ,
						@Mobile_Phone ,
						@Facsimile ,
						@Email ,
						@Statement_Type ,
						@FATCA ,
						@TIN ,
						@TIN_Issuance_Country ,
						@ps_Status , -- data dari radsoft
						@Opening_Date ,
						@Deactivation_Date ,
						@ps_RSLT_MSG  OUTPUT
				END 
				ELSE IF @ps_Investor_Type = '2'
				BEGIN
					set @PRFL_ID = 'UP_S-INVEST_026'

					set @SA_NAME = ''
					set @COMPANY_NAME = '' 
					set @COUNTRY_OF_DOMICILE = '' 
					set @SIUP_NO = '' 
					set @SIUP_EXP_DATE = '' 
					set @SKD_NO = '' 
					set @SKD_EXP_DATE = '' 
					set @NPWP_NO = '' 
					set @NPWP_REG_DATE = '' 
					set @COUNTRY_OF_ESTABLISHMENT = '' 
					set @PLACE_OF_ESTABLISHMENT = ''
					set @DATE_OF_ESTABLISHMENT = '' 
					set @ARTICLES_OF_ASSOCIATION_NO = '' 
					set @COMPANY_TYPE = ''
					set @COMPANY_CHARACTERISTIC = '' 
					set @INCOME_LEVEL = ''
					set @INVESTORS_RISK_PROFILE = '' 
					set @INVESTMENT_OBJECTIVE = ''
					set @SOURCE_OF_FUND = '' 
					set @ASSET_OWNER = ''
					set @COMPANY_ADDRESS = '' 
					set @COMPANY_CITY_CODE = ''
					set @COMPANY_CITY_NAME = ''
					set @COMPANY_POSTAL_CODE = ''
					set @COUNTRY_OF_COMPANY = ''
					set @OFFICE_PHONE = ''
					set @FACSIMILE = ''
					set @EMAIL = ''
					set @STATEMENT_TYPE = '' 
					set @AUTH_PERSON_1_FIRST_NAME = '' 
					set @AUTH_PERSON_1_MIDDLE_NAME = ''
					set @AUTH_PERSON_1_LAST_NAME = ''
					set @AUTH_PERSON_1_POSITION = ''
					set @AUTH_PERSON_1_MOBILE_PHONE = '' 
					set @AUTH_PERSON_1_EMAIL = ''
					set @AUTH_PERSON_1_NPWP_NO = ''
					set @AUTH_PERSON_1_KTP_NO = ''
					set @AUTH_PERSON_1_KTP_EXP_DT = '' 
					set @AUTH_PERSON_1_PASSPORT_NO = ''
					set @AUTH_PERSON_1_PASSPORT_EXP_DT = '' 
					set @AUTH_PERSON_2_FIRST_NAME = ''
					set @AUTH_PERSON_2_MIDDLE_NAME = ''
					set @AUTH_PERSON_2_LAST_NAME = ''
					set @AUTH_PERSON_2_POSITION = ''
					set @AUTH_PERSON_2_MOBILE_PHONE = '' 
					set @AUTH_PERSON_2_EMAIL = ''
					set @AUTH_PERSON_2_NPWP_NO = ''
					set @AUTH_PERSON_2_KTP_NO = ''
					set @AUTH_PERSON_2_KTP_EXP_DT = '' 
					set @AUTH_PERSON_2_PASSPORT_NO = ''
					set @AUTH_PERSON_2_PASSPORT_EXP_DT = '' 
					set @ASSET_INFORMATION_1_YEAR = ''
					set @ASSET_INFORMATION_2_YEAR = ''
					set @ASSET_INFORMATION_3_YEAR = ''
					set @PROFIT_INFORMATION_1_YEAR = ''
					set @PROFIT_INFORMATION_2_YEAR = ''
					set @PROFIT_INFORMATION_3_YEAR = ''
					set @FATCA = ''
					set @TIN = ''
					set @TIN_ISSUANCE_COUNTRY = ''
					set @GIIN = ''
					set @SUBSTANTIAL_US_OWNER_NAME = '' 
					set @SUBSTANTIAL_US_OWNER_ADDR = ''
					set @SUBSTANTIAL_US_OWNER_TIN = ''
					set @OPENING_DATE = ''
					set @DEACTIVATION_DATE = ''

					exec UPDN_S_INVEST_026 @AUDIT_USR,
						@AUDIT_PRG,
						@PRFL_ID ,
						@PRFL_SEQ_NO ,
						@ROW_NO ,
						@ps_SA_CODE, 
						@SA_NAME, 
						@ps_SID, 
						@ps_IFUA_NO, 
						@ps_IFUA_NAME, 
						@ps_CLIENT_CODE, 
						@ps_INVESTOR_TYPE, 
						@COMPANY_NAME, 
						@COUNTRY_OF_DOMICILE, 
						@SIUP_NO, 
						@SIUP_EXP_DATE, 
						@SKD_NO, 
						@SKD_EXP_DATE, 
						@NPWP_NO, 
						@NPWP_REG_DATE, 
						@COUNTRY_OF_ESTABLISHMENT, 
						@PLACE_OF_ESTABLISHMENT, 
						@DATE_OF_ESTABLISHMENT, 
						@ARTICLES_OF_ASSOCIATION_NO, 
						@COMPANY_TYPE, 
						@COMPANY_CHARACTERISTIC, 
						@INCOME_LEVEL, 
						@INVESTORS_RISK_PROFILE, 
						@INVESTMENT_OBJECTIVE, 
						@SOURCE_OF_FUND, 
						@ASSET_OWNER, 
						@COMPANY_ADDRESS, 
						@COMPANY_CITY_CODE, 
						@COMPANY_CITY_NAME, 
						@COMPANY_POSTAL_CODE, 
						@COUNTRY_OF_COMPANY, 
						@OFFICE_PHONE, 
						@FACSIMILE, 
						@EMAIL, 
						@STATEMENT_TYPE, 
						@AUTH_PERSON_1_FIRST_NAME, 
						@AUTH_PERSON_1_MIDDLE_NAME, 
						@AUTH_PERSON_1_LAST_NAME, 
						@AUTH_PERSON_1_POSITION, 
						@AUTH_PERSON_1_MOBILE_PHONE, 
						@AUTH_PERSON_1_EMAIL, 
						@AUTH_PERSON_1_NPWP_NO, 
						@AUTH_PERSON_1_KTP_NO, 
						@AUTH_PERSON_1_KTP_EXP_DT, 
						@AUTH_PERSON_1_PASSPORT_NO, 
						@AUTH_PERSON_1_PASSPORT_EXP_DT, 
						@AUTH_PERSON_2_FIRST_NAME, 
						@AUTH_PERSON_2_MIDDLE_NAME, 
						@AUTH_PERSON_2_LAST_NAME, 
						@AUTH_PERSON_2_POSITION, 
						@AUTH_PERSON_2_MOBILE_PHONE, 
						@AUTH_PERSON_2_EMAIL, 
						@AUTH_PERSON_2_NPWP_NO, 
						@AUTH_PERSON_2_KTP_NO, 
						@AUTH_PERSON_2_KTP_EXP_DT, 
						@AUTH_PERSON_2_PASSPORT_NO, 
						@AUTH_PERSON_2_PASSPORT_EXP_DT, 
						@ASSET_INFORMATION_1_YEAR, 
						@ASSET_INFORMATION_2_YEAR, 
						@ASSET_INFORMATION_3_YEAR, 
						@PROFIT_INFORMATION_1_YEAR, 
						@PROFIT_INFORMATION_2_YEAR, 
						@PROFIT_INFORMATION_3_YEAR, 
						@FATCA, 
						@TIN, 
						@TIN_ISSUANCE_COUNTRY, 
						@GIIN, 
						@SUBSTANTIAL_US_OWNER_NAME, 
						@SUBSTANTIAL_US_OWNER_ADDR, 
						@SUBSTANTIAL_US_OWNER_TIN, 
						@ps_STATUS, 
						@OPENING_DATE, 
						@DEACTIVATION_DATE, 
						@ps_RSLT_MSG OUTPUT
    
				END 
				ELSE
				BEGIN
					set @ERR_STS = 1
					set @ERR_MSG = 'Investor Type Tidak Sesuai'
					set @ps_RSLT_MSG = @ERR_MSG
					set @pn_RSLT_STS = 1
				END 

				if @@ERROR <> 0
				BEGIN
					set @ERR_STS = 1
					set @ERR_MSG = @@ERROR
					set @ps_RSLT_MSG = @ERR_MSG
					set @pn_RSLT_STS = 1
				END 

			END
			ELSE
			BEGIN
				set @ps_RSLT_MSG= 'CLIENTCODE DOES NOT EXISTS'
				set @pn_RSLT_STS = 1
			END

		update RDO_IFUA_SID_log
		set ERR_STS = @ERR_STS,
			ERR_MSG = @ERR_MSG,
			RSLT_MSG = @ps_RSLT_MSG
		where AUTOID = @AUTOID

		if CHARINDEX('Uploaded', @ps_RSLT_MSG) > 0
		BEGIN
			set @ps_RSLT_MSG= 'Success'
			set @pn_RSLT_STS = 0
		END
		ELSE
		BEGIN
			set @ps_RSLT_MSG= 'Failed'
			set @pn_RSLT_STS = 1
		END
END


GO
