CREATE VIEW [dbo].[KYC_RDOAPI]
  
  as

  select  'INDIVIDU' ACCNT_TYPE, 
      type, ClientCode FrontID , 
      ISNULL(IFUA_NO, '')IFUA_NO,
      FirstName NamaDepanInd, 
      MiddleName NamaTengahInd, 
      LastName NamaBelakangInd, 
      CountryofNationality Nationality, 
      IDNo NoIdentitasInd1,
      IdentitasInd1, 
      NPWPNo NPWP, 
      NPWPRegistrationDate RegistrationNPWP, 
      CountryofBirth, 
      PlaceofBirth TempatLahir, 
      DateofBirth TanggalLahir, 
      gender JenisKelamin, 
      EducationalBackground Pendidikan,
      MothersMaidenName MotherMaidenName, 
      Religion Agama, 
      Occupation Pekerjaan, 
      IncomeLevel Penghasilan, 
      MaritalStatus StatusPerkawinan, 
      SpousesName SpouseName, 
      InvestorsRiskProfile, 
      InvestmentObjective MaksudTujuan, 
      SourceofFund SumberDana, 
      AssetOwner, 
      KTPAddress OtherAlamatInd1, 
      KTPCityCode OtherKodeKotaInd1, 
      KTPPostalCode OtherKodePosInd1, 
      REPLACE(CorrespondenceAddress, '"', '''') AlamatInd1, 
      CorrespondenceCityCode KodeKotaInd1, 
      CorrespondencePostalCode KodePosInd1, 
      CountryofCorrespondence,
      Replace(DomicileAddress, '"', '''') AlamatInd2, 
      DomicileCityCode KodeKotaInd2, 
      DomicilePostalCode KodePosInd2, 
      CountryofDomicile,
      HomePhone TeleponRumah, 
      MobilePhone TeleponSelular, 
      Facsimile Fax, 
      Email, 
      StatementType, 
      FATCA, 
      TIN, 
      TINIssuanceCountry, 
      REDMPaymentBankName1 NamaBank1, 
      REDMPaymentBankCountry1 BankCountry1,
      REDMPaymentBankBranch1 BankBranchName1, 
      [REDMPaymentA/CCCY1] MataUang1, 
      [REDMPaymentA/CNo1] NomorRekening1, 
      [REDMPaymentA/CName1] NamaNasabah1,
      REDMPaymentBankName2 NamaBank2, 
      REDMPaymentBankCountry2 BankCountry2,
      REDMPaymentBankBranch2 BankBranchName2, 
      [REDMPaymentA/CCCY2] MataUang2, 
      [REDMPaymentA/CNo2] NomorRekening2, 
      [REDMPaymentA/CName2] NamaNasabah2,
      REDMPaymentBankName3 NamaBank3,
      REDMPaymentBankCountry3 BankCountry3,
      REDMPaymentBankBranch3 BankBranchName3, 
      [REDMPaymentA/CCCY3] MataUang3, 
      [REDMPaymentA/CNo3] NomorRekening3, 
      [REDMPaymentA/CName3] NamaNasabah3, 
      REPLACE(NamaKantor, '"', '''')NamaKantor, 
      AlamatKantorInd, 
      TeleponKantor, 
      JabatanKantor, 
      ISNULL(KodeKotaKantorInd, '9999')KodeKotaKantorInd, 
      KodePosKantorInd,
      BeneficialName,
      Politis, 
      PolitisRelation, 
      PolitisName,
      '' NamaPerusahaan, 
      '' NegaraDomisili, 
      '' NoSIUP, 
      '' SIUPExpirationDate, 
      '' NoSKD, 
      '' ExpiredDateSKD, 
      '' LokasiBerdiri, 
      '' TanggalBerdiri,
      '' CompanyType, 
      '' CompanyCharacteristic, 
      '' CompanyAddress, 
      '' CompanyCityName, 
      '' CityRHB, 
      '' CompanyPostalCode,
      '' TeleponBisnis, 
      '' CompanyFax, 
      PROC_STS,
      AUTOID,
	  ISNULL([bitissuspend], '0') bitissuspend,
	  convert(varchar(10), convert(datetime, IDExpirationDate, 112), 120) ExpiredDateIdentitasInd1
  from DN_RDOAPI_003
  where PROC_STS = 'N'
  --order by autoid desc 
              
union all

  select 'INSTITUSI' ACCNT_TYPE, 
      [type], 
      ClientCode FrontID , 
      ISNULL(IFUA_NO, '')IFUA_NO,
      '' NamaDepanInd, 
      '' NamaTengahInd, 
      ''NamaBelakangInd, 
      '' Nationality, 
      '' NoIdentitasInd1,
      '' IdentitasInd1, 
      NPWPNo NPWP, 
      RegistrationNPWP RegistrationNPWP, 
      '' CountryofBirth, 
      '' TempatLahir, 
      '' TanggalLahir, 
      '' JenisKelamin, 
      '' Pendidikan,
      '' MotherMaidenName, 
      '' Agama, 
      '' Pekerjaan, 
      IncomeLevel Penghasilan, 
      '' StatusPerkawinan, 
      '' SpouseName, 
      '' InvestorsRiskProfile, 
      InvestmentObjective MaksudTujuan, 
      SourceofFund SumberDana, 
      '' AssetOwner, 
      '' OtherAlamatInd1, 
      '' OtherKodeKotaInd1, 
      '' OtherKodePosInd1, 
  '' AlamatInd1, 
      '' KodeKotaInd1, 
      '' KodePosInd1, 
      '' CountryofCorrespondence,
      '' AlamatInd2, 
      '' KodeKotaInd2, 
      '' KodePosInd2, 
      '' CountryofDomicile,
      '' TeleponRumah, 
      '' TeleponSelular, 
      '' Fax, 
      CompanyMail Email, 
      StatementType, 
      '' FATCA, 
      '' TIN, 
      '' TINIssuanceCountry, 
      NamaBank1,
      BankCountry1,
      BankBranchName1, 
      MataUang1, 
      NomorRekening1, 
      NamaNasabah1,
      NamaBank2, 
      BankCountry2,
      BankBranchName2, 
      MataUang2,
      NomorRekening2, 
      NamaNasabah2,
      NamaBank3, 
      BankCountry3,
      BankBranchName3, 
      MataUang3, 
      NomorRekening3, 
      NamaNasabah3,
      '' NamaKantor, 
      '' AlamatKantorInd, 
      '' TeleponKantor, 
      '' JabatanKantor, 
      '' KodeKotaKantorInd, 
      '' KodePosKantorInd,
      '' BeneficialName,
      '' Politis, 
      '' PolitisRelation, 
      '' PolitisName, 
      NamaPerusahaan, 
      NegaraDomisili, 
      NoSIUP, 
      SIUPExpirationDate, 
      NoSKD, 
      ExpiredDateSKD, 
      PlaceOfEstablishment LokasiBerdiri, 
      DateofEstablishment TanggalBerdiri,
      CompanyType, 
      CompanyCharacteristic, 
      CompanyAddress, 
      CompanyCityName, 
      CityRHB, 
      CompanyPostalCode,
      TeleponBisnis, 
      CompanyFax, 
      PROC_STS,
      AUTOID,
	  ISNULL([bitissuspend], '0') bitissuspend,
	  '' ExpiredDateIdentitasInd1
  from DN_RDOAPI_004 
  where PROC_STS = 'N'
 -- order by autoid desc 