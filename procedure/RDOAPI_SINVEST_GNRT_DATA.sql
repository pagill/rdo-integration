CREATE PROCEDURE [dbo].[RDOAPI_SINVEST_GNRT_DATA] 
	@profile_id VARCHAR(100),
	@AUDIT_PRG VARCHAR(100),
--	@PRFL_SEQ_NO VARCHAR(100),
	@date_from DATE,
	@date_to DATE
AS

BEGIN

--SET NOCOUNT ON;


  IF @profile_id = 'DN_S-INVEST_001' 
  BEGIN
  BEGIN TRY
	
    INSERT INTO [dbo].[DN_RDOAPI]
        ([AUDIT_USR]
        ,[AUDIT_PRG]
        ,[AUDIT_DT]
        ,[PRFL_ID]
        --,[PRFL_SEQ_NO]
        ,[TRN_DT]
        ,[TRN_TYPE]
        ,[SA_CODE]
        ,[IFUA_NO]
        ,[FUND_CODE]
        ,[AMOUNT_NOMINAL]
        ,[AMOUNT_UNIT]
        ,[AMOUNT_ALLUNIT]
        ,[FEE_NOMINAL]
        ,[FEE_UNIT]
        ,[FEE_PCT]
        ,[SWC_FUND_CODE]
        ,[RED_PYMNT_ACC_SEQ_CODE]
        ,[RED_PYMNT_BANK_BIC_CODE]
        ,[RED_PYMNT_BANK_BI_MBR_CODE]
        ,[RED_PYMNT_BANK_ACC_NO]
        ,[PYMNT_DT]
        ,[TRNSFR_TYPE]
        ,[SA_REFF_NO]
        ,[SourceOfFund]
        ,[BankRecipient]
        ,[TYPE]
        ,[ApprovedTime]
        ,[TrxUnitPaymentProvider]
        ,[TrxUnitPaymentType]
        )


        
    (
   select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID, /*@PRFL_SEQ_NO as PRFLSEQ_NO,*/
    CONVERT(VARCHAR(10), a.tgl_trans, 120) as TRN_DT, 
    case a.jns_trans when 'B' then 'SUB' when 'S' then 'RED' end as TRN_TYPE,
    'DH002' as SA_CODE,
    isnull(g.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
    c.MAP_ID as FUND_CODE,
    case b.rp 
        when 0 then '0'
        when 1 then '0'
        when 2 then replace(convert(varchar(30), cast(b.jml_kotor as money), 0),'.00','')
    end 
    as AMOUNT_NOMINAL,
    case b.rp 
        when 0 then s.good_fund_value
        when 1 then ltrim(STR(b.jml_kotor,26,4))--replace(convert(varchar(30), b.jml_kotor, 0),'.00','')
        when 2 then ''
    end
    as AMOUNT_UNIT,
    case b.rp 
        when 0 then 'FULL'
        when 1 then ''
        when 2 then ''
    end
    as AMOUNT_ALLUNIT,
    dbo.IsZeroString(
    case b.rp 
        when 0 then ''
        when 1 then ''
        when 2 then replace(convert(varchar(30), b.fee, 0),'.00','')
    end 
    ,'')
    as FEE_NOMINAL,
    '' as FEE_UNIT,
    dbo.IsZeroString(
    case b.rp 
        when 0 then replace(convert(varchar(30), b.fee, 0),'.00','')
        when 1 then replace(convert(varchar(30), b.fee, 0),'.00','')
        when 2 then ''
    end
    ,'')
    as FEE_PCT,
    '' [SWC_FUND_CODE],
    '' as RED_PYMNT_ACC_SEQ_CODE,
    '' as RED_PYMNT_BANK_BIC_CODE,
    case a.jns_trans 
        when 'B' then '' 
        when 'S' then e.map_id
    end 
    as RED_PYMNT_BANK_BI_MBR_CODE,
    case a.jns_trans 
        when 'B' then '' 
        when 'S' then dbo.StripID(b.no_rek) 
    end 
    as RED_PYMNT_BANK_ACC_NO,
    case a.jns_trans 
        when 'B' then '' 
        when 'S' then CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tgl_trans), 112)
    end 
    as PYMNT_DT,
    case a.jns_trans 
        when 'B' then '' 
        when 'S' then '2' 
    end 
    as TRNSFR_TYPE,
    'TC_'+cast(b.tc_detail_id as varchar) as SA_REFF_NO,
    '1' SourceOfFund,
    '0' BankRecipient, 
    '' [Type],
    b.approve_ho_date ApprovedTime,
    '' TrxUnitPaymentProvider,
    '' TrxUnitPaymentType
    from transaksi_cabang a (nolock)
    inner join transaksi_cabang_detail b (nolock) on a.tc_header_id = b.tc_header_id
    left join saldo_daily_reksadana_ALL s (nolock) on a.niaga_acc = s.niaga_acc and b.product_code = s.product_code and s.date_trans = dbo.GetPrevTradingDay(a.tgl_trans)
    inner join NFS_PRD_MAP c (nolock) on b.product_code = c.PRD_ID
    inner join no_surat_detail d (nolock) on b.tc_detail_id = d.tc_detail_id
    left join (select bank_id, map_id 
                from nfs_bank_map (nolock)
                group by bank_id, map_id) e on b.bank_rek = e.BANK_ID
    left join SINVEST_IFUA_MAP g (nolock) on d.niaga_acc = g.niaga_acc 
                                and isnull(d.aperd_kode,'') = isnull(g.aperd_kode,'')
    left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
                max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
                from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
                where a.Niaga_acc = b.NIAGA_ACC
                and b.IFUA_NO is not null
                and a.cif_id = c.CIF_ID
                and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
                group by a.cif_id) h on h.CIF_ID = a.cif_id
    where a.jns_trans in ('B', 'S')
    and c.PRD_TYP = 'FND'
    and isnull(d.aperd_kode, '') = ''
    and b.status_drowdown = 'S'
    and a.tgl_trans between @date_from and @date_to
    --and isnull(d.sinvest_flag, 0) = 0 
    and not exists (
        select 1
        from [DN_RDOAPI] dn (nolock)
        where LEFT(dn.SA_REFF_NO, 3) = 'TC_'
            and replace(dn.SA_REFF_NO, 'TC_', '')  = b.tc_detail_id 

    )
    
    UNION ALL

    -- MANUAL
    select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID, /*@PRFL_SEQ_NO as PRFL_SEQ_NO,*/
      CONVERT(VARCHAR(10), a.tgl, 120) as TRN_DT, 
    case when a.jenis like '%Pembelian%' then 'SUB' 
        when a.jenis like '%Penjualan%' then 'RED' 
    end as TRN_TYPE,     
    'DH002' as SA_CODE,
    isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
    c.MAP_ID as FUND_CODE,
    case when a.jenis like '%Pembelian%' then replace(convert(varchar(30), a.JmlKotor, 0),'.00','')
    else
        case a.rp 
        when 0 then replace(convert(varchar(30), a.jumlah, 0),'.00','')
        else
            case when a.jenis like '%Rp%' then replace(convert(varchar(30), a.jumlah, 0),'.00','')
                when a.jenis like '%Unit(S)%' then ''
                else ''
            end 
        end  
    end  
    as AMOUNT_NOMINAL,
    case a.rp 
        when 0 then ''
        else
        case when a.jenis like '%Rp%' then ''
            when a.jenis like '%Unit(S)%' then s.good_fund_value
            else ltrim(STR(a.jumlah,26,4))--replace(convert(varchar(30), a.jumlah, 0),'.00','')
        end 
    end 
    as AMOUNT_UNIT,
    case when a.jenis like '%Rp%' then ''
        when a.jenis like '%Unit(S)%' then 'FULL'
        else ''
    end
    as AMOUNT_ALLUNIT,
    dbo.IsZeroString(
    case when a.jenis like '%Pembelian%' then replace(convert(varchar(30), a.jmlfee, 0),'.00','')
    else
        case when a.jenis like '%Rp%' then replace(convert(varchar(30), a.jmlfee, 0),'.00','')
            when a.jenis like '%Unit(S)%' then ''
            else ''
        end
    end  
    ,'')
    as FEE_NOMINAL,
    '' as FEE_UNIT,
    dbo.IsZeroString(
    case when a.jenis like '%Rp%' then ''
        when a.jenis like '%Unit(S)%' then replace(convert(varchar(30), a.fee, 0),'.00','')
        else replace(convert(varchar(30), a.fee, 0),'.00','')
    end 
    ,'')
    as FEE_PCT,
    '' [SWC_FUND_CODE],
    '' as RED_PYMNT_ACC_SEQ_CODE,
    '' as RED_PYMNT_BANK_BIC_CODE,
    case when a.jenis like '%Pembelian%' then '' 
        when a.jenis like '%Penjualan%' then e.map_id   
    end 
    as RED_PYMNT_BANK_BI_MBR_CODE,
    case when a.jenis like '%Pembelian%' then '' 
        when a.jenis like '%Penjualan%' then dbo.StripID(a.bank_account)   
    end 
    as RED_PYMNT_BANK_ACC_NO,
    case when a.jenis like '%Pembelian%' then '' 
        when a.jenis like '%Penjualan%' then CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tgl), 112)   
    end
    as PYMNT_DT,
    case when a.jenis like '%Pembelian%' then '' 
        when a.jenis like '%Penjualan%' then '2'
    end 
    as TRNSFR_TYPE,
    'MNLI_' + cast(a.urutan as varchar) as SA_REFF_NO,
    '1' SourceOfFund,
    '0' BankRecipient, 
    '' [Type],
    a.CRT_DT ApprovedTime,
    '' TrxUnitPaymentProvider,
    '' TrxUnitPaymentType
    from no_surat_detail a
    inner join NFS_PRD_MAP c on a.kode_tr = c.PRD_ID
    left join saldo_daily_reksadana_ALL s on a.niaga_acc = s.niaga_acc and a.Kode_tr = s.product_code and s.date_trans = dbo.GetPrevTradingDay(a.tgl)
    left join (select bank_id, map_id 
                    from nfs_bank_map
                    group by bank_id, map_id) e on a.bank_name = e.BANK_ID   
    left join SINVEST_IFUA_MAP b on a.niaga_acc = b.niaga_acc 
                                and isnull(a.aperd_kode,'') = isnull(b.aperd_kode,'')
    left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
                max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
                from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
                where a.Niaga_acc = b.NIAGA_ACC
                and b.IFUA_NO is not null
                and a.cif_id = c.CIF_ID
                and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
                group by a.cif_id) h on h.CIF_ID = a.cif
    where a.tc_detail_id is null
    and a.auto_detail_id is null
    --and a.category_trx = '10001'
    and (isnumeric(right(a.niaga_acc,5)) = 1 
        or isnull(a.niaga_acc,'') = '' 
        or a.niaga_acc = 'Nasabah Baru'
        or a.niaga_acc in (select ifua_no from SINVEST_IFUA_MAP))
    and (a.jenis like '%Pembelian%' or a.jenis like '%Penjualan%')
    and c.PRD_TYP = 'FND'
    and isnull(a.aperd_kode, '') = ''
    and a.tgl between @date_from and @date_to
    --and isnull(a.sinvest_flag, 0) = 0
    and not exists (
        select 1
        from [DN_RDOAPI] dn
        where LEFT(dn.SA_REFF_NO, 5) = 'MNLI_'
            and replace(dn.SA_REFF_NO, 'MNLI_', '') = a.urutan 

    )
    
    UNION ALL

    -- AUTODEBET
    select 'SYSTEM' as AUDIT_USR,
        @AUDIT_PRG as AUDIT_PRG,
        GETDATE() as AUDIT_DT,
        @profile_id as PRFL_ID,
       -- @PRFL_SEQ_NO as PRFL_SEQ_NO, 
        CONVERT(VARCHAR(10), a.tgl, 120) as TRN_DT, 
    'SUB' as TRN_TYPE,     
    'DH002' as SA_CODE,
    isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
    c.MAP_ID as FUND_CODE,
    replace(convert(varchar(30), a.jmlkotor, 0),'.00','') as AMOUNT_NOMINAL,
    '' as AMOUNT_UNIT,
    '' as AMOUNT_ALLUNIT,
    dbo.IsZeroString(replace(convert(varchar(30), a.JmlFee, 0),'.00',''),'') as FEE_NOMINAL,
    '' as FEE_UNIT,
    '' as FEE_PCT,
    '' [SWC_FUND_CODE],
    '' as RED_PYMNT_ACC_SEQ_CODE,
    '' as RED_PYMNT_BANK_BIC_CODE,
    '' as RED_PYMNT_BANK_BI_MBR_CODE,
    '' as RED_PYMNT_BANK_ACC_NO,
    '' as PYMNT_DT,
    '' as TRNSFR_TYPE,
    'AUTO_DBT_' + cast(a.urutan as varchar) as SA_REFF_NO,
    '1' SourceOfFund,
    '0' BankRecipient, 
    '' [Type],
    a.CRT_DT ApprovedTime,
    '' TrxUnitPaymentProvider,
    '' TrxUnitPaymentType
    from no_surat_detail a
    inner join NFS_PRD_MAP c on a.kode_tr = c.PRD_ID
    left join (select bank_id, map_id 
                    from nfs_bank_map
                    group by bank_id, map_id) e on a.bank_name = e.BANK_ID
    left join SINVEST_IFUA_MAP b on a.niaga_acc = b.niaga_acc 
                                and isnull(a.aperd_kode,'') = isnull(b.aperd_kode,'')
    left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
                max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
                from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
                where a.Niaga_acc = b.NIAGA_ACC
                and b.IFUA_NO is not null
                and a.cif_id = c.CIF_ID
                and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
                group by a.cif_id) h on h.CIF_ID = a.cif
    where a.auto_detail_id is not null
    and a.tc_detail_id is null
    and a.jenis = 'Autodebet Unit'
    and c.PRD_TYP = 'FND'
    and (a.aperd_kode is null or a.aperd_kode = '')
    and a.tgl between @date_from and @date_to
    --and isnull(a.sinvest_flag, 0) = 0   
    and not exists (
        select 1
        from [DN_RDOAPI] dn
        where LEFT(dn.SA_REFF_NO, 9) = 'AUTO_DBT_'
            and replace(dn.SA_REFF_NO, 'AUTO_DBT_', '') = a.urutan 

    )

    UNION ALL  
    
    -- AUTODEBET KARYAWAN
    select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID, /*@PRFL_SEQ_NO as PRFL_SEQ_NO,*/
    CONVERT(VARCHAR(10), a.tgl_upload, 120) as TRN_DT, 
    'SUB' as TRN_TYPE,     
    'DH002' as SA_CODE,
    isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
    c.MAP_ID as FUND_CODE,
    replace(convert(varchar(30), a.nominal, 0),'.00','') as AMOUNT_NOMINAL,
    '' as AMOUNT_UNIT,
    '' as AMOUNT_ALLUNIT,
    '' as FEE_NOMINAL,
    '' as FEE_UNIT,
    '' as FEE_PCT,
    '' [SWC_FUND_CODE],
    '' as RED_PYMNT_ACC_SEQ_CODE,
    '' as RED_PYMNT_BANK_BIC_CODE,
    '' as RED_PYMNT_BANK_BI_MBR_CODE,
    '' --dbo.StripID(a.norek) 
    as RED_PYMNT_BANK_ACC_NO,
    '' --CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tgl_upload), 112) 
    as PYMNT_DT,
    '' TRNSFR_TYPE,
    'AUTO_DBT_KRYWN_' + cast(a.urutan as varchar)  as SA_REFF_NO,
    '1' SourceOfFund,
    '0' BankRecipient, 
    '' [Type],
    a.tgl_upload ApprovedTime,
    '' TrxUnitPaymentProvider,
    '' TrxUnitPaymentType
    from upload_autodebet_karyawan a
    inner join NFS_PRD_MAP c on a.kd_reksadana = c.PRD_ID
    left join (select bank_id, map_id 
                    from nfs_bank_map
                    group by bank_id, map_id) e on upper(a.bank) = upper(e.BANK_ID)
    left join cust_product d on a.niaga_acc = d.Niaga_acc and d.product_code = a.kd_reksadana
    left join (
        select imap.ifua_no, cc.cif_id
        from sinvest_ifua_map imap (nolock), cust_product cp (nolock), cust_cif cc (nolock)
        where imap.niaga_acc = cp.niaga_acc
            and isnull(imap.aperd_kode, '') = isnull(cc.aperd_kode, '')
            and cp.cif_id = cc.cif_id
            and isnull(cc.aperd_kode, '') = ''
        group by imap.ifua_no, cc.cif_id
    )ifua on a.niaga_acc = ifua.ifua_no
    left join cust_cif f on f.CIF_ID = ISNULL(d.CIF_ID, ifua.cif_id)
    left join SINVEST_IFUA_MAP b on a.niaga_acc = b.niaga_acc 
                                and isnull(f.aperd_kode,'') = isnull(b.aperd_kode,'')
    left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
                max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
                from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
                where a.Niaga_acc = b.NIAGA_ACC
                and b.IFUA_NO is not null
                and a.cif_id = c.CIF_ID
                and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
                group by a.cif_id) h on h.CIF_ID = f.CIF_ID
    where c.PRD_TYP = 'FND'
    and a.tgl_upload between @date_from and @date_to
    and not exists (select 1 
                        from SINVEST_DN001 x
                    where x.FLAG_TYPE = 'AUTODEBET_KARYAWAN'
                        and x.FLAG = 1
                        and x.URUTAN_MSTR = a.urutan)
        and not exists (
            select 1
            from [DN_RDOAPI] dn (nolock)
            where LEFT(dn.SA_REFF_NO, 15) = 'AUTO_DBT_KRYWN_'
                and replace(dn.SA_REFF_NO, 'AUTO_DBT_KRYWN_', '') = a.urutan 
        )


    UNION ALL

     -- AUTODEBET DANAMAS RUPIAH
     select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID,/*@PRFL_SEQ_NO as PRFLSEQ_NO,*/
        CONVERT(VARCHAR(10), a.TglSurat, 120) as TRN_DT, 
        a.Tipe TRN_TYPE,     
        'DH002' as SA_CODE,
        isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
        c.MAP_ID as FUND_CODE,
        case f.allunit 
        when 1 then ''
        when 0 then replace(convert(varchar(30), f.Nominal, 0),'.00','')
        end 
        as AMOUNT_NOMINAL,
        case f.allunit 
        when 1 then s.good_fund_value
        when 0 then ''
        end 
        as AMOUNT_UNIT,
        case f.allunit 
        when 1 then 'FULL'
        when 0 then ''
        end
        as AMOUNT_ALLUNIT,
        '' as FEE_NOMINAL,
        '' as FEE_UNIT,
        '' as FEE_PCT,
        '' [SWC_FUND_CODE],
        '' as RED_PYMNT_ACC_SEQ_CODE,
        '' as RED_PYMNT_BANK_BIC_CODE,
        case a.Tipe 
        when 'S' then '' 
        when 'R' then e.map_id
        end
        as RED_PYMNT_BANK_BI_MBR_CODE,
        case a.Tipe 
        when 'S' then '' 
        when 'R' then dbo.StripID(bc.investoraccount)
        end
        as RED_PYMNT_BANK_ACC_NO,
        case a.Tipe 
        when 'S' then '' 
        when 'R' then CONVERT(VARCHAR(10), a.TglTransfer, 112)
        end
        as PYMNT_DT,
        case a.Tipe 
        when 'S' then ''
        when 'R' then '2' 
        end  
        as TRNSFR_TYPE,
        'AUTO_DBT_DNRP_' + cast(f.Id_Detail as varchar) as SA_REFF_NO,
        '1' SourceOfFund,
        '0' BankRecipient, 
        '' [Type],
        a.tglsurat ApprovedTime,
        '' TrxUnitPaymentProvider,
        '' TrxUnitPaymentType
    from aude_letter a 
     inner join aude_detail f on a.id_letter=f.id_letter 
     left join saldo_daily_reksadana_ALL s on s.niaga_acc = f.NoInvestor and s.product_code = f.ProductCode and s.date_trans = dbo.GetPrevTradingDay(a.TglSurat)
     --inner join aude_refBank ba on isnull(a.BankRef,1)=ba.AutoID
     left join aude_refnasabah bb on f.noInvestor=bb.niagaCode 
     left join [172.251.1.17].S21_DH.dbo.Client bc on bb.S21_code=bc.ClientID
     inner join NFS_PRD_MAP c on a.productcode = c.PRD_ID
     left join (select bank_id, map_id 
                    from nfs_bank_map
                   group by bank_id, map_id) e 
                   --on upper(ba.NamaBank) = upper(e.BANK_ID)
                   on upper(bc.savingsbankname) = upper(e.BANK_ID)
     left join cust_product g on f.noinvestor = g.niaga_acc and a.productcode = g.Product_code
     left join cust_cif d on g.cif_id = d.CIF_ID 
     left join SINVEST_IFUA_MAP b on f.noinvestor = b.niaga_acc 
                                 and isnull(d.aperd_kode,'') = isnull(b.aperd_kode,'')
     left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
                 max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
                 from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
                 where a.Niaga_acc = b.NIAGA_ACC
                 and b.IFUA_NO is not null
                 and a.cif_id = c.CIF_ID
                 and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
                 group by a.cif_id) h on h.CIF_ID = d.cif_id
     where a.Tipe in ('S', 'R')
     and c.PRD_TYP = 'FND'
     and a.TglSurat between @date_from and @date_to
     and not exists (select 1 
                       from SINVEST_DN001 x
                      where x.FLAG_TYPE = 'AUTODEBET_DNMS_RP'
                        and x.FLAG = 1
                        and x.URUTAN_MSTR = f.ID_Detail) 
     and a.Posted = 1     
        and not exists (
            select 1
            from [DN_RDOAPI] dn (nolock)
            where LEFT(dn.SA_REFF_NO, 14) = 'AUTO_DBT_DNRP_'
              and replace(dn.SA_REFF_NO, 'AUTO_DBT_DNRP_', '') = f.Id_Detail
        )      
    
    UNION ALL

     -- AUTODEBET DIVIDEN
     select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID,/*@PRFL_SEQ_NO as PRFLSEQ_NO,*/
        CONVERT(VARCHAR(10), a.TglSurat, 120) as TRN_DT, 
     'SUB' as TRN_TYPE,     
     'DH002' as SA_CODE,
     isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
     c.MAP_ID as FUND_CODE,
     replace(convert(varchar(30), f.nominal, 0),'.00','') as AMOUNT_NOMINAL,
     '' as AMOUNT_UNIT,
     '' as AMOUNT_ALLUNIT,
     '' as FEE_NOMINAL,
     '' as FEE_UNIT,
     '' as FEE_PCT,
     '' [SWC_FUND_CODE],
     '' as RED_PYMNT_ACC_SEQ_CODE,
     '' as RED_PYMNT_BANK_BIC_CODE,
     '' as RED_PYMNT_BANK_BI_MBR_CODE,
     '' --dbo.StripID(a.norek) 
     as RED_PYMNT_BANK_ACC_NO,
     '' --CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tgl_upload), 112) 
     as PYMNT_DT,
     '' TRNSFR_TYPE,
     'AUTO_DBT_DIV_' + cast(f.Id_Detail as varchar) as SA_REFF_NO,
    '1' SourceOfFund,
    '0' BankRecipient, 
    '' [Type],
    a.tglsurat ApprovedTime,
    '' TrxUnitPaymentProvider,
    '' TrxUnitPaymentType
     from audi_letter a 
     inner join audi_detail f on a.id_letter=f.id_letter 
     left join audi_refnasabah bb on f.noInvestor=bb.niagaCode 
     left join [172.251.1.17].S21_DH.dbo.Client bc on bb.S21_code=bc.ClientID
     inner join NFS_PRD_MAP c on a.productcode = c.PRD_ID
     left join (select bank_id, map_id 
                    from nfs_bank_map
                   group by bank_id, map_id) e 
                   on upper(bc.savingsbankname) = upper(e.BANK_ID)
     left join cust_product g on f.noinvestor = g.niaga_acc and a.productcode = g.Product_code
     left join cust_cif d on g.cif_id = d.CIF_ID 
     left join SINVEST_IFUA_MAP b on f.noinvestor = b.niaga_acc 
                                 and isnull(d.aperd_kode,'') = isnull(b.aperd_kode,'')
     left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
                 max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
                 from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
                 where a.Niaga_acc = b.NIAGA_ACC
                 and b.IFUA_NO is not null
                 and a.cif_id = c.CIF_ID
                 and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
                 group by a.cif_id) h on h.CIF_ID = d.cif_id
     where a.Tipe in ('S')
     and c.PRD_TYP = 'FND'
     and a.TglSurat between @date_from and @date_to
     and not exists (select 1 
                       from SINVEST_DN001 x
                      where x.FLAG_TYPE = 'AUTODEBET_DIVIDEN'
                        and x.FLAG = 1
                        and x.URUTAN_MSTR = f.ID_Detail) 
     and a.Posted = 1  
        and not exists (
            select 1
            from [DN_RDOAPI] dn (nolock)
            where LEFT(dn.SA_CODE, 13) = 'AUTO_DBT_DIV_'
              and replace(dn.SA_REFF_NO, 'AUTO_DBT_DIV_', '') = f.Id_Detail
        )    
    
    UNION ALL

    -- ROL
    select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID,/*@PRFL_SEQ_NO as PRFL_SEQ_NO,*/
    CONVERT(VARCHAR(10), a.tanggal_approve_spv, 120) as TRN_DT, 
    case a.jenis_transaksi when 'S' then 'SUB' when 'R' then 'RED' end as TRN_TYPE,     
    'DH002' as SA_CODE,
    isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
    c.MAP_ID as FUND_CODE,
    case a.jenis_nominal 
        when 1 then ''
        when 2 then ''
        when 3 then replace(cast(a.nilai_transaksi as varchar(30)) , '.00', '')
    end 
    as AMOUNT_NOMINAL,
    case a.jenis_nominal 
        when 1 then STR(a.nilai_transaksi, 25, 8)--replace(convert(varchar(30), a.nilai_transaksi, 0),'.00','')
        when 2 then STR(s.good_fund_value, 25, 8)
        when 3 then ''
    end
    as AMOUNT_UNIT,
    case a.jenis_nominal 
        when 1 then ''
        when 2 then 'FULL'
        when 3 then ''
    end
    as AMOUNT_ALLUNIT,
    dbo.IsZeroString(
    case a.jenis_nominal 
        when 1 then ''
        when 2 then ''
        when 3 then replace(convert(varchar(30), a.biaya_transaksi, 0),'.00','')
    end 
    ,'')
    as FEE_NOMINAL,
    '' as FEE_UNIT,
    dbo.IsZeroString(
    case a.jenis_nominal 
        when 1 then replace(convert(varchar(30), a.persen_biaya_transaksi, 0),'.00','')
        when 2 then replace(convert(varchar(30), a.persen_biaya_transaksi, 0),'.00','')
        when 3 then ''
    end
    ,'')
    as FEE_PCT,
    '' [SWC_FUND_CODE],
    '' as RED_PYMNT_ACC_SEQ_CODE,
    '' as RED_PYMNT_BANK_BIC_CODE,
    case a.jenis_transaksi 
        when 'S' then '' 
        when 'R' then e.map_id
    end
    as RED_PYMNT_BANK_BI_MBR_CODE,
    case a.jenis_transaksi 
        when 'S' then '' 
        when 'R' then dbo.StripID(a.no_rek) 
    end
    as RED_PYMNT_BANK_ACC_NO,
    case a.jenis_transaksi 
        when 'S' then '' 
        when 'R' then CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tanggal_approve_spv), 112) 
    end
    as PYMNT_DT,
    case a.jenis_transaksi 
        when 'S' then '' 
        when 'R' then '2' 
    end  
    as TRNSFR_TYPE,
    'SF_'+a.trans_id as SA_REFF_NO,
    '1' SourceOfFund,
    '0' BankRecipient, 
    '' [Type],
    a.tanggal_approve_spv ApprovedTime,
    '' TrxUnitPaymentProvider,
    '' TrxUnitPaymentType
    from data_rol a
    inner join NFS_PRD_MAP c on a.product_code = c.PRD_ID
    left join saldo_daily_reksadana_ALL s on a.niaga_acc = s.niaga_acc and a.product_code = s.product_code and s.date_trans = dbo.GetPrevTradingDay(a.tanggal_approve_spv)
    left join (select bank_id, map_id 
                    from nfs_bank_map
                    group by bank_id, map_id) e on upper(a.bank) = upper(e.BANK_ID)
    left join cust_cif d on a.cif_id = d.CIF_ID 
    left join SINVEST_IFUA_MAP b on a.niaga_acc = b.niaga_acc 
                                and isnull(d.aperd_kode,'') = isnull(b.aperd_kode,'')
    left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
                max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
                from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
                where a.Niaga_acc = b.NIAGA_ACC
                and b.IFUA_NO is not null
                and a.cif_id = c.CIF_ID
                and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
                group by a.cif_id) h on h.CIF_ID = a.cif_id
    where a.jenis_transaksi in ('S', 'R')
    and a.status='2'
    and c.PRD_TYP = 'FND'
    and CONVERT(VARCHAR(10), a.tanggal_approve_spv, 101) between @date_from and @date_to
    --and isnull(a.sinvest_flag, 0) = 0
        and not exists (
            select 1
            from [DN_RDOAPI] dn (nolock)
            where  LEFT(dn.SA_REFF_NO, 3) = 'SF_'
                and replace(dn.SA_REFF_NO, 'SF_', '') = a.trans_id
        ) 

    UNION ALL

    -- VOUCHER ULTAH
    select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID,/*@PRFL_SEQ_NO as PRFL_SEQ_NO, */
    CONVERT(VARCHAR(10), a.APV_DRWDWN_DT, 120) as TRN_DT, 
    'SUB' as TRN_TYPE,     
    'DH002' as SA_CODE,
    isnull(b.IFUA_NO,h.IFUA_NO) AS IFUA_NO,
    c.MAP_ID as FUND_CODE,
    replace(convert(varchar(30), convert(money, a.nom_voucher), 0),'.00','') as AMOUNT_NOMINAL,
    '' as AMOUNT_UNIT,
    '' as AMOUNT_ALLUNIT,
    '' as FEE_NOMINAL,
    '' as FEE_UNIT,
    '' as FEE_PCT,
    '' [SWC_FUND_CODE],
    '' as RED_PYMNT_ACC_SEQ_CODE,
    '' as RED_PYMNT_BANK_BIC_CODE,
    '' as RED_PYMNT_BANK_BI_MBR_CODE,
    '' as RED_PYMNT_BANK_ACC_NO,
    '' as PYMNT_DT,
    '' as TRNSFR_TYPE,
    'VCHR_ULTAH_' +  cast(a.id_dtl as varchar) as SA_REFF_NO,
    '1' SourceOfFund,
    '0' BankRecipient, 
    '' [Type],
    d.eff_date ApprovedTime,
    '' TrxUnitPaymentProvider,
    '' TrxUnitPaymentType
    from tbl_voc_ultah_detail a
    inner join tbl_voc_ultah_header d 
        on d.id_hdr = a.id_hdr 
    inner join NFS_PRD_MAP c on a.prd_id = c.PRD_ID
    left join cust_cif e on a.cif_id = e.cif_id
    left join SINVEST_IFUA_MAP b 
        on a.no_investor = b.niaga_acc 
    and isnull(e.aperd_kode,'') = isnull(b.aperd_kode,'')
    left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
                max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
                from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
                where a.Niaga_acc = b.NIAGA_ACC
                and b.IFUA_NO is not null
                and a.cif_id = c.CIF_ID
                and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
                group by a.cif_id) h on h.CIF_ID = a.cif_id
    where d.eff_date is not null
        and a.APV_DRWDWN_DT is not null
        and c.PRD_TYP = 'FND'
        and a.APV_DRWDWN_DT between @date_from and @date_to
        --and isnull(a.sinvest_flag,0) = 0 
        and not exists (
            select 1
            from [DN_RDOAPI] dn (nolock)
            where LEFT(dn.SA_REFF_NO, 11) =  'VCHR_ULTAH_'
                and cast(replace(dn.SA_REFF_NO, 'VCHR_ULTAH_', '') as int) = a.id_dtl
        ) 
    
    UNION ALL

    --Pembayaran Komisi ke reksadana Danamas Rupiah Plus
    select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID,/*@PRFL_SEQ_NO as PRFL_SEQ_NO, */
    CONVERT(VARCHAR(10), a.APV_DRWDWN_DT, 120) as TRN_DT, 
        'SUB' as TRN_TYPE,     
        'DH002' as SA_CODE,
        isnull(dbo.GET_IFUA_NO_BY_CIF_ID(a.cif_id),'') AS IFUA_NO,
        d.MAP_ID as FUND_CODE,
        replace(convert(varchar(30), convert(money,a.CMSN_NET_AMNT), 0),'.00','') as AMOUNT_NOMINAL,
        '' as AMOUNT_UNIT,
        '' as AMOUNT_ALLUNIT,
        '' as FEE_NOMINAL,
        '' as FEE_UNIT,
        '' as FEE_PCT,
        '' [SWC_FUND_CODE],
        '' as RED_PYMNT_ACC_SEQ_CODE,
        '' as RED_PYMNT_BANK_BIC_CODE,
        '' as RED_PYMNT_BANK_BI_MBR_CODE,
        '' as RED_PYMNT_BANK_ACC_NO,
        '' as PYMNT_DT,
        '' as TRNSFR_TYPE,
        'TRN_AGENT_CMSN_' + cast(a.autoid as varchar) as SA_REFF_NO,
        '1' SourceOfFund,
        '0' BankRecipient, 
        '' [Type],
        a.AUDIT_CRT_DT ApprovedTime,
        '' TrxUnitPaymentProvider,
        '' TrxUnitPaymentType
    FROM MUTUAL_FUND_AGENT_CMSN_IFUA a
    INNER JOIN NFS_PRD_MAP d on a.prd_id = d.PRD_ID
    WHERE /*isnull(a.SINVEST_FLAG , 0) = 0
        and */ISNULL(a.APV_DRWDWN_DT, '1/1/1900') <> '1/1/1900'
        and a.CMSN_IFUA_STS = 1
        and a.APV_DRWDWN_DT between @date_from and @date_to
        and not exists (
            select 1
            from [DN_RDOAPI] dn (nolock)
            where LEFT(dn.SA_REFF_NO, 15) = 'TRN_AGENT_CMSN_'
                and cast(replace(dn.SA_REFF_NO, 'TRN_AGENT_CMSN_', '') as int) = a.autoid
        ) 
        
    /*SELECT TRN_DT, TRN_TYPE, SA_CODE, IFUA_NO, FUND_CODE, 
            AMOUNT_NOMINAL, AMOUNT_UNIT, AMOUNT_ALLUNIT, 
            FEE_NOMINAL, FEE_UNIT, FEE_PCT,
            RED_PYMNT_ACC_SEQ_CODE,
            RED_PYMNT_BANK_BIC_CODE,
            RED_PYMNT_BANK_BI_MBR_CODE,
            RED_PYMNT_BANK_ACC_NO,
            PYMNT_DT, TRNSFR_TYPE, SA_REFF_NO   
        FROM DN_SINVEST_001
    WHERE FLAG = 0
        AND TRN_TYPE IN ('1','2')*/
    UNION ALL
    -- CashBack SIP
    select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID,/*@PRFL_SEQ_NO as PRFL_SEQ_NO, */
    CONVERT(VARCHAR(10), a.TglProcess, 120) as TRN_DT, 
    'SUB' as TRN_TYPE,     
    'DH002' as SA_CODE,
    a.IFUA_NO AS IFUA_NO,
    c.MAP_ID as FUND_CODE,
    replace(convert(varchar(30), convert(money, a.NominalCashBack), 0),'.00','') as AMOUNT_NOMINAL,
    '' as AMOUNT_UNIT,
    '' as AMOUNT_ALLUNIT,
    '' as FEE_NOMINAL,
    '' as FEE_UNIT,
    '' as FEE_PCT,
    '' [SWC_FUND_CODE],
    '' as RED_PYMNT_ACC_SEQ_CODE,
    '' as RED_PYMNT_BANK_BIC_CODE,
    '' as RED_PYMNT_BANK_BI_MBR_CODE,
    '' as RED_PYMNT_BANK_ACC_NO,
    '' as PYMNT_DT,
    '' as TRNSFR_TYPE,
    'VCHR_CASHBACK_SIP_' + cast(a.autoid as varchar) as SA_REFF_NO,
    '1' SourceOfFund,
    '0' BankRecipient, 
    '' [Type],
    a.lastupdate ApprovedTime,
    '' TrxUnitPaymentProvider,
    '' TrxUnitPaymentType
    from Report_CashBack_SIP a
    inner join NFS_PRD_MAP c on a.jenisprodukcashback = c.PRD_ID
    left join cust_cif e on a.cif_id = e.cif_id
    where A.tglprocess is not null
            and c.PRD_TYP = 'FND'
        and a.tglprocess between @date_from and @date_to
        and isnull(a.sinvestflag,0) = 0
        and not exists (
            select 1
            from [DN_RDOAPI] dn (nolock)
            where LEFT(dn.SA_REFF_NO, 18) = 'VCHR_CASHBACK_SIP_'
                and replace(dn.SA_REFF_NO, 'VCHR_CASHBACK_SIP_', '') = a.autoid
        ) 
    )

    union ALL

    select 
      'SYSTEM' as AUDIT_USR,
      @AUDIT_PRG as AUDIT_PRG,
      GETDATE() as AUDIT_DT,
      @profile_id as PRFL_ID,
     -- @PRFL_SEQ_NO as PRFL_SEQ_NO,
      convert(varchar(10), convert(datetime,TRN_DT), 120) TRN_DT, 
      'SUB' TRN_TYPE,
      SA_CODE, 
      SA_CODE IFUA_NO,
      FUND_CODE, 
      SUM(CAST(REPLACE(AMOUNT_NOMINAL, ',', '')as float)) AMOUNT_NOMINAL,
      SUM(CAST(REPLACE(AMOUNT_UNIT, ',', '')as float)) AMOUNT_UNIT,
      '' AMOUNT_ALLUNIT,
      SUM(cast(replace(FEE_NOMINAL, ',', '')as float)) FEE_NOMINAL,
      '' FEE_UNIT,
      case when FEE_PCT = '' then
            case when FEE_UNIT = '' then '0' 
            else FEE_PCT 
            END
        ELSE 
            FEE_PCT
        END FEE_PCT,
      '' [SWC_FUND_CODE],
      '' as RED_PYMNT_ACC_SEQ_CODE,
      '' as RED_PYMNT_BANK_BIC_CODE,
      '' as RED_PYMNT_BANK_BI_MBR_CODE,
      '' as RED_PYMNT_BANK_ACC_NO,
      '' as PYMNT_DT,
      '' as TRNSFR_TYPE,
      '' as SA_REFF_NO,
      '1' SourceOfFund,
      '0' BankRecipient, 
      '' [Type],
      convert(varchar(10), convert(datetime,TRN_DT), 120) ApprovedTime,
      '' TrxUnitPaymentProvider,
      '' TrxUnitPaymentType
    from SINVEST_TRN_APERD s
    where status in ('Approved', 'Completed', 'Inputted', 'Received')
        and s.TRN_TYPE = 'Subscription'
        and s.TRN_DT = @date_to
        and not exists (
          select 1
          from [DN_RDOAPI] t 
          where t.IFUA_NO = s.SA_CODE
            and t.FUND_CODE = s.FUND_CODE
            and convert(datetime,t.TRN_DT) = convert(datetime, s.TRN_DT)
            and t.TRN_TYPE = case when s.TRN_TYPE = 'Subscription' then 'SUB'
                                  when s.TRN_TYPE = 'Redemption' then 'RED'
                                  when s.TRN_TYPE = 'Switching' then 'SWI'
                              end

        )
    group by TRN_DT, SA_CODE, FUND_CODE, 
      case when FEE_PCT = '' then
            case when FEE_UNIT = '' then '0' 
            else FEE_PCT 
            END
        ELSE 
            FEE_PCT
        END

    union all

    select 
      'SYSTEM' as AUDIT_USR,
      @AUDIT_PRG as AUDIT_PRG,
      GETDATE() as AUDIT_DT,
      @profile_id as PRFL_ID,
     -- @PRFL_SEQ_NO as PRFL_SEQ_NO,
      convert(varchar(10), convert(datetime,TRN_DT), 120) TRN_DT, 
      'RED' TRN_TYPE,
      SA_CODE,
      SA_CODE IFUA_NO,
      FUND_CODE, 
      SUM(CAST(REPLACE(AMOUNT_NOMINAL, ',', '')as float)) AMOUNT_NOMINAL,
        SUM(case when s.AMOUNT_ALL_UNIT = 'Y' then blnc.good_fund_value else cast(REPLACE(AMOUNT_UNIT, ',', '') as float) end) AMOUNT_UNIT,
        '' AMOUNT_ALL_UNIT,
        SUM(cast(replace(FEE_NOMINAL, ',', '')as float)) FEE_NOMINAL,
        '' FEE_UNIT,
        case when FEE_PCT = '' then
            case when FEE_UNIT = '' then '0' 
            else FEE_PCT 
            END
        ELSE 
            FEE_PCT
        END FEE_PCT,
      '' [SWC_FUND_CODE],
      '' as RED_PYMNT_ACC_SEQ_CODE,
      '' as RED_PYMNT_BANK_BIC_CODE,
      '' as RED_PYMNT_BANK_BI_MBR_CODE,
      '' as RED_PYMNT_BANK_ACC_NO,
      '' as PYMNT_DT,
      '' as TRNSFR_TYPE,
      '' as SA_REFF_NO,
      '1' SourceOfFund,
      '0' BankRecipient, 
      '' [Type],
      convert(varchar(10), convert(datetime,TRN_DT), 120) ApprovedTime,
      '' TrxUnitPaymentProvider,
      '' TrxUnitPaymentType
    from SINVEST_TRN_APERD s
    inner join sinvest_ifua_map imap on s.ifua_no = imap.ifua_no 
    inner join NFS_PRD_MAP pmap on pmap.map_id = s.FUND_CODE
    inner join cust_product cp on cp.niaga_acc = imap.niaga_acc and pmap.prd_id = cp.product_code
    inner join saldo_daily_reksadana_all blnc on blnc.niaga_acc = cp.niaga_acc and cp.product_code = blnc.product_code and blnc.date_trans = dbo.getPrevTradingday(s.TRN_DT)
    where status in ('Approved', 'Completed', 'Inputted', 'Received')
        and TRN_TYPE = 'Redemption'
        and s.TRN_DT = @date_to
        and not exists (
          select 1
          from [DN_RDOAPI] t 
          where t.IFUA_NO = s.SA_CODE
            and t.FUND_CODE = s.FUND_CODE
            and convert(datetime,t.TRN_DT) = convert(datetime, s.TRN_DT)
            and t.TRN_TYPE = case when s.TRN_TYPE = 'Subscription' then 'SUB'
                                  when s.TRN_TYPE = 'Redemption' then 'RED'
                                  when s.TRN_TYPE = 'Switching' then 'SWI'
                              end

        )
    group by TRN_DT, SA_CODE, FUND_CODE, 
        case when FEE_PCT = '' then
            case when FEE_UNIT = '' then '0' 
            else FEE_PCT 
            END
        ELSE 
            FEE_PCT
        END

    

-------switching

	INSERT INTO [dbo].[DN_RDOAPI]
	(
		    [AUDIT_USR]
        ,[AUDIT_PRG]
        ,[AUDIT_DT]
        ,[PRFL_ID]
		   -- ,[PRFL_SEQ_NO]
        ,[TRN_DT]
        ,[TRN_TYPE]
        ,[SA_CODE]
        ,[IFUA_NO]
        ,[FUND_CODE]
        ,[AMOUNT_NOMINAL]
        ,[AMOUNT_UNIT]
        ,[AMOUNT_ALLUNIT]
        ,[CHARGE_FUND]
		    ,[FEE_NOMINAL]
        ,[FEE_UNIT]
        ,[FEE_PCT]
		    ,[SWC_FUND_CODE]
        ,[PYMNT_DT]
        ,[TRNSFR_TYPE]
        ,[SA_REFF_NO]
        ,[SourceOfFund]
        ,[BankRecipient]
        ,[TYPE]
        ,[ApprovedTime]
        ,[TrxUnitPaymentProvider]
        ,[TrxUnitPaymentType]
	)
	(
		select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID,/*@PRFL_SEQ_NO as PRFLSEQ_NO,*/ 
      CONVERT(VARCHAR(10), a.tgl_trans, 120) as TRN_DT, 
			'SWI' as TRN_TYPE,
			'DH002' as SA_CODE,
			isnull(g.IFUA_NO,i.IFUA_NO) AS IFUA_NO,
			c.MAP_ID as FUND_CODE,
			case b.rp 
				when 0 then ''
				when 1 then ''
				when 2 then replace(convert(varchar(30), b.jml_kotor, 0),'.00','')
			end 
			as AMOUNT_NOMINAL,
			case b.rp 
				when 0 then s.good_fund_value
				when 1 then ltrim(STR(b.jml_kotor,26,4))--replace(convert(varchar(30), b.jml_kotor, 0),'.00','')
				when 2 then ''
			end
			as AMOUNT_UNIT,
			case b.rp 
				when 0 then 'FULL'
				when 1 then ''
				when 2 then ''
			end
			as AMOUNT_ALLUNIT,
			'1' as CHARGE_FUND,
			dbo.IsZeroString(
				case b.rp 
					when 0 then ''
					when 1 then ''
					when 2 then replace(convert(varchar(30), b.fee, 0),'.00','')
				end 
			 ,'')
			as FEE_NOMINAL,
			'' as FEE_UNIT,
			dbo.IsZeroString(
				case b.rp 
					when 0 then replace(convert(varchar(30), b.fee, 0),'.00','')
					when 1 then replace(convert(varchar(30), b.fee, 0),'.00','')
					when 2 then ''
				end
			,'')
			as FEE_PCT,
			h.MAP_ID as SWC_FUND_CODE, 
			CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tgl_trans), 112) as PYMNT_DT, 
			'3' as TRNSFR_TYPE,
			'TC_' + cast(b.tc_detail_id as varchar) as SA_REFF_NO,
      '1' SourceOfFund,
      '0' BankRecipient, 
      '' [Type],
      b.approve_ho_date ApprovedTime,
      '' TrxUnitPaymentProvider,
      '' TrxUnitPaymentType
		from transaksi_cabang a
		inner join transaksi_cabang_detail b on a.tc_header_id = b.tc_header_id
    inner join cust_product cp on a.cif_id = cp.CIF_ID and b.product_code = cp.Product_code
    inner join saldo_daily_reksadana_ALL s on cp.Niaga_acc = s.niaga_acc and cp.Product_code = s.product_code and s.date_trans =dbo.GetPrevTradingDay(a.tgl_trans)
		inner join NFS_PRD_MAP c on b.product_code = c.PRD_ID
		inner join no_surat_detail d on b.tc_detail_id = d.tc_detail_id
		left join (
			select bank_id, map_id 
			from nfs_bank_map
			group by bank_id, map_id) e on b.bank_rek = e.BANK_ID
		left join SINVEST_IFUA_MAP g on d.niaga_acc = g.niaga_acc 
			and isnull(d.aperd_kode,'') = isnull(g.aperd_kode,'')
		inner join NFS_PRD_MAP h on b.product_code_switch = h.PRD_ID
		left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
				  max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
				  from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
				  where a.Niaga_acc = b.NIAGA_ACC
				  and b.IFUA_NO is not null
				  and a.cif_id = c.CIF_ID
				  and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
				  group by a.cif_id) i on i.CIF_ID = a.cif_id
		where a.jns_trans in ('W')
			and c.PRD_TYP = 'FND'
			and isnull(d.aperd_kode, '') = ''
			and b.status_drowdown = 'S'
			and a.tgl_trans between @date_from and @date_to
			--and isnull(d.sinvest_flag, 0) = 0 
			and not exists (
				select 1
				from [DN_RDOAPI] dn (nolock)
				where LEFT(dn.SA_REFF_NO, 3) = 'TC_'
					and cast(replace(dn.SA_REFF_NO, 'TC_', '') as int) = b.tc_detail_id)
  
	UNION ALL

	  -- MANUAL
	  select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID,/*@PRFL_SEQ_NO as PRFL_SEQ_NO,*/
      CONVERT(VARCHAR(10), a.tgl, 120) as TRN_DT, 
	  'SWI' as TRN_TYPE,     
	  'DH002' as SA_CODE,
	  isnull(b.IFUA_NO,i.IFUA_NO) AS IFUA_NO,
	  c.MAP_ID as FUND_CODE,
	  case when a.jenis like '%Pembelian%' then replace(convert(varchar(30), a.JmlKotor, 0),'.00','')
	  else
		case a.rp 
		  when 0 then replace(convert(varchar(30), a.jumlah, 0),'.00','')
		  else
			case when a.jenis like '%Rp%' then replace(convert(varchar(30), a.jumlah, 0),'.00','')
				 when a.jenis like '%Unit(S)%' then ''
				 else ''
			end 
		end 
	  end  
	  as AMOUNT_NOMINAL,
	  case a.rp 
		when 0 then ''
		else
		  case when a.jenis like '%Rp%' then ''
			   when a.jenis like '%Unit(S)%' then s.good_fund_value
			   else ltrim(STR(a.jumlah,26,4))--replace(convert(varchar(30), a.jumlah, 0),'.00','')
		  end 
	  end 
	  as AMOUNT_UNIT,
	  case when a.jenis like '%Rp%' then ''
		when a.jenis like '%Unit(S)%' then 'FULL'
		else ''
	  end 
	  as AMOUNT_ALLUNIT,
	  '1' as CHARGE_FUND,
	  dbo.IsZeroString(
	  case when a.jenis like '%Pembelian%' then replace(convert(varchar(30), a.jmlfee, 0),'.00','')
	  else
		case a.rp 
		  when 0 then ''
		  when 1 then ''
		  when 2 then replace(convert(varchar(30), a.jmlfee, 0),'.00','')
		end
	  end  
	  ,'')
	  as FEE_NOMINAL,
	  '' as FEE_UNIT,
	  dbo.IsZeroString(
	  case a.rp 
		when 0 then replace(convert(varchar(30), a.fee, 0),'.00','')
		when 1 then replace(convert(varchar(30), a.fee, 0),'.00','')
		when 2 then ''
	  end
	  ,'')
	  as FEE_PCT,
	  h.MAP_ID as SWC_FUND_CODE, 
	  CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tgl), 112) as PYMNT_DT, 
	  '3' as TRNSFR_TYPE,
	  'MNLI_' + cast(a.urutan as varchar) as SA_REFF_NO,
      '1' SourceOfFund,
      '0' BankRecipient, 
      '' [Type],
      a.CRT_DT ApprovedTime,
      '' TrxUnitPaymentProvider,
      '' TrxUnitPaymentType
	  from no_surat_detail a
    inner join cust_product cp on a.cif = cp.CIF_ID and a.Kode_tr = cp.Product_code
    inner join saldo_daily_reksadana_ALL s on cp.Niaga_acc = s.niaga_acc and s.product_code = cp.Product_code and s.date_trans = dbo.GetPrevTradingDay(a.Tgl)
	  inner join NFS_PRD_MAP c on a.kode_tr = c.PRD_ID
	  left join (select bank_id, map_id 
					 from nfs_bank_map
					group by bank_id, map_id) e on a.bank_name = e.BANK_ID   
	  left join SINVEST_IFUA_MAP b on a.niaga_acc = b.niaga_acc 
								  and isnull(a.aperd_kode,'') = isnull(b.aperd_kode,'')
	  inner join NFS_PRD_MAP h on right(a.jenis,3) = h.PRD_ID
	  left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
				  max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
				  from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
				  where a.Niaga_acc = b.NIAGA_ACC
				  and b.IFUA_NO is not null
				  and a.cif_id = c.CIF_ID
				  and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
				  group by a.cif_id) i on i.CIF_ID = a.cif
	  where a.tc_detail_id is null
	  and a.auto_detail_id is null
	  --and a.category_trx = '10001'
	  and isnumeric(right(a.niaga_acc,5)) = 1
	  and (a.jenis like '%Switching%')
	  and c.PRD_TYP = 'FND'
	  and isnull(a.aperd_kode, '') = ''
	  and a.tgl between @date_from and @date_to
	  --and isnull(a.sinvest_flag, 0) = 0
	  and not exists (
			select 1
			from [DN_RDOAPI] dn (nolock)
			where LEFT(dn.SA_REFF_NO, 5) = 'MNLI_'
				and replace(dn.SA_REFF_NO, 'MNLI_', '') = a.urutan)
  
	UNION ALL

	  -- ROL
	  select 'SYSTEM' as AUDIT_USR,
      @AUDIT_PRG as AUDIT_PRG,
      GETDATE() as AUDIT_DT,
      @profile_id as PRFL_ID, 
      --@PRFL_SEQ_NO as PRFL_SEQ_NO,
      CONVERT(VARCHAR(10), a.tanggal_approve_spv, 120) as TRN_DT, 
	  'SWI' as TRN_TYPE,     
	  'DH002' as SA_CODE,
	  isnull(b.IFUA_NO,i.IFUA_NO) AS IFUA_NO,
	  c.MAP_ID as FUND_CODE,
	  case a.jenis_nominal 
		when 1 then ''
		when 2 then ''
		when 3 then replace(convert(varchar(30), a.nilai_transaksi, 0),'.00','')
	  end 
	  as AMOUNT_NOMINAL,
	  case a.jenis_nominal 
		when 1 then ltrim(STR(a.nilai_transaksi,26,4))--replace(convert(varchar(30), a.nilai_transaksi, 0),'.00','')
		when 2 then s.good_fund_value
		when 3 then ''
	  end
	  as AMOUNT_UNIT,
	  case a.jenis_nominal 
		when 1 then ''
		when 2 then 'FULL'
		when 3 then ''
	  end
	  as AMOUNT_ALLUNIT,
	  '1' as CHARGE_FUND,
	  dbo.IsZeroString(
	  case a.jenis_nominal 
		when 1 then ''
		when 2 then ''
		when 3 then replace(convert(varchar(30), a.biaya_transaksi, 0),'.00','')
	  end 
	  ,'')
	  as FEE_NOMINAL,
	  '' as FEE_UNIT,
	  dbo.IsZeroString(
	  case a.jenis_nominal 
		when 1 then replace(convert(varchar(30), a.persen_biaya_transaksi, 0),'.00','')
		when 2 then replace(convert(varchar(30), a.persen_biaya_transaksi, 0),'.00','')
		when 3 then ''
	  end
	  ,'')
	  as FEE_PCT,
	  h.MAP_ID as SWC_FUND_CODE, 
	  CONVERT(VARCHAR(10), dbo.GetNextTradingDay(a.tanggal_approve_spv), 112) as PYMNT_DT, 
	  '3' as TRNSFR_TYPE,
	  'SF_' + a.trans_id as SA_REFF_NO,
    '1' SourceOfFund,
    '0' BankRecipient, 
    '' [Type],
    a.tanggal_approve_spv ApprovedTime,
    '' TrxUnitPaymentProvider,
    '' TrxUnitPaymentType
	  from data_rol a
    inner join saldo_daily_reksadana_ALL s on a.niaga_acc = s.niaga_acc and a.product_code = s.product_code and s.date_trans = dbo.GetPrevTradingDay(a.tanggal_approve_spv)
	  inner join NFS_PRD_MAP c on a.product_code = c.PRD_ID
	  left join (select bank_id, map_id 
					 from nfs_bank_map
					group by bank_id, map_id) e on upper(a.bank) = upper(e.BANK_ID)
	  left join cust_cif d on a.cif_id = d.CIF_ID 
	  left join SINVEST_IFUA_MAP b on a.niaga_acc = b.niaga_acc 
								  and isnull(d.aperd_kode,'') = isnull(b.aperd_kode,'')
	  inner join NFS_PRD_MAP h on a.product_code_switch = h.PRD_ID
	  left join (select a.cif_id, max(b.APERD_KODE) as APERD_KODE, 
				  max(b.NIAGA_ACC) as NIAGA_ACC, max(b.IFUA_NO) as IFUA_NO
				  from cust_product a, SINVEST_IFUA_MAP b, cust_cif c
				  where a.Niaga_acc = b.NIAGA_ACC
				  and b.IFUA_NO is not null
				  and a.cif_id = c.CIF_ID
				  and isnull(b.aperd_kode,'') = isnull(c.aperd_kode,'')
				  group by a.cif_id) i on i.CIF_ID = a.cif_id
	  where a.jenis_transaksi in ('C')
	  and a.status='2'
	  and c.PRD_TYP = 'FND'
	  and CONVERT(VARCHAR(10), a.tanggal_approve_spv, 101) between @date_from and @date_to
	  --and isnull(a.sinvest_flag, 0) = 0
	  and not exists (
			select 1
			from [DN_RDOAPI] dn (nolock)
			where LEFT(dn.SA_REFF_NO, 3) = 'SF_'
				and replace(dn.SA_REFF_NO, 'SF_', '') = a.trans_id)
	  )

    union all

    select 
      'SYSTEM' as AUDIT_USR,
      @AUDIT_PRG as AUDIT_PRG,
      GETDATE() as AUDIT_DT,
      @profile_id as PRFL_ID,
      --@PRFL_SEQ_NO as PRFL_SEQ_NO,
      convert(varchar(10), convert(datetime,TRN_DT), 120) TRN_DT, 
      'SWI' TRN_TYPE,
      SA_CODE,
      SA_CODE IFUA_NO,
      SWO_FUND_CODE, 
      SUM(CAST(REPLACE(SWO_NOMINAL, ',', '')as float)) AMOUNT_NOMINAL,
        SUM(case when s.SWO_ALL_UNIT = 'Y' then blnc.good_fund_value else cast(REPLACE(SWO_UNIT, ',', '') as float) end) AMOUNT_UNIT,
        '' AMOUNT_ALL_UNIT,
        '1' CHARGE_FUND,
        SUM(cast(replace(FEE_NOMINAL, ',', '')as float)) FEE_NOMINAL,
        '' FEE_UNIT,
        case when FEE_PCT = '' then
            case when FEE_UNIT = '' then '0' 
            else FEE_PCT 
            END
        ELSE 
            FEE_PCT
        END FEE_PCT,
      SWI_FUND_CODE [SWC_FUND_CODE],
      '' as PYMNT_DT,
      '' as TRNSFR_TYPE,
      '' as SA_REFF_NO,
      '1' SourceOfFund,
      '0' BankRecipient, 
      '' [Type],
      convert(varchar(10), convert(datetime,TRN_DT), 120) ApprovedTime,
      '' TrxUnitPaymentProvider,
      '' TrxUnitPaymentType
    from SINVEST_TRN_APERD_SWC s
    inner join sinvest_ifua_map imap on s.ifua_no = imap.ifua_no 
    inner join NFS_PRD_MAP pmap on pmap.map_id = s.SWO_FUND_CODE
    inner join cust_product cp on cp.niaga_acc = imap.niaga_acc and pmap.prd_id = cp.product_code
    inner join saldo_daily_reksadana_all blnc on blnc.niaga_acc = cp.niaga_acc and cp.product_code = blnc.product_code and blnc.date_trans = dbo.getPrevTradingday(s.TRN_DT)
    where status in ('Approved', 'Completed', 'Inputted', 'Received')
        and TRN_TYPE = 'Switching'
        and s.TRN_DT = @date_to
        and not exists (
          select 1
          from [DN_RDOAPI] t 
          where t.IFUA_NO = s.SA_CODE
            and t.FUND_CODE = s.SWO_FUND_CODE
            and convert(datetime,t.TRN_DT) = convert(datetime, s.TRN_DT)
            and t.TRN_TYPE = case when s.TRN_TYPE = 'Subscription' then 'SUB'
                                  when s.TRN_TYPE = 'Redemption' then 'RED'
                                  when s.TRN_TYPE = 'Switching' then 'SWI'
                              end

        )
    group by TRN_DT, SA_CODE, SWO_FUND_CODE, SWI_FUND_CODE,
        case when FEE_PCT = '' then
            case when FEE_UNIT = '' then '0' 
            else FEE_PCT 
            END
        ELSE 
            FEE_PCT
        END

END TRY
BEGIN CATCH
INSERT INTO [dbo].[DN_RDOAPI]([AUDIT_DT],[ERROR_CODE],[ERROR_MESSAGE]) VALUES(GETDATE(), ERROR_NUMBER(),ERROR_MESSAGE()) 

END CATCH

END
---------------------------------------------------------------------------------------
ELSE IF @profile_id = 'DN_S-INVEST_003' 
BEGIN
  PRINT @profile_id
  BEGIN TRY
  INSERT INTO [dbo].[DN_RDOAPI_003](
              [AUDIT_USR]
            ,[AUDIT_PRG]
            ,[AUDIT_DT]
            ,[PRFL_ID]
            --,[PRFL_SEQ_NO]
            ,[type]
            ,[SA_Code]
            ,[SID]
            ,[FirstName]
            ,[MiddleName]
            ,[LastName]
            ,[CountryofNationality]
            ,[IDNo]
            ,[IDExpirationDate]
            ,[NPWPNo]
            ,[NPWPRegistrationDate]
            ,[CountryofBirth]
            ,[PlaceofBirth]
            ,[DateofBirth]
            ,[Gender]
            ,[EducationalBackground]
            ,[MothersMaidenName]
            ,[Religion]
            ,[Occupation]
            ,[IncomeLevel]
            ,[MaritalStatus]
            ,[SpousesName]
            ,[InvestorsRiskProfile]
            ,[InvestmentObjective]
            ,[SourceofFund]
            ,[AssetOwner]
            ,[KTPAddress]
            ,[KTPCityCode]
            ,[KTPPostalCode]
            ,[CorrespondenceAddress]
            ,[CorrespondenceCityCode]
            ,[CorrespondenceCityName]
            ,[CorrespondencePostalCode]
            ,[CountryofCorrespondence]
            ,[DomicileAddress]
            ,[DomicileCityCode]
            ,[DomicileCityName]
            ,[DomicilePostalCode]
            ,[CountryofDomicile]
            ,[HomePhone]
            ,[MobilePhone]
            ,[Facsimile]
            ,[Email]
            ,[StatementType]
            ,[FATCA]
            ,[TIN]
            ,[TINIssuanceCountry]
            ,[REDMPaymentBankBICCode1]
            ,[REDMPaymentBankBIMemberCode1]
            ,[REDMPaymentBankName1]
            ,[REDMPaymentBankCountry1]
            ,[REDMPaymentBankBranch1]
            ,[REDMPaymentA/CCCY1]
            ,[REDMPaymentA/CNo1]
            ,[REDMPaymentA/CName1]
            ,[REDMPaymentBankBICCode2]
            ,[REDMPaymentBankBIMemberCode2]
            ,[REDMPaymentBankName2]
            ,[REDMPaymentBankCountry2]
            ,[REDMPaymentBankBranch2]
            ,[REDMPaymentA/CCCY2]
            ,[REDMPaymentA/CNo2]
            ,[REDMPaymentA/CName2]
            ,[REDMPaymentBankBICCode3]
            ,[REDMPaymentBankBIMemberCode3]
            ,[REDMPaymentBankName3]
            ,[REDMPaymentBankCountry3]
            ,[REDMPaymentBankBranch3]
            ,[REDMPaymentA/CCCY3]
            ,[REDMPaymentA/CNo3]
            ,[REDMPaymentA/CName3]
            ,[ClientCode]
            ,[NamaKantor]
            ,[AlamatKantorInd]
            ,[TeleponKantor]
            ,[JabatanKantor]
            ,[KodeKotaKantorInd]
            ,[KodePosKantorInd]
            ,[BeneficialName]
            ,[Politis]
            ,[PolitisRelation]
            ,[PolitisName]
            ,[IdentitasInd1]
            ,[IFUA_NO]
        )
          select 'SYSTEM' as AUDIT_USR,@AUDIT_PRG as AUDIT_PRG,GETDATE() as AUDIT_DT,@profile_id as PRFL_ID, /*@PRFL_SEQ_NO as PRFL_SEQ_NO,*/
      case isnull(h.IFUA_NO,'') 
        when '' then '1' 
        else '2'
      end as type, 
      'DH002' as SA_Code,
      case isnull(h.IFUA_NO,'') 
        when '' then '' 
        else a.clientsid
      end as SID, 
      a.NAMA_NASABAH as [FirstName], 
      '' as [MiddleName], 
      '' as [LastName],
      case a.dd_warganegara
        when 'A' then mcc.ISO_CODE 
        else 'ID' 
      end as [CountryofNationality], 
      left(
        case when (len(a.dd_no_id) =0 or a.dd_no_id is null) then '' 
          else isnull(a.dd_no_id, '')  
        end 
      ,20)
      as [IDNo], 
      case when len(ISNULL(a.dd_no_id, '')) > 0 then 
          replace(convert(varchar,ISNULL(a.dd_tgl_exp_id, '25000101'),112) , '19000101', '25000101')
          else ''
      end as [IDExpirationDate],
      case when len(replace(dbo.stripID(isnull(a.dd_no_npwp,'')), '-', '')) <> 15 then '' 
        else dbo.stripID(isnull(a.dd_no_npwp,''))  
      end [NPWPNo], 
      case when len(replace(dbo.stripID(isnull(a.dd_no_npwp,'')), '-', '')) <> 15 then ''
        else replace(ISNULL(convert(varchar,a.dd_tanggal_registrasi,112), '19000101'), '19000101', '') 
      end [NPWPRegistrationDate],
      case a.dd_warganegara
        when 'A' then ISNULL(mcc.ISO_CODE , '')
        else 'ID' 
      end [CountryofBirth], 
      case when len(a.dd_tmp_lahir)=0 then '0' 
        else a.dd_tmp_lahir 
      end as [PlaceofBirth], 
      case when len(cast(a.dd_tgl_lahir as varchar(30)))=0 then '0' 
        else convert(varchar,a.dd_tgl_lahir,120) 
      end as [DateofBirth],
      case when len(replace(replace(replace(replace(replace(a.dd_jenis_kelamin,'P','2'),'F','2'),'W','2'),'L',1),'M',2))=0 then '0' 
        else case when replace(replace(replace(replace(replace(a.dd_jenis_kelamin,'P','2'),'F','2'),'W','2'),'L',1),'M',2) is null then '0' 
              else replace(replace(replace(replace(replace(a.dd_jenis_kelamin,'P','2'),'F','2'),'W','2'),'L',1),'M',2) 
            end 
      end [Gender],
      case a.dd_pendidikan
        when '0' then '3'
        when '1' then '4'
        when '2' then '5'
        when '3' then '6'
        when '4' then '7'
        else '8'
      end as [EducationalBackground],
      a.dd_nama_ibu_sblm_nikah as [MothersMaidenName],
      case a.dd_Agama
        when '0' then '5'
        when '1' then '4'
        when '2' then '1'
        when '3' then '2'
        when '4' then '3'
        else '7'
      end as [Religion],
      case a.dp_status_kerja
        when '0' then '3'
        when '1' then '2'
        when '2' then '1'
        when '3' then '9'
        when '4' then '8'
        when '5' then '4'
        when '6' then '4'
        when '7' then '5'
        when '8' then '6'
        else '9'
      end as [Occupation],
      case isnull(a.dp_hasil_perthn,0) 
        when '0' then '2'
        when '1' then '2'
        when '2' then '2'
        when '3' then '4'
        when '4' then '4'
        when '5' then '5'
        when '6' then '6'
        when '7' then '4'
        when '8' then '1'
        when '9' then '2'
        when '10' then '3'
        when '11' then '4'
      else '3' end as [IncomeLevel], 
      case a.dd_status_nikah
        when '0' then '1'
        when '1' then '3'
        when '2' then '2'
      else '1' end [MaritalStatus], 
      ISNULL(a.dd_nama_suami_istri, '') [SpousesName], 
      '' [InvestorsRiskProfile],
      case 
        when a.tujuan_investasi >=32 then '5' 
        when a.tujuan_investasi >=16 then '3' 
        when a.tujuan_investasi >=8 then '4' 
        when a.tujuan_investasi >=4 then '1' 
        when a.tujuan_investasi >=2 then '2' 
        when a.tujuan_investasi >=1 then '1' 
        else '5'
      end as [InvestmentObjective], 
      case 
        when a.dp_Sumber_dana >=2048 then '10' 
        when a.dp_Sumber_dana >=1024 then '3'
        when a.dp_Sumber_dana >=512 then '6'
        when a.dp_Sumber_dana >=256 then '10'
        when a.dp_Sumber_dana >=128 then '9'
        when a.dp_Sumber_dana >=64 then '5'
        when a.dp_Sumber_dana >=32 then '7'
        when a.dp_Sumber_dana >=16 then '8'
        when a.dp_Sumber_dana >=8 then '1'
        when a.dp_Sumber_dana >=4 then '5'
        when a.dp_Sumber_dana >=2 then '9'
        when a.dp_Sumber_dana >=1 then '1'
      else '10' end as [SourceofFund],
      case a.dd_status_rumah 
        when '0' then '1'
        when '1' then '1'
        when '5' then '1'
        else '2' 
      end [AssetOwner], 
      left(
      REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
      case when (len(a.dd_alamat_ktp)=0) or (a.dd_alamat_ktp='<br>')  then '' 
        else case when charindex('EMAIL Alternatif',a.dd_alamat_ktp)>0  then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Alternatif email',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Alternatif',a.dd_alamat_ktp)-2) 
        else case when charindex('Alternative email',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Alternative',a.dd_alamat_ktp)-2) 
        else case when charindex('Email Alternatif',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Email Alternative',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Email Lain',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Email Tambahan',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Email Alt',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else case when charindex('Email',a.dd_alamat_ktp)>0 then left(a.dd_alamat_ktp,charindex('Email',a.dd_alamat_ktp)-2) 
        else isnull(a.dd_alamat_ktp,'') end end end end end end end end end 
      end 
      , CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' ') 
      ,100)
      as [KTPAddress],  
      REPLACE(
      case when (len(a.dd_kota_ktp)=0) then '9999'
        when isnumeric(a.dd_kota_ktp)=1 then isnull(cast(convert(int, k.kode) as varchar(7)),cast(convert(int, m.kode) as varchar(7)))
        when (k.kode is null) then isnull(k.kode, '9999')
        when (a.dd_kota_ktp is null) then '9999'	
        else isnull(cast(convert(int, k.kode) as varchar(7)),cast(convert(int, m.kode) as varchar(7)))  
      end 
      ,'.0','')
      as [KTPCityCode], 
      case when (len(ISNULL(a.dd_kodepos_ktp, ''))=0) then '' 
        else isnull(a.dd_kodepos_ktp, '') 
      end as [KTPPostalCode],
      left(
      REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
      case when (len(a.dd_alamat_srt)=0) then '' 
        else isnull(a.dd_alamat_srt,di_alamat) 
      end 
      , CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' ') 
      ,100)
      as [CorrespondenceAddress], 
      REPLACE(
      case when (len(a.dd_kota_srt)=0) then '9999'
        when isnumeric(a.dd_kota_srt)=1 then isnull(cast(convert(int, k.kode) as varchar(7)), '9999')
        when l.kode is null then isnull(l.kode, '9999')
        when a.dd_kota_srt is null then '9999'
        else isnull(cast(convert(int, l.kode) as varchar(7)), '9999') 
      end 
      ,'.0','')
      as [CorrespondenceCityCode], 
      case when (len(a.dd_kota_srt)=0) then ''
        when isnumeric(a.dd_kota_srt)=1 then isnull(cast(a.dd_kota_srt as varchar(30)), '')
        when l.kode is null then isnull(cast(a.dd_kota_srt as varchar(30)), '')
        when a.dd_kota_srt is null then ''
        else isnull(cast(a.dd_kota_srt as varchar(30)), '') 
      end as [CorrespondenceCityName], 
      case when (len(ISNULL(a.dd_kodepos_srt, ''))=0) then '' 
        else isnull(a.dd_kodepos_srt, a.di_kodepos) 
      end as [CorrespondencePostalCode], 
      '' as [CountryofCorrespondence],
      REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
      case when (len(a.dd_alamat_tinggal)=0) then '' 
        else isnull(a.dd_alamat_tinggal, a.dd_alamat_ktp) 
      end 
      , CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' ') as [DomicileAddress], 
      REPLACE(
      case when (len(a.dd_kota_tinggal)=0) then '9999'
        when isnumeric(a.dd_kota_tinggal)=1 then isnull(cast(convert(int, l.kode) as varchar(7)),cast(convert(int, m.kode) as varchar(7)))
        when l.kode is null then isnull(l.kode, '9999')
        when a.dd_kota_tinggal is null then '9999'
        else isnull(cast(convert(int, l.kode) as varchar(7)),cast(convert(int, m.kode) as varchar(7)))  
      end 
      ,'.0','')
      as [DomicileCityCode], 
      case when (len(a.dd_kota_tinggal)=0) then ''
        when isnumeric(a.dd_kota_tinggal)=1 then isnull(cast(a.dd_kota_tinggal as varchar(30)), '')
        when l.kode is null then isnull(a.dd_kota_tinggal, '')
        when a.dd_kota_tinggal is null then isnull(cast(a.dd_kota_tinggal as varchar(30)), '')
        else isnull(cast(a.dd_kota_tinggal as varchar(30)), '')  
      end as [DomicileCityName], 
      ISNULL(case when (len(a.dd_kodepos_tinggal)=0) or (len(a.di_kodepos)=0)  then '' 
        else isnull(a.dd_kodepos_tinggal,di_kodepos) 
      end, '') as [DomicilePostalCode], 
      '' as [CountryofDomicile], 
      ISNULL(cast(a.dd_telp_ktp as varchar), '0') as [HomePhone], 
      cast(a.dd_handphone_ktp as varchar) as [MobilePhone], 
      ISNULL(cast(a.dd_fax_srt as varchar), '') [Facsimile], 
    case isnull(a.dd_email_srt,'') 
        when '' then '' 
        else a.dd_email_srt 
      end as [Email], 
      case isnull(a.dd_email_srt,'') 
        when '' then '1'
        else '2' 
      end as [StatementType], 
      '' as [FATCA], 
      '' as [TIN], 
      '' as [TINIssuanceCountry],
      '' as [REDMPaymentBankBICCode1], 
      ISNULL(c.map_id, '') as [REDMPaymentBankBIMemberCode1], 
      ISNULL(b1map.PAR_VAL_RDO, '') as [REDMPaymentBankName1], 
      case isnull(c.map_id,'')
        when '' then ''
        else 'ID'
      end as [REDMPaymentBankCountry1], 
      ISNULL(b.cabang, '') as [REDMPaymentBankBranch1], 
      case when isnull(b1map.PAR_VAL_RDO, '') = '' then '' 
          else case when isnull(b.currency, 'IDR') = 'IDR' then '1'
                    when isnull(b.currency, 'IDR') = 'USD' then '2'
                    when isnull(b.currency, 'IDR') = 'EUR' then '3'
              else '4' --lainnya
              end
      end as [REDMPaymentA/CCCY1], 
      dbo.StripID(ISNULL(b.no_rek, '')) as [REDMPaymentA/CNo1], 
      ISNULL(b.pemilik, '') as [REDMPaymentA/CName1],
      '' as [REDMPaymentBankBICCode2], 
      ISNULL(e.map_id, '') as [REDMPaymentBankBIMemberCode2], 
      ISNULL(b2map.PAR_VAL_RDO, '') as [REDMPaymentBankName2], 
      case isnull(e.map_id,'')
        when '' then ''
        else 'ID'
      end as [REDMPaymentBankCountry2], 
      ISNULL(d.cabang, '') as [REDMPaymentBankBranch2], 

      case when isnull(b2map.PAR_VAL_RDO, '') = '' then '' 
          else case when isnull(d.currency, 'IDR') = 'IDR' then '1'
                    when isnull(d.currency, 'IDR') = 'USD' then '2'
                    when isnull(d.currency, 'IDR') = 'EUR' then '3'
              else '4' --lainnya
              end 
      end as [REDMPaymentA/CCCY2],

      dbo.StripID(ISNULL(d.no_rek, '')) as [REDMPaymentA/CNo2],
      isnull(d.pemilik, '') as [REDMPaymentA/CName2],
      '' as [REDMPaymentBankBICCode3], 
      ISNULL(g.map_id, '') as [REDMPaymentBankBIMemberCode3],
      ISNULL(b3map.PAR_VAL_RDO, '') as [REDMPaymentBankName3], 
      case isnull(g.map_id,'')
        when '' then ''
        else 'ID'
      end as [REDMPaymentBankCountry3], 
      ISNULL(f.cabang, '') as [REDMPaymentBankBranch3],
      case when ISNULL(b3map.PAR_VAL_RDO, '') = '' then '' 
          else case when isnull(f.currency, 'IDR') = 'IDR' then '1'
                    when isnull(f.currency, 'IDR') = 'USD' then '2'
                    when isnull(f.currency, 'IDR') = 'EUR' then '3'
              else '4' --lainnya
              end
      end as [REDMPaymentA/CCCY3], 
      dbo.StripID(ISNULL(f.no_rek, '')) as [REDMPaymentA/CNo3],
      ISNULL(f.pemilik, '') as [REDMPaymentA/CName3],
      RIGHT(replicate('0',6) + convert(varchar(6),a.urutan), 6) as [ClientCode],
    ISNULL(DP_NAMA_PERUSAHAAN, '') [NamaKantor],
    LEFT(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ISNULL(DP_ALAMAT, ''), CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' '), 100)  [AlamatKantorInd],
    ISNULL(REPLACE(DP_TELP, 'NULL', '') , '') [TeleponKantor],
    ISNULL(a.DP_JABATAN, '') [JabatanKantor],
    REPLACE(
      case when (len(a.DP_KOTA)=0) then '9999'
        when isnumeric(a.DP_KOTA)=1 then cast(convert(int, dp.kode) as varchar(7))
        when a.dd_kota_tinggal is null then '9999'
        else cast(convert(int, dp.kode) as varchar(7))
      end 
      ,'.0','') [KodeKotaKantorInd],
    case when (len(a.dp_kodepos)=0)  then '' 
        else isnull(a.dp_kodepos, '') 
      end [KodePosKantorInd],
    '' [BeneficialName],
    '' [Politis],
    '' [PolitisRelation],
    '' [PolitisName],
      ISNULL(id_map.PAR_VAL_RDO, 7) IdentitasId1,
      isnull(h.IFUA_NO,'')IFUA_NO
      from FPR a
      left join aria_provinsi k on a.dd_kota_ktp=k.[kotamadya/kabupaten]
      left join aria_provinsi l on a.dd_kota_srt=l.[kotamadya/kabupaten]
      left join aria_provinsi m on a.dd_kota_tinggal=m.[kotamadya/kabupaten]
    left join aria_provinsi dp on a.DP_KOTA=dp.[kotamadya/kabupaten]
      left join fpr_bank_sinvest_temp b
        on b.fpr_id = a.urutan    
      and b.SEQ_NO = 1
      left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) c 
        on b.bank = c.BANK_ID
      left join SINVEST_BI_MBR_CD n on c.MAP_ID = n.BI_CD
      left join RDO_PAR_MAP b1map on b1map.PAR_VAL_MYBO = n.BI_CD and b1map.PAR_ID = 'MSTR_BANK'
      left join fpr_bank_sinvest_temp d
        on d.fpr_id = a.urutan    
      and d.SEQ_NO = 2    
      left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) e 
        on d.bank = e.BANK_ID
      left join SINVEST_BI_MBR_CD o on e.MAP_ID = o.BI_CD
      left join RDO_PAR_MAP b2map on b2map.PAR_VAL_MYBO = o.BI_CD and b2map.PAR_ID = 'MSTR_BANK'
      left join fpr_bank_sinvest_temp f
        on f.fpr_id = a.urutan    
      and f.SEQ_NO = 3   
      left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) g 
        on f.bank = g.BANK_ID
      left join SINVEST_BI_MBR_CD p on g.MAP_ID = p.BI_CD 
      left join RDO_PAR_MAP b3map on b3map.PAR_VAL_MYBO = p.BI_CD and b3map.PAR_ID = 'MSTR_BANK'
      left join (select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                  from cust_cif a
                  inner join cust_product b on a.CIF_ID = b.CIF_ID
                  inner join SINVEST_IFUA_MAP c 
                    on b.Niaga_acc = c.niaga_acc 
                    and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                    and c.IFUA_NO not like '%T%'
                  group by a.CIF_ID
                  union
                  select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                  from cust_cif a
                  inner join cust_product b on a.CIF_ID = b.CIF_ID
                  inner join SINVEST_IFUA_MAP_delete c on b.Niaga_acc = c.niaga_acc 
                    and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                    and c.IFUA_NO not like '%T%'
                    and c.NOTES like '%suspend%'
                  group by a.CIF_ID
            ) h 
            on a.CIF_SMS = h.CIF_ID
      inner join (select fprid, max(Approval_Date) Apv_Date
                    from trx_fpr_temp 
                  where approval_by is not null 
                    and (valid=9 and approval_status=1
                          or valid=0 and approval_status=0
                          or valid=77 and approval_status=1) 
                  group by fprid) q on q.fprid = a.urutan
      left join map_cifsinvest_country mcc on mcc.CIF_CODE = a.HR_DD_WARGANEGARA_LAINNYA
      left join RDO_PAR_MAP id_map on id_map.PAR_VAL_MYBO = a.DD_JENIS_ID and id_map.PAR_ID = 'identity'
    where a.JENIS_REKENING = '0'
      --and a.VALID = 1
        and isnull(a.SINVEST_FLAG_KYC, 0) = 0  
        and exists (select 1 from vw_cust_cif_sam_rfrrl cs where cs.cif_id = a.cif_sms)
        and DATEADD(dd, DATEDIFF(dd, 0, q.Apv_Date), 0) between @date_from and @date_to
    order by a.urutan desc
    
    
    END TRY
  BEGIN CATCH
  INSERT INTO [dbo].[DN_RDOAPI_003]([AUDIT_DT],[ERROR_CODE],[ERROR_MESSAGE]) VALUES(GETDATE(), ERROR_NUMBER(),ERROR_MESSAGE()) 

  END CATCH


  BEGIN TRY
    insert into [DN_RDOAPI_004](
      [AUDIT_USR],
      [AUDIT_PRG],
      [AUDIT_DT],
      [PRFL_ID],
      --[PRFL_SEQ_NO],
      type,
      ClientCode,
      NamaPerusahaan,
      NegaraDomisili,
      NoSIUP,
      SIUPExpirationDate,
      NoSKD,
      ExpiredDateSKD,
      NPWPNo,
      RegistrationNPWP,
      PlaceofEstablishment,
      DateofEstablishment,
      CompanyType,
      CompanyCharacteristic,
      IncomeLevel,
      InvestmentObjective,
      SourceofFund,
      CompanyAddress,
      CompanyCityName,
      CityRHB,
      CompanyPostalCode,
      TeleponBisnis,
      CompanyFax,
      CompanyMail,
      StatementType,
      NamaBank1,
      BankCountry1,
      BankBranchName1,
      MataUang1,
      NomorRekening1,
      NamaNasabah1,
      NamaBank2,
      BankCountry2,
      BankBranchName2,
      MataUang2,
      NomorRekening2,
      NamaNasabah2,
      NamaBank3,
      BankCountry3,
      BankBranchName3,
      MataUang3,
      NomorRekening3,
      NamaNasabah3,
      IFUA_NO
    )
    select top 10 
      'SYSTEM' as AUDIT_USR,
      @AUDIT_PRG as AUDIT_PRG,
      GETDATE() as AUDIT_DT,
      @profile_id as PRFL_ID,
    -- @PRFL_SEQ_NO as PRFL_SEQ_NO,
      case isnull(h.IFUA_NO,'') 
        when '' then '1' 
        else '2'
      end as type,  
      RIGHT(replicate('0',6) + convert(varchar(6),a.urutan), 6) as [ClientCode],
      NAMA_NASABAH as NamaPerusahaan,
      ISNULL(mcc.ISO_CODE, '') NegaraDomisili,
      case when LEN(REPLACE(REPLACE(REPLACE(ISNULL(cast(ad_no_siup as varchar), ''), '0', ''), '-', ''), 'NULL', '')) = 0 then '' 
        else ISNULL(cast(ad_no_siup as varchar), '') 
      end as NoSIUP,
      case when LEN(REPLACE(REPLACE(REPLACE(ISNULL(cast(ad_no_siup as varchar), ''), '0', ''), '-', ''), 'NULL', '')) = 0 then '' 
        else replace(convert(varchar(10),isnull(ad_no_siup_exp, '2500-01-01'),120), '1900-01-01', '2500-01-01') 
      end SIUPExpirationDate,
      case when LEN(REPLACE(REPLACE(ISNULL(cast(ad_no_surat_domisili as varchar), ''), '0', ''), '-', '')) = 0 then '' 
        else ISNULL(cast(ad_no_surat_domisili as varchar), '') 
      end as NoSKD,
      case when LEN(REPLACE(REPLACE(ISNULL(cast(ad_no_surat_domisili as varchar), ''), '0', ''), '-', '')) = 0 then '' 
        else replace(convert(varchar(10),ISNULL(ad_surat_domisili_exp, '2500-01-01'),120), '1900-01-01', '2500-01-01') 
      end ExpiredDateSKD,
      case when len(replace(dbo.StripID(ISNULL(a.di_no_npwp, '')), '-', '')) <> 15 then '' 
        else dbo.stripID(isnull(a.di_no_npwp,'')) 
      end [NPWPNo],
      case when len(replace(dbo.StripID(ISNULL(a.di_no_npwp, '')), '-', '')) <> 15 then '' 
          else REPLACE(convert(varchar,ISNULL(ad_tanggal_registrasi, '19000101') ,112), '19000101', '') 
      end NPWPRegistrationDate,
      --ISNULL(mcc.ISO_CODE, '') CountryofEstablishment,
      ISNULL(di_lokasi_pendirian, '') as PlaceofEstablishment,
      case when convert(varchar(10), ISNULL(di_tgl_pendirian, '1900-01-01') ,120) =  '1900-01-01'  then '' 
        else convert(varchar(10),di_tgl_pendirian ,120) 
      end  DateofEstablishment,
      --cast(a.AD_NO_AKTA_BERDIRI as varchar(50)) ArticlesofAssociationNo,
      case di_jenis_institusi
      when '0' then '2'
      WHEN '1' then '3'
      WHEN '2' then '4'
      WHEN '3' then '6'
      WHEN '4' then '8'
      WHEN '5' then '8'
      WHEN '6' then '8'
      WHEN '7' then '8'
      WHEN '8' then '2'
      WHEN '9' then '1'
      WHEN '10' then '1'
      else '' end CompanyType,
      case di_karakter_pt
      when '0' then '1'
      WHEN '1' then '6'
      WHEN '2' then '8'
      WHEN '3' then '5'
      WHEN '4' then '7'
      WHEN '5' then '8' ELSE  '' END AS CompanyCharacteristic,
      case laba_bersih
      when '0' then '1'
      WHEN '1' then '1'
      WHEN '2' then '1'
      WHEN '3' then '1'
      WHEN '4' then '1'
      WHEN '5' then '2'
      WHEN '6' then '3'
      WHEN '7' then '4'
      WHEN '8' then '5'
      else '' end IncomeLevel,
      --'3' InvestorsRiskProfile, --sementara di default sampe ada tambahan di BO
      case 
        when tujuan_investasi >=32 then '5' 
        when tujuan_investasi >=16 then '3' 
        when tujuan_investasi >=8 then '4' 
        when tujuan_investasi >=4 then '1' 
        when tujuan_investasi >=2 then '2' 
        when tujuan_investasi >=1 then '1' 
        else '5'
        end as [InvestmentObjective], 
      case 
        when di_jenis_institusi in (0,6) then 3 --Yayasan/lainnya -> savingInterest
        else 1 -- > Business Profit
      end SourceofFund,
    --   case di_domisili_kantor 
    --   when '0' then '1'
    --   when '1' then '2'
    --   else
    --   '2' end [AssetOwner], 
      left(
      REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
      case when (len(di_alamat)=0) or (di_alamat='<br>')  then '' 
      else case when charindex('EMAIL Alternatif',di_alamat)>0  then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Alternatif email',di_alamat)>0 then left(di_alamat,charindex('Alternatif',di_alamat)-2) 
      else case when charindex('Alternative email',di_alamat)>0 then left(di_alamat,charindex('Alternative',di_alamat)-2) 
      else case when charindex('Email Alternatif',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Email Alternative',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Email Lain',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Email Tambahan',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Email Alt',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else case when charindex('Email',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
      else isnull(di_alamat,'') end end end end end end end end end end
      , CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' ') 
      ,100)
      as CompanyAddress,
      REPLACE(
      case when (len(di_kota)=0) then '0'
          when isnumeric(di_kota)=1 then isnull(cast(convert(int, k.kode) as varchar(7)),'0')--isnull(cast(di_kota as varchar(7)),cast(di_kota as varchar(30)))
          when (k.kode is null) then isnull(k.kode, '0')
          when (di_kota is null) then '9999'	
      else isnull(cast(convert(int, k.kode) as varchar(7)),'0')  
      end
      ,'.0','') as CompanyCityName, --ini name apa code??
      case when (len(di_kota)=0) then ''
          when isnumeric(di_kota)=1 then isnull(cast(di_kota as varchar(30)),cast(di_kota as varchar(30)))
          when (k.kode is null) then isnull(di_kota, '')
          when (di_kota is null) then ''	
      else isnull(cast(di_kota as varchar(30)),'') end CityRHB,
      case when (len(di_kodepos)=0) then '0' else isnull(di_kodepos, '0')  end CompanyPostalCode,
      ISNULL(cast(di_telp as varchar), '') [TeleponBisnis], 
      ISNULL(cast(di_fax as varchar), '') [CompanyFax], 
      ISNULL(di_email, '') [CompanyMail], 
      case when LEN(ISNULL(di_email, '')) = 0 then '1' else '2' end StatementType,

        ISNULL(b1map.PAR_VAL_RDO, '') as [NamaBank1],  
        case isnull(b1map.PAR_VAL_RDO, '')
            when '' then ''
                else 'ID'
            end as [BankCountry1],  
        ISNULL(b.cabang, '') as [BankBranchName1], 
        case when isnull(b1map.PAR_VAL_RDO, '') = '' then '' 
            else case when isnull(b.currency, 'IDR') = 'IDR' then '1'
                      when isnull(b.currency, 'IDR') = 'USD' then '2'
                      when isnull(b.currency, 'IDR') = 'EUR' then '3'
                else '4' --lainnya
                end
        end as [MataUang1], 
        dbo.stripID(ISNULL(b.no_rek, '')) as [NomorRekening1], 
        ISNULL(b.pemilik, '') as [NamaNasabah1],
      
        ISNULL(b2map.PAR_VAL_RDO, '') as [NamaBank2],  
        case isnull(b2map.PAR_VAL_RDO, '')
        when '' then ''
            else 'ID'
        end as [BankCountry2],  
        ISNULL(d.cabang, '') as [BankBranchName2],
        case when isnull(b2map.PAR_VAL_RDO, '') = '' then '' 
            else case when isnull(d.currency, 'IDR') = 'IDR' then '1'
                      when isnull(d.currency, 'IDR') = 'USD' then '2'
                      when isnull(d.currency, 'IDR') = 'EUR' then '3'
                else '4' --lainnya
                end 
        end as [MataUang2], 
        dbo.StripID(ISNULL(d.no_rek, '')) as [NomorRekening2], 
        ISNULL(d.pemilik, '') as [NamaNasabah2],

        ISNULL(b3map.PAR_VAL_RDO, '') as [NamaBank3],  
        case isnull(b3map.PAR_VAL_RDO, '')
        when '' then ''
            else 'ID'
        end as [BankCountry3], 
        ISNULL(f.cabang, '') as [BankBranchName3], 
        case when ISNULL(b3map.PAR_VAL_RDO, '') = '' then '' 
            else case when isnull(f.currency, 'IDR') = 'IDR' then '1'
                      when isnull(f.currency, 'IDR') = 'USD' then '2'
                      when isnull(f.currency, 'IDR') = 'EUR' then '3'
                else '4' --lainnya
                end
        end as [MataUang3], 
        dbo.StripID(ISNULL(f.no_rek, '')) as [NomorRekening3], 
        ISNULL(f.pemilik, '') as [NamaNasabah3],
        isnull(h.IFUA_NO,'')IFUA_NO
      from FPR a
      left join fpr_keuangan_pt fkeu on fkeu.FPR_ID=a.urutan
      left join aria_provinsi k on a.di_kota=k.[kotamadya/kabupaten]
      --left join aria_provinsi m on a.dd_kota_tinggal=m.[kotamadya/kabupaten]
      left join fpr_bank_sinvest_temp b
        on b.fpr_id = a.urutan    
      and b.SEQ_NO = 1
      left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) c 
        on b.bank = c.BANK_ID
      left join SINVEST_BI_MBR_CD n on c.MAP_ID = n.BI_CD
      left join RDO_PAR_MAP b1map on b1map.PAR_VAL_MYBO = n.BI_CD and b1map.PAR_ID = 'MSTR_BANK'
      left join fpr_bank_sinvest_temp d
        on d.fpr_id = a.urutan    
      and d.SEQ_NO = 2       
      left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) e 
        on d.bank = e.BANK_ID
      left join SINVEST_BI_MBR_CD o on e.MAP_ID = o.BI_CD
      left join RDO_PAR_MAP b2map on b2map.PAR_VAL_MYBO = o.BI_CD and b2map.PAR_ID = 'MSTR_BANK'
      left join fpr_bank_sinvest_temp f
        on f.fpr_id = a.urutan    
      and f.SEQ_NO = 3   
      left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) g 
        on f.bank = g.BANK_ID
      left join SINVEST_BI_MBR_CD p on g.MAP_ID = p.BI_CD
      left join RDO_PAR_MAP b3map on b3map.PAR_VAL_MYBO = p.BI_CD and b3map.PAR_ID = 'MSTR_BANK'
      left join (
            select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
            from cust_cif a
            inner join cust_product b on a.CIF_ID = b.CIF_ID
            inner join SINVEST_IFUA_MAP c on b.Niaga_acc = c.niaga_acc 
              and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
              and c.IFUA_NO not like '%T%'
            group by a.CIF_ID
            union
            select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
            from cust_cif a
            inner join cust_product b on a.CIF_ID = b.CIF_ID
            inner join SINVEST_IFUA_MAP_delete c on b.Niaga_acc = c.niaga_acc 
              and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
              and c.IFUA_NO not like '%T%'
              and c.NOTES like '%suspend%'
            group by a.CIF_ID
      ) h on a.CIF_SMS = h.CIF_ID
      inner join (select fprid, max(Approval_Date) Apv_Date
                    from trx_fpr_temp 
                  where approval_by is not null 
                    and (valid=9 and approval_status=1
                            or valid=0 and approval_status=0
                            or valid=77 and approval_status=1) 
                  group by fprid) q on q.fprid = a.urutan
    left join map_cifsinvest_country mcc on mcc.CIF_CODE = a.HR_DI_NEGARA
    where a.JENIS_REKENING = '1'
        and isnull(a.SINVEST_FLAG_KYC, 0) = 0  
        and exists (select 1 from vw_cust_cif_sam_rfrrl cs where cs.cif_id = a.cif_sms)
        and DATEADD(dd, DATEDIFF(dd, 0, q.Apv_Date), 0) between @date_from and @date_to
    order by a.urutan desc
  END TRY 
  BEGIN CATCH
    INSERT INTO [dbo].[DN_RDOAPI_004]([AUDIT_DT],[ERROR_CODE],[ERROR_MSG]) VALUES(GETDATE(), ERROR_NUMBER(),ERROR_MESSAGE()) 
  END CATCH

END

END


GO
