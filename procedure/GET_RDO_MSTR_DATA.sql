CREATE PROCEDURE [dbo].[GET_RDO_MSTR_DATA](
    @ps_AUDIT_USR varchar(150),
    @ps_AUDIT_PRG varchar(50),
    @ps_PAR_ID varchar(100),
    @ps_PAR_VAL varchar(100),
    @ps_PAR_DESC varchar(150),
    @ps_PAR_VAL2 varchar(100),
    @ps_PAR_DESC2 varchar(150),
    @pd_LAST_UPDT datetime
)

as


IF EXISTS (
    SELECT 1
    FROM RDO_PAR_MAP
    WHERE PAR_ID = @ps_PAR_ID
        and PAR_VAL_RDO = @ps_PAR_VAL
        and PAR_VAL_MYBO = @ps_PAR_VAL2
)
BEGIN
    delete from RDO_PAR_MAP WHERE PAR_ID = @ps_PAR_ID and PAR_VAL_RDO = @ps_PAR_VAL and PAR_VAL_MYBO = @ps_PAR_VAL2
END

     INSERT INTO RDO_PAR_MAP (
        AUDIT_USR, AUDIT_PRG, AUDIT_DT, PAR_ID, PAR_VAL_RDO, 
        PAR_VAL_MYBO, PAR_DESC_RDO, PAR_DESC_MYBO,  LAST_UPDT
    )
    VALUES(
        @ps_AUDIT_USR, @ps_AUDIT_PRG, getDate(), @ps_PAR_ID, @ps_PAR_VAL, 
        @ps_PAR_VAL2, @ps_PAR_DESC, @ps_PAR_DESC2, @pd_LAST_UPDT
    )
GO
