CREATE PROCEDURE [dbo].[RDO_NAV_PRFRMNC_SYNC](
--declare 
	@ps_FUND_CODE varchar(50),
	@ps_NAV_DT varchar(20),
	@pn_TMINONE float,
    @pn_TMINTWO float,
    @pn_TMINTHREE float,
    @pn_MOM float,
    @pn_THREEMO float,
    @pn_SIXMO float,
    @pn_MTD float,
    @pn_YTD float,
    @pn_YOY float,
    @pn_SI float,
    @pn_YMINTWO float,
    @pn_YMINTHREE float,
    @pn_YMINFOUR float,
    @pn_YMINFIVE float
)

as

declare @vs_PRD_ID varchar(10),
	@vn_ERR_STS int,
	@vs_ERR_MSG varchar(150),
    @PROC_STS varchar(5),
    @ERR_STS varchar(5),
    @ERR_MSG varchar(500),
    @AUTOID int,
    @RSLT_MSG varchar(500);

-----------------------------------------------------------
select @vs_PRD_ID = prd_id
from NFS_PRD_MAP (nolock)
where map_id = @ps_FUND_CODE


-----------------------------------------------------------
  set @PROC_STS = '0'
  set @ERR_STS = '0'
  set @ERR_MSG = ''
 

INSERT INTO dbo.RDO_NAV_SYNC_LOG(
    AUDIT_USR, AUDIT_PRG, AUDIT_DT, PRFL_ID, NAV_DT, FUND_CODE, 
    TMINONE, TMINTWO, TMINTHREE,
    MOM, THREEMO, SIXMO,
    MTD, YTD, YOY, SI,
    YMINTWO, YMINTHREE, YMINFOUR, YMINFIVE
)
VALUES
(
    'SYSTEM', 'RDO_NAV_PRFMNC_SYNC', GETDATE(), 'NAV_PRFRMNC', @ps_NAV_DT, @ps_FUND_CODE, 
    @pn_TMINONE, @pn_TMINTWO, @pn_TMINTHREE,
    @pn_MOM, @pn_THREEMO, @pn_SIXMO,
    @pn_MTD, @pn_YTD, @pn_YOY, @pn_SI,
    @pn_YMINTWO, @pn_YMINTHREE, @pn_YMINFOUR, @pn_YMINFIVE
    
)

set @AUTOID = SCOPE_IDENTITY()

IF @@ERROR <> 0
BEGIN
    SET @ERR_STS = '1'
    SET @ERR_MSG = 'Error'
END

------------------------------------------------------------
IF @ERR_STS = 0
BEGIN
    IF EXISTS (
        SELECT 1
        FROM info_rkdn ir
        WHERE info_id = @vs_PRD_ID
            and NAB_DT = convert(datetime,@ps_NAV_DT)
    )
    BEGIN
        UPDATE INFO_RKDN
        set LAST_1_MNTH = @pn_MOM,
            YEAR_TO_DT = @pn_YTD,
            LAST_1_YEAR = @pn_YOY
        WHERE INFO_ID = @vs_PRD_ID
            and NAB_DT = convert(datetime, @ps_NAV_DT)
    END
    ELSE
    BEGIn
        SET @ERR_STS = '1'
        SET @ERR_MSG = 'NAV Data Not Found'
    END

    IF @@ERROR <> 0
    BEGIN
      SET @ERR_STS = '1'
      SET @ERR_MSG = 'Error'
    END

END

if @ERR_STS = '0' 
begin
    set @RSLT_MSG = 'NAV for <b>' + @ps_FUND_CODE + '</b> successfully uploaded'
end
else
begin
    set @RSLT_MSG = @ERR_MSG
end

update RDO_NAV_SYNC_LOG
set PROC_STS = '1',
      ERR_STS = @ERR_STS,
      ERR_MSG = @ERR_MSG,
      RSLT_MSG = @RSLT_MSG
where AUTOID = @AUTOID 

GO
