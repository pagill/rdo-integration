CREATE VIEW [dbo].[TRN_RDOAPI]

    as

    select 
        AUTOID,
        PRFL_ID,
        PRFL_SEQ_NO,
        case when SA_REFF_NO = '' then IFUA_NO + '_' + cast(AUTOID as varchar) else SA_REFF_NO end SA_REFF_NO, 
        IFUA_NO,
        TRN_DT,
        FUND_CODE,
        SWC_FUND_CODE,
        TRN_TYPE,
        REPLACE(LTRIM(STR(AMOUNT_NOMINAL, 26, 4)), '.0000', '')AMOUNT_NOMINAL,
        REPLACE(LTRIM(STR(AMOUNT_UNIT, 26, 4)), '.0000', '')AMOUNT_UNIT,
        REPLACE(LTRIM(STR(FEE_PCT, 26, 2)), '.00', '') FEE_PCT,
        REPLACE(LTRIM(STR(FEE_NOMINAL, 26, 2)), '.00', '')FEE_NOMINAL,
        SourceOfFund,
        BankRecipient,
        AMOUNT_ALLUNIT Type,
        ApprovedTime,
        TrxUnitPaymentProvider,
        TrxUnitPaymentType
    FROM [DN_RDOAPI]
    where PROC_STS = 'N'