select
  case isnull(h.IFUA_NO,'') 
    when '' then '1' 
    else '2'
  end as type,  
  RIGHT(replicate('0',6) + convert(varchar(6),a.urutan), 6) as [ClientCode],
  NAMA_NASABAH as NamaPerusahaan,
  ISNULL(mcc.ISO_CODE, '') NegaraDomisili,
  case when LEN(REPLACE(REPLACE(REPLACE(ISNULL(cast(ad_no_siup as varchar), ''), '0', ''), '-', ''), 'NULL', '')) = 0 then '' 
    else ISNULL(cast(ad_no_siup as varchar), '') 
  end as NoSIUP,
  case when LEN(REPLACE(REPLACE(REPLACE(ISNULL(cast(ad_no_siup as varchar), ''), '0', ''), '-', ''), 'NULL', '')) = 0 then '' 
    else replace(convert(varchar,isnull(ad_no_siup_exp, '25000101'),112), '19000101', '25000101') 
  end SIUPExpirationDate,
  case when LEN(REPLACE(REPLACE(ISNULL(cast(ad_no_surat_domisili as varchar), ''), '0', ''), '-', '')) = 0 then '' 
    else ISNULL(cast(ad_no_surat_domisili as varchar), '') 
  end as NoSKD,
  case when LEN(REPLACE(REPLACE(ISNULL(cast(ad_no_surat_domisili as varchar), ''), '0', ''), '-', '')) = 0 then '' 
    else replace(convert(varchar,ISNULL(ad_surat_domisili_exp, '25000101'),112), '19000101', '25000101') 
  end ExpiredDateSKD,
  ISNULL(cast(dbo.StripID(di_no_npwp) as varchar), '') NPWPNo,
  case when len(cast(di_no_npwp as varchar(30)))=0 then '' 
    else REPLACE(convert(varchar,ISNULL(ad_tanggal_registrasi, '19000101') ,112), '19000101', '') 
  end  RegistrationNPWP,
  ISNULL(mcc.ISO_CODE, '') CountryofEstablishment,
  ISNULL(di_lokasi_pendirian, '') as PlaceofEstablishment,
  case when convert(varchar(10), ISNULL(di_tgl_pendirian, '1900-01-01') ,120) =  '1900-01-01'  then '' 
    else convert(varchar,di_tgl_pendirian ,120) 
  end  DateofEstablishment,
  --cast(a.AD_NO_AKTA_BERDIRI as varchar(50)) ArticlesofAssociationNo,
  case di_jenis_institusi
  when '0' then '2'
  WHEN '1' then '3'
  WHEN '2' then '4'
  WHEN '3' then '6'
  WHEN '4' then '8'
  WHEN '5' then '8'
  WHEN '6' then '8'
  WHEN '7' then '8'
  WHEN '8' then '2'
  WHEN '9' then '1'
  WHEN '10' then '1'
  else '' end CompanyType,
  case di_karakter_pt
  when '0' then '1'
  WHEN '1' then '6'
  WHEN '2' then '8'
  WHEN '3' then '5'
  WHEN '4' then '7'
  WHEN '5' then '8' ELSE  '' END AS CompanyCharacteristic,
  case laba_bersih
  when '0' then '1'
  WHEN '1' then '1'
  WHEN '2' then '1'
  WHEN '3' then '1'
  WHEN '4' then '1'
  WHEN '5' then '2'
  WHEN '6' then '3'
  WHEN '7' then '4'
  WHEN '8' then '5'
  else '' end IncomeLevel,
  --'3' InvestorsRiskProfile, --sementara di default sampe ada tambahan di BO
  case 
  	when tujuan_investasi >=32 then '5' 
  	when tujuan_investasi >=16 then '3' 
  	when tujuan_investasi >=8 then '4' 
  	when tujuan_investasi >=4 then '1' 
  	when tujuan_investasi >=2 then '2' 
  	when tujuan_investasi >=1 then '1' 
  	else '5'
  	end as [InvestmentObjective], 
  case 
    when di_jenis_institusi in (0,6) then 3 --Yayasan/lainnya -> savingInterest
    else 1 -- > Business Profit
  end SourceofFund,
--   case di_domisili_kantor 
--   when '0' then '1'
--   when '1' then '2'
--   else
--   '2' end [AssetOwner], 
  left(
  REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
  case when (len(di_alamat)=0) or (di_alamat='<br>')  then '' 
  else case when charindex('EMAIL Alternatif',di_alamat)>0  then left(di_alamat,charindex('Email',di_alamat)-2) 
  else case when charindex('Alternatif email',di_alamat)>0 then left(di_alamat,charindex('Alternatif',di_alamat)-2) 
  else case when charindex('Alternative email',di_alamat)>0 then left(di_alamat,charindex('Alternative',di_alamat)-2) 
  else case when charindex('Email Alternatif',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
  else case when charindex('Email Alternative',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
  else case when charindex('Email Lain',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
  else case when charindex('Email Tambahan',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
  else case when charindex('Email Alt',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
  else case when charindex('Email',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
  else isnull(di_alamat,'') end end end end end end end end end end
  , CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' ') 
  ,100)
  as CompanyAddress,
  REPLACE(
  case when (len(di_kota)=0) then '0'
       when isnumeric(di_kota)=1 then isnull(cast(convert(int, k.kode) as varchar(7)),'0')--isnull(cast(di_kota as varchar(7)),cast(di_kota as varchar(30)))
       when (k.kode is null) then isnull(k.kode, '0')
       when (di_kota is null) then '9999'	
  else isnull(cast(convert(int, k.kode) as varchar(7)),'0')  
  end
  ,'.0','') as CompanyCityName, --ini name apa code??
  case when (len(di_kota)=0) then ''
       when isnumeric(di_kota)=1 then isnull(cast(di_kota as varchar(30)),cast(di_kota as varchar(30)))
       when (k.kode is null) then isnull(di_kota, '')
       when (di_kota is null) then ''	
  else isnull(cast(di_kota as varchar(30)),'') end CityRHB,
  case when (len(di_kodepos)=0) then '0' else isnull(di_kodepos, '0')  end CompanyPostalCode,
  ISNULL(cast(di_telp as varchar), '') [TeleponBisnis], 
  ISNULL(cast(di_fax as varchar), '') [CompanyFax], 
  ISNULL(di_email, '') [CompanyMail], 
  case when LEN(ISNULL(di_email, '')) = 0 then '1' else '2' end StatementType,

    ISNULL(b1map.PAR_VAL_RDO, '') as [NamaBank1],  
    case isnull(b1map.PAR_VAL_RDO, '')
        when '' then ''
            else 'ID'
        end as [BankCountry1],  
    ISNULL(b.cabang, '') as [BankBranchName1], 
    case when isnull(b1map.PAR_VAL_RDO, '') = '' then '' 
        else case when isnull(b.currency, 'IDR') = 'IDR' then '1'
                  when isnull(b.currency, 'IDR') = 'USD' then '2'
                  when isnull(b.currency, 'IDR') = 'EUR' then '3'
             else '4' --lainnya
             end
    end as [MataUang1], 
    dbo.stripID(ISNULL(b.no_rek, '')) as [NomorRekening1], 
    ISNULL(b.pemilik, '') as [NamaNasabah1],
  
    ISNULL(b2map.PAR_VAL_RDO, '') as [NamaBank2],  
    case isnull(b2map.PAR_VAL_RDO, '')
    when '' then ''
        else 'ID'
    end as [BankCountry2],  
    ISNULL(d.cabang, '') as [BankBranchName2],
    case when isnull(b2map.PAR_VAL_RDO, '') = '' then '' 
        else case when isnull(d.currency, 'IDR') = 'IDR' then '1'
                  when isnull(d.currency, 'IDR') = 'USD' then '2'
                  when isnull(d.currency, 'IDR') = 'EUR' then '3'
             else '4' --lainnya
             end 
    end as [MataUang2], 
    dbo.StripID(ISNULL(d.no_rek, '')) as [NomorRekening2], 
    ISNULL(d.pemilik, '') as [NamaNasabah2],

    ISNULL(b3map.PAR_VAL_RDO, '') as [NamaBank3],  
    case isnull(b3map.PAR_VAL_RDO, '')
    when '' then ''
        else 'ID'
    end as [BankCountry3], 
    ISNULL(f.cabang, '') as [BankBranchName3], 
    case when ISNULL(b3map.PAR_VAL_RDO, '') = '' then '' 
        else case when isnull(f.currency, 'IDR') = 'IDR' then '1'
                  when isnull(f.currency, 'IDR') = 'USD' then '2'
                  when isnull(f.currency, 'IDR') = 'EUR' then '3'
             else '4' --lainnya
             end
    end as [MataUang3], 
    dbo.StripID(ISNULL(f.no_rek, '')) as [NomorRekening3], 
    ISNULL(f.pemilik, '') as [NamaNasabah3]
  from FPR a
  left join fpr_keuangan_pt fkeu on fkeu.FPR_ID=a.urutan
  left join aria_provinsi k on a.di_kota=k.[kotamadya/kabupaten]
  --left join aria_provinsi m on a.dd_kota_tinggal=m.[kotamadya/kabupaten]
  left join fpr_bank_sinvest_temp b
    on b.fpr_id = a.urutan    
   and b.SEQ_NO = 1
  left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) c 
    on b.bank = c.BANK_ID
  left join SINVEST_BI_MBR_CD n on c.MAP_ID = n.BI_CD
  left join RDO_PAR_MAP b1map on b1map.PAR_VAL_MYBO = n.BI_CD and b1map.PAR_ID = 'MSTR_BANK'
  left join fpr_bank_sinvest_temp d
    on d.fpr_id = a.urutan    
   and d.SEQ_NO = 2       
  left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) e 
    on d.bank = e.BANK_ID
  left join SINVEST_BI_MBR_CD o on e.MAP_ID = o.BI_CD
  left join RDO_PAR_MAP b2map on b2map.PAR_VAL_MYBO = o.BI_CD and b2map.PAR_ID = 'MSTR_BANK'
  left join fpr_bank_sinvest_temp f
    on f.fpr_id = a.urutan    
   and f.SEQ_NO = 3   
  left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) g 
    on f.bank = g.BANK_ID
  left join SINVEST_BI_MBR_CD p on g.MAP_ID = p.BI_CD
  left join RDO_PAR_MAP b3map on b3map.PAR_VAL_MYBO = p.BI_CD and b3map.PAR_ID = 'MSTR_BANK'
  left join (
        select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                 from cust_cif a
                inner join cust_product b on a.CIF_ID = b.CIF_ID
                inner join SINVEST_IFUA_MAP c 
                   on b.Niaga_acc = c.niaga_acc 
                  and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                  and c.IFUA_NO not like '%T%'
                group by a.CIF_ID) h 
           on a.CIF_SMS = h.CIF_ID
  
  inner join (select fprid, max(Approval_Date) Apv_Date
                from trx_fpr_temp 
               where approval_by is not null 
                 and (valid=9 and approval_status=1
                        or valid=0 and approval_status=0
                        or valid=77 and approval_status=1) 
               group by fprid) q on q.fprid = a.urutan
  left join map_cifsinvest_country mcc on mcc.CIF_CODE = a.HR_DI_NEGARA
  where a.JENIS_REKENING = '1'
    --and a.VALID = 1
    and isnull(a.sinvest_flag_kyc, 0) = 0
   -- and a.cif_sms = @cif_sms

   {
            "Code": "1",
            "ID": "MataUang",
            "Desc": "IDR",
            "LastUpdate": "2017-10-06T16:28:19."
        },
        {
            "Code": "2",
            "ID": "MataUang",
            "Desc": "USD",
            "LastUpdate": "2017-10-06T16:28:19."
        },
        {
            "Code": "3",
            "ID": "MataUang",
            "Desc": "EURO",
            "LastUpdate": "2017-10-06T16:28:19."
        },
        {
            "Code": "4",
            "ID": "MataUang",
            "Desc": "LAINNYA / OTHERS",
            "LastUpdate": "2017-10-06T16:28:19."
        },


        select * from reksadana.dbo.RDO_PAR_MAP

        insert into reksadana.dbo.RDO_PAR_MAP values ('AlbertMario', 'Script', getDate(), 'MataUang', '1', 'IDR', 'IDR', 'Indonesian Rupiah', getDate())
        
        insert into reksadana.dbo.RDO_PAR_MAP values ('AlbertMario', 'Script', getDate(), 'MataUang', '2', 'USD', 'USD', 'US Dollar', getDate())
        
        insert into reksadana.dbo.RDO_PAR_MAP values ('AlbertMario', 'Script', getDate(), 'MataUang', '3', 'EURO', 'EUR', 'Euro Dollar', getDate())
        


        select * From reksadana.dbo.Mst_Currency