select 
    'DH002' SACode,
	'PT Sinarmas Asset Management' SAName,
    a.clientsid SID,
	ISNULL(h.ifua_no, '') IFUA,
	--[dbo].[GET_IFUA_NO_BY_CIF_ID](a.cif_sms) IFUA,
    ISNULL(a.NAMA_NASABAH, a.di_nama) as Name,
	RIGHT('000000' +  cast(a.urutan as varchar), 6) clientcode,
	'2' investortype,
	ISNULL(NAMA_NASABAH, a.di_nama) companyname,
    'ID' CountryofDomicile,
    ISNULL(cast(ad_no_siup as varchar), '') as SIUPNo,
    case when len(cast(ad_no_siup_exp as varchar(30)))=0 then '0' else ISNULL(convert(varchar,ad_no_siup_exp,112), '19000101') end SIUPExpirationDate,
    ISNULL(cast(ad_no_surat_domisili as varchar), '') as SKDNo,
    case when len(cast(ad_surat_domisili_exp as varchar(30)))=0 then '0' else ISNULL(convert(varchar,ad_surat_domisili_exp,112), '') end SKDExpirationDate,
    ISNULL(cast(dbo.stripid(di_no_npwp) as varchar), '') NPWPNo,
    case when len(cast(ad_tanggal_registrasi as varchar(30)))=0 then '0' else ISNULL(convert(varchar,ad_tanggal_registrasi,112), '19000101') end  NPWPRegistrationDate,
    '' CountryofEstablishment,
    ISNULL(di_lokasi_pendirian, '') as PlaceofEstablishment,
    case when len(cast(di_tgl_pendirian  as varchar(30)))=0 then '0' else ISNULL(convert(varchar,di_tgl_pendirian ,112), '19000101') end  DateofEstablishment,
    ad_no_akta_berdiri as ArticlesofAssociationNo,
    case di_jenis_institusi
    when '0' then '2'
    WHEN '1' then '3'
    WHEN '2' then '4'
    WHEN '3' then '6'
    WHEN '4' then '8'
    WHEN '5' then '8'
    WHEN '6' then '8'
    WHEN '7' then '8'
    WHEN '8' then '2'
    WHEN '9' then '1'
    WHEN '10' then '1'
    else '' end CompanyType,
    case di_karakter_pt
    when '0' then '8'
    WHEN '1' then '6'
    WHEN '2' then '8'
    WHEN '3' then '5'
    WHEN '4' then '7'
    WHEN '5' then '8' ELSE  '' END AS CompanyCharacteristic,
    case laba_bersih
    when '0' then '1'
    WHEN '1' then '1'
    WHEN '2' then '1'
    WHEN '3' then '1'
    WHEN '4' then '1'
    WHEN '5' then '2'
    WHEN '6' then '3'
    WHEN '7' then '4'
    WHEN '8' then '5'
    else '' end IncomeLevel,
    '' InvestorsRiskProfile,
    case 
    	when tujuan_investasi >=32 then '5' 
    	when tujuan_investasi >=16 then '3' 
    	when tujuan_investasi >=8 then '4' 
    	when tujuan_investasi >=4 then '1' 
    	when tujuan_investasi >=2 then '2' 
    	when tujuan_investasi >=1 then '1' 
    	else '5'
    	end as [InvestmentObjective], 
    '' SourceofFund,
    case di_domisili_kantor 
    when '0' then '1'
    when '1' then '2'
    else
    '2' end [AssetOwner], 
    left(
    REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
    case when (len(di_alamat)=0) or (di_alamat='<br>')  then '' 
    else case when charindex('EMAIL Alternatif',di_alamat)>0  then left(di_alamat,charindex('Email',di_alamat)-2) 
    else case when charindex('Alternatif email',di_alamat)>0 then left(di_alamat,charindex('Alternatif',di_alamat)-2) 
    else case when charindex('Alternative email',di_alamat)>0 then left(di_alamat,charindex('Alternative',di_alamat)-2) 
    else case when charindex('Email Alternatif',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
    else case when charindex('Email Alternative',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
    else case when charindex('Email Lain',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
    else case when charindex('Email Tambahan',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
    else case when charindex('Email Alt',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
    else case when charindex('Email',di_alamat)>0 then left(di_alamat,charindex('Email',di_alamat)-2) 
    else isnull(di_alamat,'') end end end end end end end end end end
    , CHAR(13),' '), CHAR(10),' '), CHAR(9),' '), '<br>',' '), ',',' ') 
    ,100)
    as CompanyAddress,
    REPLACE(
    case when (len(di_kota)=0) then '0'
         when isnumeric(di_kota)=1 then isnull(cast(convert(int, k.kode) as varchar(7)),'0')--isnull(cast(di_kota as varchar(7)),cast(di_kota as varchar(30)))
         when (k.kode is null) then isnull(k.kode, '0')
         when (di_kota is null) then '9999'	
    else isnull(cast(convert(int, k.kode) as varchar(7)),'0')  
    end
    ,'.0','') as CompanyCityCode,
    case when (len(di_kota)=0) then ''
         when isnumeric(di_kota)=1 then isnull(cast(di_kota as varchar(30)),cast(di_kota as varchar(30)))
         when (k.kode is null) then isnull(di_kota, '')
         when (di_kota is null) then ''	
    else isnull(cast(di_kota as varchar(30)),'') end CompanyCityName,
    case when (len(di_kodepos)=0) then '0' else isnull(di_kodepos, '0')  end CompanyPostalCode,
    '' CountryofCompany,
    ISNULL(cast(di_telp as varchar), '') [OfficePhone], 
	ISNULL(cast(di_fax as varchar), '') [Facsimile], 
	case isnull(a.di_email,'') 
      when '' then '' 
      else a.di_email 
    end as [Email], 
    case status_surat_konfirmasi 
		when 1 then '2'
		else '1' 
	end StatementType,
    cast('' as varchar(50)) [AuthorizedPerson1-FirstName],
    cast('' as varchar(50)) [AuthorizedPerson1-MiddleName],
    cast('' as varchar(50)) [AuthorizedPerson1-LastName],
    cast('' as varchar(50))  [AuthorizedPerson1-Position],
    cast('' as varchar(50))  [AuthorizedPerson1-MobilePhone],
    cast('' as varchar(50)) [AuthorizedPerson1-Email],
    cast('' as varchar(50)) [AuthorizedPerson1-NPWPNo],
    cast('' as varchar(50))  [AuthorizedPerson1-KTPNo],
    cast('' as varchar(50)) [AuthorizedPerson1-KTPExpirationDate],
    cast('' as varchar(50)) [AuthorizedPerson1-PassportNo],
    cast('' as varchar(50)) [AuthorizedPerson1-PassportExpirationDate],
    cast('' as varchar(50)) [AuthorizedPerson2-FirstName],
    cast('' as varchar(50)) [AuthorizedPerson2-MiddleName],
    cast('' as varchar(50)) [AuthorizedPerson2-LastName],
    cast('' as varchar(50)) [AuthorizedPerson2-Position],
    cast('' as varchar(50)) [AuthorizedPerson2-MobilePhone],
    cast('' as varchar(50)) [AuthorizedPerson2-Email],
    cast('' as varchar(50)) [AuthorizedPerson2-NPWPNo],
    cast('' as varchar(50)) [AuthorizedPerson2-KTPNo],
    cast('' as varchar(50)) [AuthorizedPerson2-KTPExpirationDate],
    cast('' as varchar(50)) [AuthorizedPerson2-PassportNo],
    cast('' as varchar(50)) [AuthorizedPerson2-PassportExpirationDate],
    cast('' as varchar(50)) [AssetInfomationforthePast3Years-LastYear],
    cast('' as varchar(50)) [AssetInformationforthePast3Years-2YAgo],
    cast('' as varchar(50)) [AssetInformationforthePast3Years-3YAgo],
    cast('' as varchar(50)) [ProfitInformationforthePast3Years-LastYear],
    cast('' as varchar(50)) [ProfitInformationforthePast3Years-2YearsAgo],
    cast('' as varchar(50)) [ProfitInformationforthePast3Years-3YearsAgo],
    '' [FATCA], 
    '' [TIN], 
    '' [TINIssuanceCountry],
    '' [GIIN],
    '' [SubstantialUSOwnerName],
    '' [SubstantialUSOwnerAddress],
    '' [SubstantialUSOwnerTIN],
	'1' [Status],
	'' [OpeningDate],
	'' [DeactivationDate]
    from FPR a
    left join fpr_keuangan_pt fkeu on fkeu.FPR_ID=a.urutan
    left join aria_provinsi k on a.di_kota=k.[kotamadya/kabupaten]
    --left join aria_provinsi m on a.di_kota=m.[kotamadya/kabupaten]
    left join fpr_bank_sinvest_temp b
      on b.fpr_id = a.urutan    
     and b.SEQ_NO = 1
    left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) c 
      on b.bank = c.BANK_ID
    left join SINVEST_BI_MBR_CD n on c.MAP_ID = n.BI_CD
    left join fpr_bank_sinvest_temp d
      on d.fpr_id = a.urutan    
     and d.SEQ_NO = 2       
    left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) e 
      on d.bank = e.BANK_ID
    left join SINVEST_BI_MBR_CD o on e.MAP_ID = o.BI_CD
    left join fpr_bank_sinvest_temp f
      on f.fpr_id = a.urutan    
     and f.SEQ_NO = 3   
    left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) g 
      on f.bank = g.BANK_ID
    left join (select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                 from cust_cif a
                inner join cust_product b on a.CIF_ID = b.CIF_ID
                inner join SINVEST_IFUA_MAP c 
                   on b.Niaga_acc = c.niaga_acc 
                  and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                 -- and c.IFUA_NO not like '%T%'
                group by a.CIF_ID
				
				union 

				select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                 from cust_cif a
                inner join cust_product b on a.CIF_ID = b.CIF_ID
                inner join SINVEST_IFUA_MAP_DELETE c 
                   on b.Niaga_acc = c.niaga_acc 
                  and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                --  and c.IFUA_NO not like '%T%'
                group by a.CIF_ID
				
				) h 
           on a.CIF_SMS = h.CIF_ID
    left join SINVEST_BI_MBR_CD p on g.MAP_ID = p.BI_CD
    inner join (select fprid, max(Approval_Date) Apv_Date
                  from trx_fpr_temp 
                 where approval_by is not null 
                   and (valid=9 and approval_status=1
                        or valid=0 and approval_status=0
                        or valid=77 and approval_status=1) 
                 group by fprid) q on q.fprid = a.urutan
    where a.JENIS_REKENING = '1'
      --and a.VALID = 1
      and exists (select 1 from vw_cust_cif_sam_rfrrl cs where cs.cif_id = a.cif_sms)
      and DATEADD(dd, DATEDIFF(dd, 0, q.Apv_Date), 0) <= '20210521'
	  and ISNULL(nama_nasabah, '') not like '%TEST%'
	  and ISNULL(nama_nasabah, '') not like '%INSTITUSI AM%'
	  and ISNULL(nama_nasabah, '') not like '%NASABAH BSM%'
	  and ISNULL(nama_nasabah, '') not like '%NASABAH SAM%'
	  order by ISNULL(nama_nasabah, di_nama)


	  -------------- bank 1
	  select 
    'DH002' SACode,
	'PT Sinarmas Asset Management' SAName,
    a.clientsid SID,
	ISNULL(h.ifua_no, '') IFUA,
	--[dbo].[GET_IFUA_NO_BY_CIF_ID](a.cif_sms) IFUA,
    ISNULL(NAMA_NASABAH, di_nama) as Name,
	RIGHT('000000' +  cast(a.urutan as varchar), 6) clientcode,
	  '' as [REDMPaymentBankBICCode1], 
    ISNULL(c.map_id, '') as [REDMPaymentBankBIMemberCode1], 
     b.bank as [REDMPaymentBankName1], 
    case isnull(c.map_id,'')
      when '' then ''
      else 'ID'
    end as [REDMPaymentBankCountry1], 
     b.cabang as [REDMPaymentBankBranch1], 
     isnull(b.currency,
        case isnull(c.map_id,'')
          when '' then ''
          else 'IDR'
        end) as [REDMPaymentA/CCCY1], 
     dbo.stripid(b.no_rek) as [REDMPaymentA/CNo1], 
     b.pemilik as [REDMPaymentA/CName1],
	b.Seq_no [REDM Payment A/C Sequential Code],
	'1' [Status]

    from FPR a
    left join fpr_keuangan_pt fkeu on fkeu.FPR_ID=a.urutan
    left join aria_provinsi k on a.di_kota=k.[kotamadya/kabupaten]
    --left join aria_provinsi m on a.di_kota=m.[kotamadya/kabupaten]
    INNER join fpr_bank_sinvest_temp b
      on b.fpr_id = a.urutan    
     and b.SEQ_NO = 1
    left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) c 
      on b.bank = c.BANK_ID
    left join SINVEST_BI_MBR_CD n on c.MAP_ID = n.BI_CD
    left join (select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                 from cust_cif a
                inner join cust_product b on a.CIF_ID = b.CIF_ID
                inner join SINVEST_IFUA_MAP c 
                   on b.Niaga_acc = c.niaga_acc 
                  and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                 -- and c.IFUA_NO not like '%T%'
                group by a.CIF_ID
				
				union 

				select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                 from cust_cif a
                inner join cust_product b on a.CIF_ID = b.CIF_ID
                inner join SINVEST_IFUA_MAP_DELETE c 
                   on b.Niaga_acc = c.niaga_acc 
                  and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                --  and c.IFUA_NO not like '%T%'
                group by a.CIF_ID
				
				) h 
           on a.CIF_SMS = h.CIF_ID
    inner join (select fprid, max(Approval_Date) Apv_Date
                  from trx_fpr_temp 
                 where approval_by is not null 
                   and (valid=9 and approval_status=1
                        or valid=0 and approval_status=0
                        or valid=77 and approval_status=1) 
                 group by fprid) q on q.fprid = a.urutan
    where a.JENIS_REKENING = '1'
      --and a.VALID = 1
      and exists (select 1 from vw_cust_cif_sam_rfrrl cs where cs.cif_id = a.cif_sms)
      and DATEADD(dd, DATEDIFF(dd, 0, q.Apv_Date), 0) <= '20210521'
	  and ISNULL(nama_nasabah, '') not like '%TEST%'
	  and ISNULL(nama_nasabah, '') not like '%INSTITUSI AM%'
	  and ISNULL(nama_nasabah, '') not like '%NASABAH BSM%'
	  and ISNULL(nama_nasabah, '') not like '%NASABAH SAM%'
	  and ISNULL(b.pemilik, '') <> ''

	  union ----- bank2

	   select 
    'DH002' SACode,
	'PT Sinarmas Asset Management' SAName,
    a.clientsid SID,
	ISNULL(h.ifua_no, '') IFUA,
	--[dbo].[GET_IFUA_NO_BY_CIF_ID](a.cif_sms) IFUA,
    ISNULL(NAMA_NASABAH, di_nama) as Name,
	RIGHT('000000' +  cast(a.urutan as varchar), 6) clientcode,
    '' as [REDMPaymentBankBICCode2], 
    ISNULL(e.map_id, '') as [REDMPaymentBankBIMemberCode2], 
    d.bank as [REDMPaymentBankName2], 
    case isnull(e.map_id,'')
      when '' then ''
      else 'ID'
    end as [REDMPaymentBankCountry2], 
    d.cabang as [REDMPaymentBankBranch2], 
	isnull(d.currency,
        case isnull(e.map_id,'')
          when '' then ''
          else 'IDR'
        end) as [REDMPaymentA/CCCY2],
     dbo.stripid(d.no_rek) as [REDMPaymentA/CNo2],
     d.pemilik as [REDMPaymentA/CName2],
	d.Seq_no [REDM Payment A/C Sequential Code],
	'1' [Status]
    from FPR a
    left join fpr_keuangan_pt fkeu on fkeu.FPR_ID=a.urutan
    left join aria_provinsi k on a.di_kota=k.[kotamadya/kabupaten]
    --left join aria_provinsi m on a.di_kota=m.[kotamadya/kabupaten]
    inner join fpr_bank_sinvest_temp d
      on d.fpr_id = a.urutan    
     and d.SEQ_NO = 2       
    left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) e 
      on d.bank = e.BANK_ID
    left join SINVEST_BI_MBR_CD o on e.MAP_ID = o.BI_CD
   
    left join (select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                 from cust_cif a
                inner join cust_product b on a.CIF_ID = b.CIF_ID
                inner join SINVEST_IFUA_MAP c 
                   on b.Niaga_acc = c.niaga_acc 
                  and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                 -- and c.IFUA_NO not like '%T%'
                group by a.CIF_ID
				
				union 

				select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                 from cust_cif a
                inner join cust_product b on a.CIF_ID = b.CIF_ID
                inner join SINVEST_IFUA_MAP_DELETE c 
                   on b.Niaga_acc = c.niaga_acc 
                  and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                --  and c.IFUA_NO not like '%T%'
                group by a.CIF_ID
				
				) h 
           on a.CIF_SMS = h.CIF_ID
  
    inner join (select fprid, max(Approval_Date) Apv_Date
                  from trx_fpr_temp 
                 where approval_by is not null 
                   and (valid=9 and approval_status=1
                        or valid=0 and approval_status=0
                        or valid=77 and approval_status=1) 
                 group by fprid) q on q.fprid = a.urutan
    where a.JENIS_REKENING = '1'
      --and a.VALID = 1
      and exists (select 1 from vw_cust_cif_sam_rfrrl cs where cs.cif_id = a.cif_sms)
      and DATEADD(dd, DATEDIFF(dd, 0, q.Apv_Date), 0) <= '20210521'
	  and ISNULL(nama_nasabah, '') not like '%TEST%'
	  and ISNULL(nama_nasabah, '') not like '%INSTITUSI AM%'
	  and ISNULL(nama_nasabah, '') not like '%NASABAH BSM%'
	  and ISNULL(nama_nasabah, '') not like '%NASABAH SAM%'
	  and ISNULL(d.pemilik, '') <> ''

union --- bank 3

 select 
    'DH002' SACode,
	'PT Sinarmas Asset Management' SAName,
    a.clientsid SID,
	ISNULL(h.ifua_no, '') IFUA,
	--[dbo].[GET_IFUA_NO_BY_CIF_ID](a.cif_sms) IFUA,
    ISNULL(NAMA_NASABAH, di_nama) as Name,
	RIGHT('000000' +  cast(a.urutan as varchar), 6) clientcode,
    '' as [REDMPaymentBankBICCode3], 
     ISNULL(g.map_id, '')  as [REDMPaymentBankBIMemberCode3],
     f.bank as [REDMPaymentBankName3], 
    case isnull(g.map_id,'')
      when '' then ''
      else 'ID'
  end as [REDMPaymentBankCountry3], 
    f.cabang as [REDMPaymentBankBranch3],
    isnull(f.currency,
        case isnull(g.map_id,'')
          when '' then ''
          else 'IDR'
        end) as [REDMPaymentA/CCCY3], 
    dbo.stripid(f.no_rek) as [REDMPaymentA/CNo3],
    f.pemilik as [REDMPaymentA/CName3],
	f.Seq_no [REDM Payment A/C Sequential Code],
	'1' [Status]
    from FPR a
    left join fpr_keuangan_pt fkeu on fkeu.FPR_ID=a.urutan
    left join aria_provinsi k on a.di_kota=k.[kotamadya/kabupaten]
    --left join aria_provinsi m on a.di_kota=m.[kotamadya/kabupaten]
    inner join fpr_bank_sinvest_temp f
      on f.fpr_id = a.urutan    
     and f.SEQ_NO = 3   
    left join (select bank_id, map_id from nfs_bank_map group by bank_id, map_id) g 
      on f.bank = g.BANK_ID
    left join (select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                 from cust_cif a
                inner join cust_product b on a.CIF_ID = b.CIF_ID
                inner join SINVEST_IFUA_MAP c 
                   on b.Niaga_acc = c.niaga_acc 
                  and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                 -- and c.IFUA_NO not like '%T%'
                group by a.CIF_ID
				
				union 

				select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                 from cust_cif a
                inner join cust_product b on a.CIF_ID = b.CIF_ID
                inner join SINVEST_IFUA_MAP_DELETE c 
                   on b.Niaga_acc = c.niaga_acc 
                  and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                --  and c.IFUA_NO not like '%T%'
                group by a.CIF_ID
				
				) h 
           on a.CIF_SMS = h.CIF_ID
    left join SINVEST_BI_MBR_CD p on g.MAP_ID = p.BI_CD
    inner join (select fprid, max(Approval_Date) Apv_Date
                  from trx_fpr_temp 
                 where approval_by is not null 
                   and (valid=9 and approval_status=1
                        or valid=0 and approval_status=0
                        or valid=77 and approval_status=1) 
                 group by fprid) q on q.fprid = a.urutan
    where a.JENIS_REKENING = '1'
      --and a.VALID = 1
      and exists (select 1 from vw_cust_cif_sam_rfrrl cs where cs.cif_id = a.cif_sms)
      and DATEADD(dd, DATEDIFF(dd, 0, q.Apv_Date), 0) <= '20210521'
	  and ISNULL(nama_nasabah, '') not like '%TEST%'
	  and ISNULL(nama_nasabah, '') not like '%INSTITUSI AM%'
	  and ISNULL(nama_nasabah, '') not like '%NASABAH BSM%'
	  and ISNULL(nama_nasabah, '') not like '%NASABAH SAM%'
	  and ISNULL(f.pemilik, '') <> ''

	order by ISNULL(NAMA_NASABAH, di_nama), [REDM Payment A/C Sequential Code]

