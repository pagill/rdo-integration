select cc.cif_id, 
	RIGHT('00000' + cast(f.urutan as varchar),6) FPRID,
	cc.cif_master,
	ISNULL(cc.clientsid, '') SID,
	ifua.IFUA_NO,
	cc.user_name,
	cc.id_number,
	ISNULL(cc.agent_cd, '') KodeMarketing,
	ISNULL(agt.nama, '') Marketing,
	ISNULL(agt.kd_cab, '') KodeCabang,
	ISNULL(cbg.nama, '') Cabang,
	ISNULL(agt.upline, '') KodeUpline,
	ISNULL(upl.nama, '') Upline
from cust_cif cc (nolock)
inner join vw_cust_cif_sam_rfrrl vcc on cc.cif_id = vcc.cif_id
left join fpr f (nolock) on cc.cif_id = f.cif_sms
left join (select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                 from cust_cif a (nolock)
                inner join cust_product b (nolock) on a.CIF_ID = b.CIF_ID
                inner join SINVEST_IFUA_MAP c (nolock)
                   on b.Niaga_acc = c.niaga_acc 
                  and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                 -- and c.IFUA_NO not like '%T%'
                group by a.CIF_ID
				
				union 

				select a.CIF_ID, max(c.IFUA_NO) as IFUA_NO
                 from cust_cif a (nolock)
                inner join cust_product b (nolock) on a.CIF_ID = b.CIF_ID
                inner join SINVEST_IFUA_MAP_DELETE c (nolock)
                   on b.Niaga_acc = c.niaga_acc 
                  and isnull(a.aperd_kode,'') = isnull(c.aperd_kode,'')
                --  and c.IFUA_NO not like '%T%'
                group by a.CIF_ID
				
				) ifua 
           on cc.cif_id = ifua.CIF_ID
left join agent agt (nolock) on cc.agent_cd = agt.agent_cd
left join cabang cbg (nolock) on agt.kd_cab = cbg.kd_cab
left join agent upl (nolock) on agt.upline = upl.agent_cd
where cc.cif_master <> '99999999'
order by ISNULL(cbg.nama, ''), ISNULL(upl.nama, ''), ISNULL(agt.nama, ''), cc.user_name