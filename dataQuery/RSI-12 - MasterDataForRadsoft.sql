
select ID, [name], [TYPE], Currency, bankname, product_name, norekening, atasnama
from (
select '' [ID], 
	'' [Name],
	case when a.jenis_rekening = 'S' then 'Subscribtion' 
		 when a.jenis_rekening = 'R' then 'Operation' end [type],
	'' currency,
	ISNULL(bk.bankname, a.bankname)Bankname ,
	tb.product_name,
	dbo.stripid(a.norekening) [NoRekening],
	a.atasnama,'Reguler' dipake
from SubScribeBank a
left join mst_bankcustody bk on bk.bankid = replace(replace(a.bankname, 'Niaga', 'CIMB'), 'Bank Sinarmas', 'Sinarmas')
inner join product_tab tb on a.product_code = tb.product_code
where tb.reksa = 1

union

select '', '', 
	case when a.jenis_rekening = 'S' then 'Subscribtion' 
		 when a.jenis_rekening = 'R' then 'Operation' end [type],
	'',
	ISNULL(bk.bankname, a.bankname)Bankname ,
	tb.product_name,
	dbo.stripid(a.norekening) [NoRekening],
	a.atasnama,
	'Online' dipake
From [dbo].[vw_sbscrb_bank_rol] a
left join mst_bankcustody bk on bk.bankid = replace(replace(replace(a.bankname, 'Niaga', 'CIMB'), 'Bank Sinarmas', 'Sinarmas'), 'MAYBANK', 'BII')
inner join product_tab tb on a.product_code = tb.product_code
)qry
group by ID, [name], [TYPE], Currency, bankname, product_name, norekening, atasnama
order by product_name, [type]


select bank_name, '', account_number, account_name, '' , branch, contact_person, '', 
phone_number,
	case when charindex('/', phone_number) > 0 then LEFT(phone_number, (charindex('/', phone_number)-1)) else  phone_number end,
	case when charindex('/', phone_number) > 0 then replace(SUBSTRING(phone_number, charindex('/', phone_number)+1, len(phone_number)), ' ', '') else '' end
from bank_input order by autoid

Select pmap.map_id, pt.product_name, fdl.brokercode, mb.brokername, '', '', '', '', 
	fdl.brokerFeeValue,
	fdk.brokerFeeValue,
	fdv.brokerFeeValue,
	fdw.brokerFeeValue,
	fdb.brokerFeeValue,
	fds.brokerFeeValue,
	fdbn.brokerFeeValue,
	fdsn.brokerFeeValue,
	fdc.brokerFeeValue
from product_tab pt (nolock)
inner join master_fund mf (nolock) on mf.product_code=pt.product_code 
left join nfs_prd_map pmap (nolock) on pmap.prd_id = pt.product_code
LEFT join fundbrokerfeedetail fdl (nolock) on fdl.product_code = pt.product_code and fdl.brokerfeename = 'LEVY'
LEFT join master_broker mb (nolock) on mb.brokercode = fdl.brokercode
LEFT join fundbrokerfeedetail fdk (nolock) on fdk.product_code = pt.product_code 
	and fdk.brokerCode = fdl.brokercode 
	and fdk.brokerfeename = 'KPEI'
LEFT join fundbrokerfeedetail fdv (nolock) on fdv.product_code = pt.product_code 
	and fdv.brokerCode = fdl.brokercode  
	and fdv.brokerfeename = 'VAT'
LEFT join fundbrokerfeedetail fdw (nolock) on fdw.product_code = pt.product_code 
	and fdw.brokerCode = fdl.brokercode 
	and fdw.brokerfeename = 'WHT'
LEFT join fundbrokerfeedetail fdb (nolock) on fdb.product_code = pt.product_code 
	and fdb.brokerCode = fdl.brokercode 
	and fdb.brokerfeename = 'Buy'
LEFT join fundbrokerfeedetail fds (nolock) on fds.product_code = pt.product_code 
	and fds.brokerCode = fdl.brokercode 
	and fds.brokerfeename = 'Sell'
LEFT join fundbrokerfeedetail fdbn (nolock) on fdbn.product_code = pt.product_code
	and fdBN.brokerCode = fdl.brokercode 
	and fdbn.brokerfeename = 'Buy_Net'
LEFT join fundbrokerfeedetail fdsn (nolock) on fdsn.product_code = pt.product_code 
	and fdsn.brokerCode = fdl.brokercode 
	and fdsn.brokerfeename = 'Sell_Net'
LEFT join fundbrokerfeedetail fdc (nolock) on fdc.product_code = pt.product_code
	and fdc.brokerCode = fdl.brokercode 
	and fdc.brokerfeename = 'CapitalGainTax'
--group by pmap.map_id, pt.product_name, fdl.brokercode, mb.brokername,
--	fdl.brokerFeeValue,
--	fdk.brokerFeeValue,
--	fdv.brokerFeeValue,
--	fdw.brokerFeeValue,
--	fdb.brokerFeeValue,
--	fds.brokerFeeValue,
--	fdbn.brokerFeeValue,
--	fdsn.brokerFeeValue,
--	fdc.brokerFeeValue
order by pt.product_name, mb.brokername



SELECT DISTINCT ps.productname, pmap.map_id,  awal,
                    CASE
					when awal = 0 and akhir < 1 THEN '< 1'
                    WHEN awal = 0 AND akhir < 3 THEN '< 3'
                    WHEN awal = 0 AND akhir < 6 THEN '< 6'
                    WHEN awal = 0 AND akhir < 12 THEN '< 12'
                    WHEN awal = 0 AND akhir > 12 THEN ''
					WHEN awal = 1 AND akhir > 12 THEN '≥ 1'
                    WHEN awal = 3 AND akhir < 6 THEN '< 6'
                    WHEN awal = 3 AND akhir > 6 THEN '>= 3'
                    WHEN awal = 6 AND akhir < 12 THEN '< 12'
                    WHEN awal = 6 AND akhir > 12 THEN '>= 6'
                    WHEN awal = 12 AND akhir > 12 THEN '>= 12'
                    ELSE
                    cast(fee*100 AS varchar)+'%'
                    END AS range, cast(fee*100 AS varchar)+'%' AS fee2
                    FROM Param_Pengendapan a
                    inner join productsetup ps on a.product_code = ps.productid
					inner join nfs_prd_map pmap on ps.productid = pmap.prd_id
                    ORDER BY ps.productname, Awal, akhir ASC