<?php
//------------------------------------------------------------------
//Retrieve JSON File
header("Content-Type: application/json; charset=UTF-8");
$input = trim(file_get_contents("php://input"));
$datas = json_decode($input, false);

//------------------------------------------------------------------
//Set Variable Constant
require_once('dbconn_sql.php');
mssql_select_db($db_reksadana, $conn);

mssql_query("SET ANSI_NULLS ON", $conn);
mssql_query("SET ANSI_WARNINGS ON", $conn);

$error_Data = false;

foreach ($datas as $data){
    // echo $data->sa_code . "\n";
    // echo $data->ifua_no . "\n";
    // echo $data->ifua_name . "\n";
    // echo $data->sid . "\n";
    // echo $data->client_code . "\n";
    // echo $data->investor_type . "\n";
    // echo $data->status . "\n";

    // echo $data["sa_code"] . "\n";
    // echo $data["ifua_no"] . "\n";
    // echo $data["ifua_name"] . "\n";
    // echo $data["sid"] . "\n";
    // echo $data["client_code"] . "\n";
    // echo $data["investor_type"] . "\n";
    // echo $data["status"] . "\n";

    //Set variable
    $v_sa_code = $data->sa_code;
    $v_ifua_no = $data->ifua_no;
    $v_ifua_name = $data->ifua_name;
    $v_sid = $data->sid;
    $v_client_code = $data->client_code;
    $v_investor_type  = $data->investor_type;
    $v_status   = $data->status;

    $v_check = true;
    $v_RSLT_ID = "0";
    $v_RSLT_MSG = "";

    //Validation
    if ($v_check && $v_sa_code == "") {
        $v_RSLT_ID = "200";
        $v_RSLT_MSG = "SA Code cannot null";
        $v_check = false;
    }

    if ($v_check && $v_ifua_no == "") {
        $v_RSLT_ID = "200";
        $v_RSLT_MSG = "IFUA No cannot null";
        $v_check = false;
    }

    if ($v_check && $v_ifua_name == "") {
        $v_RSLT_ID = "200";
        $v_RSLT_MSG = "IFUA Name cannot null";
        $v_check = false;
    }

    if ($v_check && $v_sid == "") {
        $v_RSLT_ID = "200";
        $v_RSLT_MSG = "SID cannot null";
        $v_check = false;
    }

    if ($v_check && $v_client_code == "") {
        $v_RSLT_ID = "200";
        $v_RSLT_MSG = "Client Code cannot null";
        $v_check = false;
    }

    if ($v_check && $v_investor_type == "") {
        $v_RSLT_ID = "200";
        $v_RSLT_MSG = "Investor Type cannot null";
        $v_check = false;
    }

    if ($v_check && $v_status == "") {
        $v_RSLT_ID = "200";
        $v_RSLT_MSG = "Status cannot null";
        $v_check = false;
    }

    if ($v_check) {
        //$array_join = array();

        $query		= "DECLARE @RSLT_STS INT, @RSLT_MSG VARCHAR(300) ".
                    "EXEC SINVEST_IFUA_PATCH_RDOAPI '".$v_sa_code."','".$v_ifua_no."','".$v_ifua_name."','".$v_sid."','".$v_client_code."','".$v_investor_type."','".$v_status."', @RSLT_STS OUTPUT, @RSLT_MSG OUTPUT ".
                    "SELECT @RSLT_STS RSLT_STS, @RSLT_MSG RSLT_MSG";

        $exec       = mssql_query($query);  
        
        if ($exec) {
            while ($row = mssql_fetch_array($exec)){
                $v_RSLT_ID = $row["RSLT_STS"];
                $v_RSLT_MSG = $row["RSLT_MSG"];

                $rslt = array(
                    'rslt_id'=>$v_RSLT_ID,
                    'message'=>$v_RSLT_MSG
                );

                if ($v_RSLT_ID == 0) {
                    //$arrayMerge = "";
                    if ($error_Data) {
                        
                    } else {
                        $arrayMerge = $rslt;
                    }
                }
                
                if ($v_RSLT_ID == 1) {
                    //$arrayMerge = "";
                    $error_Data = true;
                    $arrayMerge = $rslt;
                }

                //$arrayMerge = $rslt;
                //array_push($rslt, $rslt);
                //$response = $response + $rslt;

            }
        }
    }
} 

$response = $arrayMerge;
echo json_encode($response);

mssql_free_result($exec);
mssql_close($conn);

?>